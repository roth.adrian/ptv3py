import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import ptv3py
from ptv3py import simulation

np.random.seed(0)

simulation_kwargs = {
    "n_frames": 2,
    "n_particles_per_frame": 3,
    "image_shape": (20, 20),
}
n_cameras = 2  # always the case


def get_simple_camera_calibration():
    return simulation.get_simple_camera_calibration(simulation_kwargs["image_shape"])


def simulate_particles_to_match(
    n_frames, n_particles_per_frame, image_shape, all_coordinates=None, noise_std=0
):
    """Corresponds to the simple camera calibration"""
    volume_size = np.array([image_shape[0], image_shape[0], image_shape[1]])

    if all_coordinates is None:
        n_particles = n_frames * n_particles_per_frame
        all_coordinates = (
            1
            + np.round(
                np.random.rand(n_frames * n_particles_per_frame, 3) * (volume_size - 2)
            )
        ).T
    else:
        n_particles = len(all_coordinates)
        all_coordinates = all_coordinates.T
        n_particles_per_frame = n_particles // n_frames

    first_camera_ids = np.arange(n_particles, dtype=int)
    second_camera_ids = first_camera_ids + n_particles

    real_threed_particles = {
        "id": first_camera_ids,
        "id_C1": second_camera_ids,
        "frame": np.repeat(np.arange(n_frames, dtype=int), n_particles_per_frame),
    }
    real_threed_particles.update(
        {dim: all_coordinates[i] for i, dim in enumerate(["x", "y", "z"])}
    )
    real_threed_particles = pd.DataFrame(real_threed_particles)
    ptv3py.set_coordinate_columns(real_threed_particles)

    camera_calibration = get_simple_camera_calibration()
    simulated_found_particles = simulation.simulate_projection(
        real_threed_particles, camera_calibration
    )

    simulated_found_particles["coordinates"] += (
        np.random.randn(len(simulated_found_particles), 2) * noise_std
    )

    # ptv3py.set_coordinate_columns(simulated_found_particles)
    return real_threed_particles, simulated_found_particles


def get_matching_correct_fraction(
    real_threed_particles, matched_particles, only_ids=False
):
    if only_ids:
        interesting_columns = ["id", "id_C1"]
    else:
        interesting_columns = ["id", "frame", "coordinates", "id_C1"]
    matched_particles = matched_particles[interesting_columns]
    real_threed_particles = real_threed_particles[interesting_columns]

    n_particles = len(real_threed_particles)
    n_correct_particles = 0
    for i in range(len(real_threed_particles)):
        found_correct = False
        real_particle = real_threed_particles.loc[i].to_numpy()
        for j in range(len(matched_particles)):
            matched_particle = matched_particles.loc[j].to_numpy()
            found_correct = np.all(real_particle == matched_particle)
            if found_correct:
                break
        if found_correct:
            n_correct_particles += 1

    correct_fraction = n_correct_particles / n_particles
    return correct_fraction


def test_matching_simple():
    real_threed_particles, simulated_found_particles = simulate_particles_to_match(
        **simulation_kwargs
    )

    # adding extra column to check that it comes through correctly
    simulated_found_particles["test"] = simulated_found_particles["camera"].copy()

    camera_calibration = get_simple_camera_calibration()
    matched_particles = ptv3py.threedmatch_particles(
        simulated_found_particles, camera_calibration, 0.1
    )
    ptv3py.visualise_matched_particles(
        matched_particles,
        simulated_found_particles,
        axis_equal=True,
        zaxis=1,
    )

    assert np.all(
        matched_particles["test"] == 0
    ), "Extra numerical column is not merged correctly"

    correct_fraction = get_matching_correct_fraction(
        real_threed_particles, matched_particles
    )
    assert correct_fraction == 1, "Correct fraction of matched particles {}".format(
        correct_fraction
    )


def test_matching_coordinate_limits():
    real_threed_particles, simulated_found_particles = simulate_particles_to_match(
        **simulation_kwargs
    )

    coordinate_limits = [[-1, 0], [-1, 0], [-1, 0]]
    camera_calibration = get_simple_camera_calibration()
    matched_particles = ptv3py.threedmatch_particles(
        simulated_found_particles,
        camera_calibration,
        0.1,
        coordinate_limits=coordinate_limits,
    )
    assert len(matched_particles) == 0, "Match volume not working"


def test_matching_coordinate_limits2():
    camera_calibration = get_simple_camera_calibration()
    all_coordinates = np.array([[1, 1, 1], [2, 2, 2]])
    real_threed_particles, simulated_found_particles = simulate_particles_to_match(
        1, 2, simulation_kwargs["image_shape"], all_coordinates
    )

    coordinate_limits = [[0, 3], [0, 3], [1.4, 1.6]]
    matched_particles = ptv3py.threedmatch_particles(
        simulated_found_particles,
        camera_calibration,
        10,
        coordinate_limits=coordinate_limits,
    )
    assert len(matched_particles) == 2
    assert np.all(matched_particles["matching_error"] == 1)


def test_matching_with_feature():
    all_coordinates = np.array([[11, 5, 3], [10, 6, 2]])
    real_threed_particles, simulated_found_particles = simulate_particles_to_match(
        1, 2, simulation_kwargs["image_shape"], all_coordinates
    )
    simulated_found_particles.loc[:1, "id"] = simulated_found_particles.loc[
        :1, "id"
    ].to_numpy()[::-1]
    simulated_found_particles["feature"] = [1, 10, 10, 1]
    camera_calibration = get_simple_camera_calibration()

    matched_particles = ptv3py.threedmatch_particles(
        simulated_found_particles,
        camera_calibration,
        10,
        feature_column=["feature", 1],
    )

    correct_fraction = get_matching_correct_fraction(
        real_threed_particles, matched_particles, only_ids=True
    )
    assert correct_fraction == 1, "Correct fraction of matched particles {}".format(
        correct_fraction
    )


def test_matching_visualization():
    real_threed_particles, simulated_found_particles = simulate_particles_to_match(
        **simulation_kwargs
    )
    real_threed_particles.drop(index=0, inplace=True)
    ps = ptv3py.visualise_matched_particles(
        real_threed_particles, simulated_found_particles, verbose=True
    )
    # plt.show()
    plt.close(ps)


def test_matching_helper():
    _, simulated_found_particles = simulate_particles_to_match(
        **simulation_kwargs, noise_std=0.1
    )
    matching_kwargs = {
        "max_error": 0.1,
    }
    camera_calibration = get_simple_camera_calibration()
    mptf, ps = ptv3py.particle_matching_parameters_helper(
        simulated_found_particles,
        camera_calibration,
        images=simulation_kwargs["image_shape"],
    )
    mptf.set_parameters(**matching_kwargs)
    mptf.match_particles()
    # plt.show()
    plt.close(ps)
    plt.close(mptf)
