import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import ptv3py
from ptv3py import particle_finding, simulation


pd.set_option("display.max_columns", None)
pd.set_option("display.width", None)

simulation_kwargs = {
    "n_frames": 2,
    "n_cameras": 2,
    "image_shape": (100, 100),
    "n_particles_per_frame": 5,
    "point_spread_sizes": 1,
    "point_spread_function": "gaussian",
}


def simulate_particle_coordinates_and_images(
    n_frames,
    n_cameras,
    image_shape,
    n_particles_per_frame,
    point_spread_sizes=3,
    point_spread_function="gaussian",
    saturation=255,
):
    np.random.seed(0)
    # global simulated_real_particles, simulated_particle_images
    # if simulated_real_particles is None or simulated_particle_images is None:
    n_particles = n_frames * n_cameras * n_particles_per_frame
    point_spread_sizes = np.expand_dims(np.asarray(point_spread_sizes).ravel(), 1)
    coordinates = 2 * point_spread_sizes + np.random.rand(n_particles, 2) * (
        image_shape - 4 * point_spread_sizes
    )
    real_particles = {
        "id": np.arange(n_particles, dtype=int),
        "frame": np.repeat(
            np.arange(n_frames, dtype=int), n_particles_per_frame * n_cameras
        ),
        "camera": np.tile(
            np.repeat(np.arange(n_cameras, dtype=int), n_particles_per_frame),
            n_frames,
        ),
        "x": coordinates[:, 0],
        "y": coordinates[:, 1],
    }
    simulated_real_particles = pd.DataFrame(real_particles)
    ptv3py.set_coordinate_columns(simulated_real_particles)
    simulated_particle_images = simulation.simulate_particle_images(
        image_shape,
        simulated_real_particles,
        point_spread_sizes=point_spread_sizes,
        point_spread_function=point_spread_function,
        saturation=saturation,
    )
    return simulated_real_particles, simulated_particle_images


# to test the extra_features_func parameter
def max_values_features_func(image, n_labels, labels):
    max_values = np.zeros(n_labels - 1)
    for label in range(1, n_labels):
        max_values[label - 1] = np.max(image[labels == label])
    return pd.DataFrame({"max_value": max_values})


def visualise_particles(real_particles, images, particles=None, labels=None, vmax=255):
    if particles is not None:
        if "id" not in particles:
            particles.insert(0, "id", 0)
        if "frame" not in particles:
            particles.insert(1, "frame", 0)
        for i, column in enumerate(particles.columns):
            if column not in real_particles:
                real_particles.insert(
                    i,
                    column,
                    np.zeros(len(real_particles), dtype=particles.dtypes[column]),
                )
        values = np.concatenate(
            (np.ones(len(real_particles)) * 0.5, np.ones(len(particles)) * 0)
        )
        particles = pd.concat([real_particles, particles])
    else:
        particles = real_particles
        values = np.ones(len(real_particles)) * 0.5
    if len(images.shape) == 3:
        images = np.expand_dims(images, 0)

    post_plotting = None
    if labels is not None:

        def post_plotting(ps):
            ps.imshow(labels)

    ptv3py.visualise_found_particles(
        particles,
        images,
        vmax=vmax,
        values_clim=[0, 1],
        values=values,
        post_plotting=post_plotting,
        verbose=True,
    )
    plt.show()


def check_correct_found_particles(real_particles, particles):
    n_particles = len(real_particles)
    n_correct_particles = 0
    for i, real_particle in real_particles.iterrows():
        _, frame, camera, x, y = real_particle.to_numpy()[:5]
        found_correct = False
        for j, particle in real_particles.iterrows():
            coordinates = particle["coordinates"].to_numpy()
            location_error = np.sqrt(
                (x - coordinates[0]) ** 2 + (y - coordinates[1]) ** 2
            )
            found_correct = (
                frame == particle["frame"]
                and camera == particle["camera"]
                and location_error < 5e-2
            )
            if found_correct:
                break
        if found_correct:
            n_correct_particles += 1
            particles.drop(j, inplace=True)
        if particles.shape[0] == 0:
            break

    correct_fraction = n_correct_particles / n_particles
    assert correct_fraction == 1, "Correct fraction of particles found {}".format(
        correct_fraction
    )


def test_finding_simple():
    real_particles, images = simulate_particle_coordinates_and_images(
        **simulation_kwargs
    )
    # testing visualisation function
    ps = ptv3py.visualise_found_particles(real_particles, images, vmax=255)
    plt.close(ps)
    # plt.show()

    finding_kwargs = {
        "threshold": 80,
        "gradient_threshold": 10,
        "saturation": 255,
    }
    particles = ptv3py.find_particles(
        images, extra_features_func=max_values_features_func, **finding_kwargs
    )
    # visualise_particles(real_particles, images, particles)

    assert len(particles) == len(
        real_particles
    ), "error, not correct number of particles found"

    correct_max_value_fraction = np.sum(particles["max_value"] > 0.80) / len(particles)
    assert (
        correct_max_value_fraction == 1
    ), "Extra features function max_values not working, fraction correct {}".format(
        correct_max_value_fraction
    )
    check_correct_found_particles(real_particles, particles)


def test_multiprocess_finding():
    real_particles, images = simulate_particle_coordinates_and_images(
        **simulation_kwargs
    )

    threshold = 80
    particles = ptv3py.find_particles(
        images, threshold=threshold, with_multiprocessing=2
    )
    # visualise_particles(real_particles, images, particles)
    assert len(particles) == len(
        real_particles
    ), "error, not correct number of particles found"
    check_correct_found_particles(real_particles, particles)


def test_merge_particle_clusters():
    real_particles, _ = simulate_particle_coordinates_and_images(**simulation_kwargs)

    # some value to test the special_merge_functions parameter
    real_particles["key"] = 1
    special_merge_functions = {"key": np.sum}

    # large number so that all particles are merged
    min_point_distance = np.inf
    merged_particles = ptv3py.merge_particle_clusters(
        real_particles,
        min_point_distance,
        special_merge_functions=special_merge_functions,
    )
    assert (
        len(merged_particles)
        == simulation_kwargs["n_frames"] * simulation_kwargs["n_cameras"]
    ), "All merged particle clusters does not seem to work"
    assert np.all(
        merged_particles.dtypes == real_particles.dtypes
    ), "All merged particle gives wrong return types"
    assert np.all(
        merged_particles["key"].to_numpy() == simulation_kwargs["n_particles_per_frame"]
    ), "Special merge function does not seem to work"

    # small number so no merges are made
    min_point_distance = 1
    merged_particles = ptv3py.merge_particle_clusters(
        real_particles, min_point_distance
    )
    assert np.all(
        merged_particles.to_numpy() == real_particles.to_numpy()
    ), "None merged particle clusters does not seem to work"


def test_coordinates_below_threshold():
    test_image = np.array(
        [
            [0, 0, 0, 0, 0],
            [0, 2, 3, 3, 0],
            [0, 20, 36, 0, 0],
            [0, 5, 5, 7, 0],
            [0, 0, 0, 0, 0],
        ]
    )
    images = np.array([test_image])

    finding_kwargs = {
        "threshold": 25,
        "saturation": 255,
    }
    particles = ptv3py.find_particles(images, **finding_kwargs)
    print()
    print(particles)
    # visualise_particles(particles, images, vmax=40)

    assert (
        particles.loc[0, "coordinates"].to_numpy()[0] < 2
    ), "error, wrong side of center"


def test_finding_visualizer():
    real_particles, images = simulate_particle_coordinates_and_images(
        **simulation_kwargs
    )

    pss = []

    # simple
    scaled_real_particles = real_particles.copy()
    scaled_real_particles["coordinates"] /= simulation_kwargs["image_shape"][0]
    ps = ptv3py.visualise_found_particles(
        scaled_real_particles,
        images,
        extent=[0, 1, 1, 0],
        vmin=20,
        vmax=100,
        cmap="gray_r",
        verbose=True,
    )
    ps.colorbar()
    ps.suptitle("extent and image stuff")
    pss.append(ps)

    # no images
    ps = ptv3py.visualise_found_particles(real_particles, images.shape, verbose=True)
    ps.suptitle("no images")
    pss.append(ps)

    # no particles
    ps = ptv3py.visualise_found_particles([], images, verbose=True)
    ps.suptitle("no particles")
    pss.append(ps)

    # with values
    real_particles_with_values = real_particles.copy()
    real_particles_with_values["values"] = np.arange(len(real_particles_with_values))
    ps = ptv3py.visualise_found_particles(
        real_particles_with_values,
        images,
        values="values",
        values_clim=[0, 4],
        values_cmap="inferno",
        verbose=True,
    )
    ps.colorbar()
    ps.suptitle("with values")
    pss.append(ps)

    # plot_only
    ps = ptv3py.visualise_found_particles(
        real_particles, images, plot_only=True, verbose=True
    )
    ps.suptitle("plot only")
    pss.append(ps)

    # plt.show()

    for ps in pss:
        plt.close(ps)


def test_finding_helper():
    real_particles, images = simulate_particle_coordinates_and_images(
        **simulation_kwargs
    )
    finding_kwargs = {
        "threshold": 80,
        "gradient_threshold": 10,
        "saturation": 255,
    }
    fptf, ps = ptv3py.particle_finding_parameters_helper(
        images=images,
        saturation=finding_kwargs["saturation"],
        find_particles_function=particle_finding.find_particles_single_frame,
        # find_particles_function_kwargs={
        #     "extra_features_func": particle_finding.max_sum_area_features_func
        # },
    )
    fptf.set_parameters(
        **finding_kwargs,
        extra_features_func=particle_finding.max_sum_area_features_func
    )
    fptf.find_particles()
    # plt.show()
    plt.close(ps)
    plt.close(fptf)
