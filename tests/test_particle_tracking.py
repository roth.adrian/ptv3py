import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import ptv3py
from ptv3py import particle_tracking

ndims = 2
simulation_kwargs = {
    "n_particles": 15,
    "n_frames": 15,  # at least three frames since the tracking requires it to function
    "ndims": ndims,
    "speed_mu": 0.3,
    "speed_std": 0.2,
    "acceleration_mu": 0.01,
    "acceleration_std": 0,
    "noise_std": 0.01,
    "volume": np.ones(ndims) * 20,
}

tracking_kwargs = {
    "min_speed": 0,
    "max_speed": 2,
    "max_acceleration": 0.2,
    "max_extrapolation_error": 0.2,
    "max_jumped_frames": 0,
    "min_track_length": 2,
    "initial_velocity": None,
    "initial_max_acceleration": None,
}


def get_simulation(seed=0):
    np.random.seed(seed)
    true_tracked_particles = ptv3py.simulate_particle_tracks(**simulation_kwargs)
    # ps = ptv3py.visualise_tracked_particles(true_tracked_particles, verbose=2)
    return true_tracked_particles


def assert_result_accuracy(
    true_tracked_particles, tracked_particles, min_accuracy=1, exact_accuracy=None
):
    tracking_accuracy = ptv3py.tracking_accuracy_estimator(
        true_tracked_particles, tracked_particles
    )
    if exact_accuracy is None:
        assert (
            tracking_accuracy >= min_accuracy
        ), "Tracking accuracy: {:.2f}. Not high enough accuracy, expected minimum {:.2f}".format(
            tracking_accuracy, min_accuracy
        )
    else:
        assert (
            abs(tracking_accuracy - exact_accuracy) <= 1e-5
        ), "Tracking accuracy: {:.2f}. Not close enough to expected {:.2f}".format(
            tracking_accuracy, exact_accuracy
        )


def test_tracking_simple(tracking_kwargs=tracking_kwargs):
    true_tracked_particles = get_simulation()
    print()
    print(true_tracked_particles)

    # removing track_id
    particles = true_tracked_particles.drop("id_track", axis=1)
    # retracking expecting perfect same results
    tracked_particles = ptv3py.track_particles(
        particles,
        add_track_score=True,
        verbose=0,
        **tracking_kwargs,
    )
    print(tracked_particles)

    tracking_kwargs.pop("min_track_length", 0)
    ps = ptv3py.visualise_tracked_particles(
        tracked_particles,
        verbose=2,
        **tracking_kwargs,  # cmap="viridis", cmap_information=True
    )
    # plt.show()
    plt.close(ps)

    assert_result_accuracy(true_tracked_particles, tracked_particles)
    assert np.all(
        tracked_particles["track_score"] <= simulation_kwargs["n_frames"]
    ), "score can never be over the number of frames"


def test_tracking_with_jumps():
    true_tracked_particles = get_simulation(1)
    true_tracked_particles.sort_values(
        ["id_track", "frame"], inplace=True, ignore_index=True
    )
    # removing one particle in one frame
    remove_index = 6
    print()
    print(true_tracked_particles.loc[remove_index : remove_index + 1])
    true_tracked_particles.drop(remove_index, inplace=True)
    # removing track
    particles = true_tracked_particles.drop("id_track", axis=1)

    current_tracking_kwargs = tracking_kwargs.copy()
    current_tracking_kwargs["max_jumped_frames"] = 1
    tracked_particles = ptv3py.track_particles(
        particles,
        verbose=0,
        **current_tracking_kwargs,
    )
    current_tracking_kwargs.pop("min_track_length", 0)
    ptv3py.visualise_tracked_particles(
        tracked_particles,
        verbose=2,
        **current_tracking_kwargs,
    )
    # plt.show()
    assert_result_accuracy(true_tracked_particles, tracked_particles)

    ### should only allow a single jump for one track
    remove_index2 = 9
    print()
    print(true_tracked_particles.loc[remove_index2 : remove_index2 + 1])
    true_tracked_particles.drop(remove_index2, inplace=True)
    # removing track
    particles = true_tracked_particles.drop("id_track", axis=1)

    tracked_particles = ptv3py.track_particles(
        particles,
        verbose=0,
        **current_tracking_kwargs,
    )
    accuracy = (simulation_kwargs["n_particles"] - 1) / simulation_kwargs["n_particles"]
    assert_result_accuracy(
        true_tracked_particles, tracked_particles, exact_accuracy=accuracy
    )


def test_tracking_velocity():
    current_simulation_kwargs = simulation_kwargs.copy()
    for key in ["speed_std", "acceleration_mu", "acceleration_std", "noise_std"]:
        del current_simulation_kwargs[key]
    current_simulation_kwargs["speed_mu"] = 1
    true_tracked_particles = ptv3py.simulate_particle_tracks(
        **current_simulation_kwargs
    )
    tracked_velocities, averaged_tracked_velocities = ptv3py.tracked_velocities(
        true_tracked_particles,
        dt=1,
        with_accelerations=True,
        return_average_particles=True,
    )

    speeds = np.linalg.norm(tracked_velocities["velocities"], axis=1)
    speeds2 = tracked_velocities["speed"]
    average_speeds = averaged_tracked_velocities["speed"]
    assert np.all(
        speeds - 1 < 1e-6
    ), "Speed should all be one but they have mean, {:.2f}".format(np.mean(speeds))
    assert np.all(
        speeds2 - 1 < 1e-6
    ), "Speed2 should all be one but they have mean, {:.2f}".format(np.mean(speeds2))
    assert np.all(
        average_speeds - 1 < 1e-6
    ), "Average speed should all be one but they have mean, {:.2f}".format(
        np.mean(average_speeds)
    )

    current_simulation_kwargs["acceleration_mu"] = 1
    true_tracked_particles = ptv3py.simulate_particle_tracks(
        **current_simulation_kwargs
    )
    tracked_velocities, averaged_tracked_velocities = ptv3py.tracked_velocities(
        true_tracked_particles,
        dt=1,
        with_accelerations=True,
        return_average_particles=True,
    )
    normed_accelerations = np.linalg.norm(tracked_velocities["accelerations"], axis=1)
    normed_accelerations2 = tracked_velocities["normed_acceleration"]
    average_normed_accelerations = averaged_tracked_velocities["normed_acceleration"]
    assert np.all(
        normed_accelerations - 1 < 1e-6
    ), "Normed acceleration should all be one but they have mean, {:.2f}".format(
        np.mean(normed_accelerations)
    )
    assert np.all(
        normed_accelerations2 - 1 < 1e-6
    ), "Normed acceleration2 should all be one but they have mean, {:.2f}".format(
        np.mean(normed_accelerations2)
    )
    assert np.all(
        average_normed_accelerations - 1 < 1e-6
    ), "Average normed acceleration should all be one but they have mean, {:.2f}".format(
        np.mean(average_normed_accelerations)
    )

    tracked_velocities = ptv3py.tracked_velocities(
        true_tracked_particles,
        dt=1,
        with_accelerations=True,
        with_extrapolation_error=True,
    )
    extrapolation_errors = tracked_velocities["extrapolation_error"]
    assert np.all(
        extrapolation_errors < 1e-10
    ), "Extrapolation errors should all be zero but they have mean, {:.2f}".format(
        np.mean(extrapolation_errors)
    )


def test_tracking_largest_occurence():
    n_particles = simulation_kwargs["n_particles"]
    n_frames = simulation_kwargs["n_frames"]
    true_tracked_particles = get_simulation()

    sizes = []
    for track_id in range(n_particles):
        sizes.append(np.random.permutation(np.arange(n_frames) + 1))
    true_tracked_particles["size"] = np.stack(sizes).flatten()
    largest_particles = ptv3py.tracked_largest_occurence(true_tracked_particles)

    output_sizes = largest_particles["size"].to_numpy()
    assert np.all(output_sizes == n_frames), "not found only largest size particles"


def test_tracking_initial_velocity():
    frames = np.arange(5)
    coordinates1 = np.array(
        [
            [0, 0],
            [2, 0],
            [4, 0],
            [6, 0],
            [8, 0],
        ]
    )
    coordinates2 = np.array(
        [
            [-1, 0],
            [-2, 0],
            [-3, 0],
            [-4, 0],
        ]
    )
    true_tracked_particles1 = pd.DataFrame(
        {"x": coordinates1[:, 0], "y": coordinates1[:, 1]}
    )
    true_tracked_particles1.insert(0, "frame", frames)
    true_tracked_particles1.insert(0, "id_track", 0)
    true_tracked_particles2 = pd.DataFrame(
        {"x": coordinates2[:, 0], "y": coordinates2[:, 1]}
    )
    true_tracked_particles2.insert(0, "frame", frames[1:])
    true_tracked_particles2.insert(0, "id_track", np.arange(1, 5))
    true_tracked_particles = pd.concat(
        (true_tracked_particles1, true_tracked_particles2)
    )
    true_tracked_particles.insert(1, "id", np.arange(len(true_tracked_particles)))
    ptv3py.set_coordinate_columns(true_tracked_particles)
    print()
    print(true_tracked_particles)

    particles = true_tracked_particles.drop("id_track", axis=1)
    tracked_particles = ptv3py.track_particles(
        particles,
        max_acceleration=1,
        initial_velocity=[2, 0],
        # verbose=-0.1,
    )
    print(tracked_particles)
    assert_result_accuracy(true_tracked_particles, tracked_particles)

    # testing with initial_max_acceleration
    particles = true_tracked_particles.drop("id_track", axis=1)
    tracked_particles = ptv3py.track_particles(
        particles,
        initial_velocity=[2, 0],
        initial_max_acceleration=1,
        verbose=0,
    )
    print()
    print(tracked_particles)
    tracked_particles.index = tracked_particles["id"]
    assert (
        len(np.unique(tracked_particles["id_track"][np.arange(5)])) == 1
    ), "initial_max_acceleration not working"
    assert (
        len(np.unique(tracked_particles["id_track"][np.arange(5, 9)])) == 4
    ), "initial_max_acceleration not working"


def test_tracking_with_intensity():
    frames = np.arange(5)
    coordinates1 = np.array(
        [
            [0, 0],
            [1, 0],
            [2, 0],
            [3, 0.1],
            [4, -1],
        ]
    )
    coordinates2 = np.array(
        [
            [0, 0],
            [1, 0],
            [2, 0],
            [3, -0.1],
            [4, 1],
        ]
    )
    true_tracked_particles1 = pd.DataFrame(
        {"x": coordinates1[:, 0], "y": coordinates1[:, 1]}
    )
    true_tracked_particles1.insert(0, "frame", frames)
    true_tracked_particles1.insert(0, "id_track", 1)
    true_tracked_particles1["intensity"] = 1
    true_tracked_particles2 = pd.DataFrame(
        {"x": coordinates2[:, 0], "y": coordinates2[:, 1]}
    )
    true_tracked_particles2.insert(0, "frame", frames)
    true_tracked_particles2.insert(0, "id_track", 2)
    true_tracked_particles2["intensity"] = 2

    # this is just to check that single particle tracks in the end does not create infinite loops
    true_tracked_particles3 = pd.DataFrame({"x": [-1], "y": [-1]})
    true_tracked_particles3.insert(0, "frame", 4)
    true_tracked_particles3.insert(0, "id_track", 3)
    true_tracked_particles3["intensity"] = 3

    true_tracked_particles = pd.concat(
        (true_tracked_particles1, true_tracked_particles2, true_tracked_particles3)
    )
    true_tracked_particles.insert(1, "id", np.arange(len(true_tracked_particles)))
    ptv3py.set_coordinate_columns(true_tracked_particles)
    print()
    print(true_tracked_particles)

    particles = true_tracked_particles.drop("id_track", axis=1)
    tracked_particles = ptv3py.track_particles(
        particles,
        max_extrapolation_error=-1,
        track_columns=["intensity"],
        add_track_score=True,
        # verbose=2,
    )
    print(tracked_particles)
    assert_result_accuracy(true_tracked_particles, tracked_particles)

    print()
    print()
    print("Intensity tracking with jumps")
    true_tracked_particles.drop(index=2, inplace=True)
    print(true_tracked_particles)

    particles = true_tracked_particles.drop("id_track", axis=1)
    tracked_particles = ptv3py.track_particles(
        particles,
        max_extrapolation_error=-1,
        max_jumped_frames=1,
        track_columns=["intensity"],
        add_track_score=True,
        verbose=2,
    )
    print(tracked_particles)
    assert_result_accuracy(true_tracked_particles, tracked_particles)


def test_parameter_derivative_tracker1d():
    values = np.array([1, 1, 1, 2, 1])
    true_tracked_particles = pd.DataFrame({"value": values})
    true_tracked_particles.insert(0, "frame", np.arange(5))
    true_tracked_particles.insert(0, "id_track", 1)

    parameter_tracks = particle_tracking.Parameter_derivative_tracking(
        "value",
        limits=[],
        max_extrapolation_error=2,
        n_derivatives=0,
        extrapolation_error_scaling=5,
    )
    assert (
        parameter_tracks.static_attributes["value_score"] is False
    ), "value_score should be False generally"

    columns = parameter_tracks.extract_columns(true_tracked_particles)
    assert np.all(columns[0] == values[:, None])
    assert len(columns[1]) == 0

    first_two_rows = parameter_tracks.getitem_from_columns(columns, slice(2))
    assert np.all(first_two_rows[0] == values[:2])
    assert len(columns[1]) == 0

    first_row = parameter_tracks.getitem_from_columns(columns, [0])
    assert first_row[0][0] == values[0]
    assert len(columns[1]) == 0

    first_track = parameter_tracks.create_new_track(first_row)
    assert np.all(first_track.static_attributes["limits"] == [0, 2])
    valid, second_track, scores = first_track.validate_and_update(
        parameter_tracks.getitem_from_columns(columns, [1]), 0
    )
    assert scores[0] == 1
    assert valid[0]
    assert np.all(second_track[0].derivatives == [[1], [0]])
    valid, track, scores = second_track[0].validate_and_update(
        parameter_tracks.getitem_from_columns(columns, [2]), 0
    )
    assert scores[0] == 1
    assert valid[0]
    assert np.all(track[0].derivatives == [[1], [0]])
    valid, track, scores = track[0].validate_and_update(
        parameter_tracks.getitem_from_columns(columns, [3]), 0
    )
    assert scores[0] == 1 - 1 / 5
    assert valid[0]
    assert np.all(track[0].derivatives == [[2], [1]])
    valid, track, scores = track[0].validate_and_update(
        parameter_tracks.getitem_from_columns(columns, [4]), 0
    )
    assert scores[0] == 1 - 1 / 5
    assert valid[0]
    assert np.all(track[0].derivatives == [[1], [-1]])


def test_parameter_derivative_tracker2d():
    coordinates = np.array(
        [
            [0, 0],
            [1, 1],
            [2, 0],
            [3, 1],
            [4, 2],
        ]
    )
    true_tracked_particles = pd.DataFrame(
        {"x": coordinates[:, 0], "y": coordinates[:, 1]}
    )
    true_tracked_particles.insert(0, "frame", np.arange(5))
    true_tracked_particles.insert(0, "id_track", 1)
    ptv3py.set_coordinate_columns(true_tracked_particles)

    parameter_tracks = particle_tracking.Parameter_derivative_tracking(
        "coordinates",
        limits=[[0, -1], [0, 2]],
        max_extrapolation_error=2.1,
        n_derivatives=2,
        extrapolation_error_scaling=5,
    )
    assert np.all(
        parameter_tracks.static_attributes["limits"] == [[0, np.inf], [0, 2], [0, 2.1]]
    )
    assert "initial_limits" not in parameter_tracks.static_attributes

    #### Testing update ####
    columns = parameter_tracks.extract_columns(true_tracked_particles)
    assert np.all(columns[0] == coordinates)
    assert len(columns[1]) == 0

    parameter_tracks = parameter_tracks.create_new_track(
        parameter_tracks.getitem_from_columns(columns, [0])
    )
    assert parameter_tracks.initial
    assert np.all(parameter_tracks.derivatives[-1] == [0, 0])
    assert parameter_tracks.last_n_jumped_frames == 0
    assert np.isnan(parameter_tracks.last_extrapolation_error)
    new_derivatives = parameter_tracks.calc_new_derivatives(
        parameter_tracks.getitem_from_columns(columns, [1]), 0
    )
    assert len(new_derivatives[1]) == 0
    assert np.all(
        new_derivatives[0][0, :-1] == np.ones([2, 1, 2])
    ), "derivatives not correct"
    assert np.all(new_derivatives[0][0, -1] == [0, 0]), "derivatives not correct"
    new_derivatives_jumps = parameter_tracks.calc_new_derivatives(
        parameter_tracks.getitem_from_columns(columns, [1]), n_jumped_frames=1
    )
    assert np.all(
        new_derivatives_jumps[0][0] == [[1, 1], [0.5, 0.5], [0, 0]]
    ), "derivatives with jumps not correct"

    parameter_tracks.update(
        parameter_tracks.getitem_from_columns(new_derivatives, 0), 0
    )
    assert not parameter_tracks.initial
    assert np.isnan(parameter_tracks.last_extrapolation_error)
    assert parameter_tracks.last_n_jumped_frames == 0

    new_derivatives2 = parameter_tracks.calc_new_derivatives(
        parameter_tracks.getitem_from_columns(columns, [2]), 0
    )
    assert np.all(
        new_derivatives2[0][0] == [[2, 0], [1, -1], [0, -2], [0, 0]]
    ), "acceleration derivatives not correct"
    parameter_tracks.update(
        parameter_tracks.getitem_from_columns(new_derivatives2, 0), 0
    )
    assert np.isnan(parameter_tracks.last_extrapolation_error)
    assert parameter_tracks.last_n_jumped_frames == 0

    new_derivatives3 = parameter_tracks.calc_new_derivatives(
        parameter_tracks.getitem_from_columns(columns, [3]), 0
    )
    assert np.all(
        new_derivatives3[0][0] == [[3, 1], [1, 1], [0, 2], [0, 4]]
    ), "third derivatives not correct"
    parameter_tracks.update(
        parameter_tracks.getitem_from_columns(new_derivatives3, 0), 0
    )
    assert parameter_tracks.last_extrapolation_error == 4
    assert parameter_tracks.get_score() == 1 - 4 / 5

    parameter_tracks.update(
        parameter_tracks.getitem_from_columns(new_derivatives3, 0), 1
    )
    assert parameter_tracks.last_n_jumped_frames == 1
    ########################

    #### Testing is_valid_updates ####
    parameter_tracks.derivatives = np.array([[0, 0], [1, 0], [2, 0], [0, 0]])
    parameter_tracks.last_extrapolation_error = 0
    parameter_tracks.last_n_jumped_frames = 0
    new_derivatives = parameter_tracks.calc_new_derivatives(columns, 0)
    is_valid = parameter_tracks.is_valid_updates(new_derivatives)
    # print(is_valid)
    # print(new_derivatives.transpose([1, 0, 2]))
    assert np.all(
        is_valid == [False, False, True, False, False]
    ), "is_valid_updates not correct"

    is_valid, updated_tracks, scores = parameter_tracks.validate_and_update(columns, 0)
    assert len(updated_tracks) == 1
    assert scores[0] == 0.8
    ########################

    #### Testing is_valid_updates with_initial ####
    parameter_tracks_initial = particle_tracking.Parameter_derivative_tracking(
        "coordinates",
        limits=[[0, -1], [0, 2]],
        max_extrapolation_error=1.9,
        n_derivatives=2,
        initial_limits=[[np.nan, -1], [np.nan, 4]],
        initial_derivatives=[[4, 2]],
    )
    assert np.all(
        parameter_tracks_initial.static_attributes["notnan_initial_limits"]
        == [[False, True], [False, True], [False, False]]
    )
    assert np.all(
        parameter_tracks_initial.static_attributes["initial_limits"] == [np.inf, 4]
    )
    assert np.all(
        parameter_tracks_initial.static_attributes["initial_derivatives"] == [[4, 2]]
    )

    new_parameter_tracks_initial = parameter_tracks_initial.create_new_track(
        parameter_tracks_initial.getitem_from_columns(columns, [0])
    )
    assert np.all(new_parameter_tracks_initial.derivatives == [[0, 0], [4, 2], [0, 0]])
    new_derivatives_initial = new_parameter_tracks_initial.calc_new_derivatives(
        parameter_tracks_initial.getitem_from_columns(columns, [4]), 0
    )
    assert np.all(new_derivatives_initial[0][0] == [[4, 2], [4, 2], [0, 0], [0, 0]])
    is_valid = new_parameter_tracks_initial.is_valid_updates(new_derivatives_initial)
    assert is_valid[0]

    new_parameter_tracks_initial.update(
        new_parameter_tracks_initial.getitem_from_columns(new_derivatives_initial, 0), 0
    )
    assert np.all(new_parameter_tracks_initial.derivatives == [[4, 2], [4, 2], [0, 0]])
    ########################

    #### Testing ordering ####
    parameter_tracks1 = parameter_tracks.copy()
    parameter_tracks1.derivatives = np.array([[0, 0], [1, 0], [2, 0], [0, 0]])
    parameter_tracks1.last_extrapolation_error = 0
    parameter_tracks2 = parameter_tracks.copy()
    parameter_tracks2.derivatives = np.array([[0, 0], [1, 0], [2, 0], [2, 0]])
    parameter_tracks2.last_extrapolation_error = 0
    assert parameter_tracks1 < parameter_tracks2
    ########################


def test_parameter_derivative_sub_tracker():
    # coordinates = np.array(
    #     [
    #         [0, 0],
    #         [1, 0],
    #         [2, 0],
    #         [2, 0],
    #     ]
    # )
    # intensity = np.array([1, 2, 3.1, 6])
    # true_tracked_particles = pd.DataFrame(
    #     {"x": coordinates[:, 0], "y": coordinates[:, 1], "intensity": intensity}
    # )
    # true_tracked_particles.insert(0, "id_track", 1)
    # ptv3py.set_coordinate_columns(true_tracked_particles)

    # base_tracker = particle_tracking.Parameter_derivative_tracking(
    #     "coordinates",
    #     n_derivatives=2,
    #     limits=[[0, -1], [0, 2]],
    #     max_extrapolation_error=2.1,
    #     extrapolation_error_scaling=5,
    # )
    # error_scaling = 2
    # intensity_tracker = particle_tracking.Parameter_derivative_tracking(
    #     "intensity",
    #     limits=[[0, 2]],
    #     n_derivatives=1,
    #     max_extrapolation_error=-1,
    #     extrapolation_error_scaling=error_scaling,
    # )
    # base_tracker.add_sub_tracker(intensity_tracker)

    # parameter_track = base_tracker.create_new_track(true_tracked_particles.loc[[0]])
    # is_valid, updated_tracks, scores = parameter_track.validate_and_update(
    #     true_tracked_particles.loc[[1]], 0
    # )
    # sub_new_derivatives = (
    #     updated_tracks[0]
    #     .sub_trackers[0]
    #     .calc_new_derivatives(true_tracked_particles.loc[[2, 3]], 0)
    # )
    # assert np.all(
    #     np.abs(sub_new_derivatives - [[[3.1], [6]], [[1.1], [4]], [[0.1], [3]]]) < 1e-7
    # )
    # is_valid, updated_tracks, scores = updated_tracks[0].validate_and_update(
    #     true_tracked_particles.loc[[2, 3]], 0
    # )
    # assert np.all(is_valid == [True, False])
    # assert scores[0] == 2 - 0.1 / error_scaling

    # second test
    coordinates = np.array(
        [
            [0, 0],
            [1, 1],
            [2, 2],
            [3, 1],
            [4, 2],
        ]
    )
    true_tracked_particles = pd.DataFrame(
        {"x": coordinates[:, 0], "y": coordinates[:, 1], "intensity": [1, 2, 2.5, 3, 4]}
    )
    true_tracked_particles.insert(0, "frame", np.arange(5))
    true_tracked_particles.insert(0, "id_track", 1)
    ptv3py.set_coordinate_columns(true_tracked_particles)

    #### Testing sub parameter tracker #######
    parameter_tracks_base = particle_tracking.Parameter_derivative_tracking(
        "coordinates",
        limits=[[0, -1], [0, 2]],
        max_extrapolation_error=2.1,
        n_derivatives=2,
        extrapolation_error_scaling=5,
    )
    sub_tracker = particle_tracking.Parameter_derivative_tracking(
        "intensity",
        n_derivatives=1,
        extrapolation_error_scaling=2,
        jumped_frames_penalty=1,
    )
    assert sub_tracker.static_attributes["jumped_frames_penalty"] == 1
    parameter_tracks_base.add_sub_tracker(sub_tracker)
    assert sub_tracker.parent is parameter_tracks_base
    assert sub_tracker.static_attributes["jumped_frames_penalty"] == 0

    columns = parameter_tracks_base.extract_columns(true_tracked_particles)
    assert np.all(
        columns[1][0][0] == true_tracked_particles["intensity"].to_numpy()[:, None]
    )

    parameter_tracks = parameter_tracks_base.create_new_track(
        true_tracked_particles.loc[[0]]
    )
    assert np.all(parameter_tracks.sub_trackers[0].derivatives == [[1], [0]])
    assert parameter_tracks.get_score() == 0 + 0

    new_derivatives = parameter_tracks.calc_new_derivatives(
        parameter_tracks.getitem_from_columns(columns, [1]), 0
    )
    parameter_tracks.update(
        parameter_tracks.getitem_from_columns(new_derivatives, 0), 0
    )
    assert np.all(parameter_tracks.sub_trackers[0].derivatives == [[[2], [1], [0]]])

    new_derivatives = parameter_tracks.calc_new_derivatives(
        parameter_tracks.getitem_from_columns(columns, [2]), 0
    )
    parameter_tracks.update(
        parameter_tracks.getitem_from_columns(new_derivatives, 0), 0
    )
    assert np.all(
        parameter_tracks.sub_trackers[0].derivatives == [[[2.5], [0.5], [-0.5]]]
    )
    assert parameter_tracks.get_score() == 0 + (1 - 0.5 / 2)


def test_parameter_derivative_tracker_negative_n_derivatives():
    true_tracked_particles = pd.DataFrame({"intensity": [3, 4, 5]})
    # true_tracked_particles.insert(0, "frame", np.arange(5))
    # true_tracked_particles.insert(0, "id_track", 1)
    # ptv3py.set_coordinate_columns(true_tracked_particles)

    # checking score of negative n_dimensions
    parameter_tracks = particle_tracking.Parameter_derivative_tracking(
        "intensity",
        n_derivatives=-1,
        extrapolation_error_scaling=5,
    )
    assert (
        parameter_tracks.static_attributes["value_score"] is True
    ), "value_score should be true for negative n_derivatives"
    assert (
        parameter_tracks.static_attributes["n_derivatives"] == 0
    ), "n_derivatives should not be negative"
    parameter_tracks = parameter_tracks.create_new_track(
        true_tracked_particles.loc[[0]]
    )
    assert parameter_tracks.get_score() == 3 / 5, "score is wrong"


def test_tracking_visualizer():
    true_tracked_particles = get_simulation()

    pss = []

    # no tracks
    ps = ptv3py.visualise_tracked_particles([])
    assert ps is None

    ps = ptv3py.visualise_tracked_particles(
        true_tracked_particles,
        accumulate_tracks=True,
        axis_equal=True,
        verbose=2,
        max_speed=2,
    )
    ps.set_frame(5)
    ps.pick_data(track_id=2, ind=-1)
    pss.append(ps)

    # with cmap information
    ps = ptv3py.visualise_tracked_particles(
        true_tracked_particles, cmap="inferno", verbose=2
    )
    ps.set_frame(-1)
    ps.colorbar()
    pss.append(ps)

    # 3D
    true_tracked_particles_3D = true_tracked_particles.copy()
    true_tracked_particles_3D.insert(
        len(true_tracked_particles_3D.columns),
        "coordinates",
        np.random.randn(len(true_tracked_particles_3D)) * 0.01,
        allow_duplicates=True,
    )
    ps = ptv3py.visualise_tracked_particles(true_tracked_particles_3D, verbose=2)
    ps.set_frame(5)
    ps.pick_data(track_id=2, ind=-1)
    pss.append(ps)

    # 3D to 2D
    ps = ptv3py.visualise_tracked_particles(
        true_tracked_particles_3D, threed2twod=True, max_speed=2, verbose=2
    )
    ps.set_frame(5)
    ps.pick_data(track_id=2, ind=-1)
    pss.append(ps)

    # 3D with 2D data
    particles = true_tracked_particles.copy()
    particles.drop(columns="id_track", inplace=True)
    particles["camera"] = 0
    ps = ptv3py.visualise_tracked_particles(
        true_tracked_particles_3D, particles_2d=particles, verbose=2
    )
    ps.set_frame(5)
    ps.pick_data(track_id=2, ind=-1)
    pss.append(ps)

    # plt.show()
    for ps in pss:
        plt.close(ps)


def test_tracking_helper():
    true_tracked_particles = get_simulation()
    particles = true_tracked_particles.drop(columns="id_track")
    particles.insert(len(particles.columns), "test", np.arange(len(particles)))
    tptf, ps = ptv3py.particle_tracking_parameters_helper(
        particles=particles,
    )
    current_tracking_kwargs = tracking_kwargs.copy()
    current_tracking_kwargs["coordinate_parameter_tracker"] = dict(
        n_derivatives=1, limits=[0, 1.9], max_extrapolation_error=0.2
    )
    current_tracking_kwargs["track_columns"] = ["test"]
    tptf.set_parameters(**current_tracking_kwargs)

    # checking that setting parameter tracks base works
    assert ps.picking_variables["tracking_kwargs"]["max_speed"] == 1.9
    assert np.isnan(ps.picking_variables["tracking_kwargs"]["max_acceleration"])

    tptf.set_framelims([3, 9])
    tptf.track_particles()
    # plt.show()
    plt.close(ps)
    plt.close(tptf)


def test_tracking_helper_debug_mode():
    frames = np.arange(5)
    coordinates1 = np.array(
        [
            [0, 0],
            [1, 0],
            [2, 0],
            [3, 0.1],
            [4, -1],
        ]
    )
    coordinates2 = np.array(
        [
            [0, 0],
            [1, 0],
            [2, 0],
            [3, -0.1],
            [4, 1],
        ]
    )
    true_tracked_particles1 = pd.DataFrame(
        {"x": coordinates1[:, 0], "y": coordinates1[:, 1]}
    )
    true_tracked_particles1.insert(0, "frame", frames)
    true_tracked_particles1.insert(0, "id_track", 1)
    true_tracked_particles1["intensity"] = 1
    true_tracked_particles2 = pd.DataFrame(
        {"x": coordinates2[:, 0], "y": coordinates2[:, 1]}
    )
    true_tracked_particles2.insert(0, "frame", frames)
    true_tracked_particles2.insert(0, "id_track", 2)
    true_tracked_particles2["intensity"] = 2

    # this is just to check that single particle tracks in the end does not create infinite loops
    true_tracked_particles3 = pd.DataFrame({"x": [-1], "y": [-1]})
    true_tracked_particles3.insert(0, "frame", 4)
    true_tracked_particles3.insert(0, "id_track", 3)
    true_tracked_particles3["intensity"] = 3

    true_tracked_particles = pd.concat(
        (true_tracked_particles1, true_tracked_particles2, true_tracked_particles3)
    )
    true_tracked_particles.insert(1, "id", np.arange(len(true_tracked_particles)))
    ptv3py.set_coordinate_columns(true_tracked_particles)
    current_tracking_kwargs = {
        "extrapolation_error_scaling": None,
        "track_columns": ["intensity"],
    }

    particles = true_tracked_particles.drop(columns="id_track")

    tptf, ps = ptv3py.particle_tracking_parameters_helper(
        particles=particles, axis_equal=True
    )

    tptf.set_parameters(**current_tracking_kwargs, verbose=-0.1)

    tptf.track_particles()
    # plt.show()
    plt.close(tptf)
    plt.close(ps)
