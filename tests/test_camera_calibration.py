import numpy as np
import matplotlib.pyplot as plt
from ptv3py import camera_calibration_module

import os.path as osp

import functools

np.random.seed(0)

camera_calibration_file = osp.join(osp.dirname(__file__), "test_camera_calibration.npy")


def get_camera_calibration():
    camera_calibration = camera_calibration_module.CameraCalibration.load(
        camera_calibration_file
    )
    image_shape = camera_calibration._threed_lines.shape[1:3]
    return camera_calibration, image_shape


def test_camera_calibration():
    camera_calibration, image_shape = get_camera_calibration()
    shape = (4, 5)
    test_points = (
        np.random.rand(*shape, 2)
        * np.array(camera_calibration._threed_lines.shape[1:3])[::-1]
    )
    # run with camera as single number
    projected_points = camera_calibration(test_points, camera=0)
    output_shape = projected_points.shape[: len(shape)]
    assert output_shape == shape, "running with multiple points does not work correctly"

    # run with camera as full matrix
    camera = np.random.rand(*shape) > 0.5
    projected_points = camera_calibration(test_points, camera=camera)

    output_shape = projected_points.shape[: len(shape)]
    assert output_shape == shape, "running with multiple points does not work correctly"


def test_quick_interpolation_function():
    camera_calibration, image_shape = get_camera_calibration()
    camera = 0
    row = int(image_shape[0] * np.random.rand())
    col = int(image_shape[1] * np.random.rand())

    n_interpolation_points = 5
    x, y = np.meshgrid(
        np.linspace(col, col + 1, n_interpolation_points),
        np.linspace(row, row + 1, n_interpolation_points),
    )
    points = np.dstack((x, y))

    # Checking if they return approximately the same thing
    quick_lines = camera_calibration.quick_interpolation(points, camera, row, col)
    lines = camera_calibration(points, camera)
    diffs = np.linalg.norm(quick_lines - lines, axis=(-2, -1))

    # Does not seem to be exactly the same thing
    assert np.all(diffs < 1e-4), "Errors are too large"

    # Looking at the edges where correct answer is known. Should be perfectly correct
    real_edge_lines = camera_calibration._threed_lines[
        camera, row : row + 2, col : col + 2
    ]
    extraction_tuple = ([[0, 0], [-1, -1]], [[0, -1], [0, -1]])

    diff1 = np.linalg.norm(
        quick_lines[extraction_tuple] - real_edge_lines, axis=(-2, -1)
    )
    assert np.all(diff1 == 0), "Edges of quick lines incorrect"

    diff2 = np.linalg.norm(lines[extraction_tuple] - real_edge_lines, axis=(-2, -1))
    assert np.all(diff2 == 0), "Edges of standard lines incorrect"


def test_camera_projection(use_quick_interpolation=True):
    camera_calibration, image_shape = get_camera_calibration()

    test_point = np.random.rand(2) * np.array(image_shape)[::-1]
    vector_length = np.random.rand() * 10

    for camera in range(2):
        if use_quick_interpolation:
            base_point = test_point.astype(int)
            interpolator = functools.partial(
                camera_calibration.quick_interpolation,
                camera=camera,
                row=base_point[1],
                col=base_point[0],
            )
        else:
            interpolator = functools.partial(camera_calibration.__call__, camera=camera)
        threed_line = interpolator(test_point)

        threed_points = threed_line[0] + threed_line[1] * vector_length
        projected_point = camera_calibration.project(
            threed_points, use_quick_interpolation=use_quick_interpolation
        )
        diff = test_point - projected_point[camera]
        assert np.all(
            diff < 1e-7
        ), "projection for camera {} seems wrong, max_error {}".format(
            camera, np.max(diff)
        )

    # just testing to run function with multiple points
    shape = (4, 5)
    threed_points = (np.random.rand(*shape, 3) - 0.5) * 10
    projected_points = camera_calibration.project(
        threed_points, use_quick_interpolation=use_quick_interpolation
    )
    output_shape = projected_points.shape[: len(shape)]
    assert output_shape == shape, "running with multiple points does not work correctly"


def test_visualise_calibration():
    camera_calibration, _ = get_camera_calibration()
    fig, ax = camera_calibration_module.visualise_camera_calibration(camera_calibration)
    # plt.show()
    plt.close(fig)
