"""
Module for preforming ptv calibration as explained in https://aip.scitation.org/doi/10.1063/1.5080743
"A simplified and versatile calibration method for multi-camera optical systems in 3D particle imaging"
"""
import os.path as osp
import matplotlib.pyplot as plt
import numpy as np

import cv2
from skimage import measure
from scipy import interpolate, optimize
import functools
from uncertainties import unumpy

from . import visualise_helpers


def get_chessboard_coordinates(chessboard_shape, chessboard_square_width, Z_position):
    """Return the real 3D coordinates of the chessboard pattern as
    [[X1 Y1 Z_position],
     [X2 Y2 Z_position],
     ...               ]
    """
    objp = np.zeros((chessboard_shape[0] * chessboard_shape[1], 3), np.float32)
    # creating meshgrid
    objp[:, :2] = np.mgrid[: chessboard_shape[0], : chessboard_shape[1]].T.reshape(
        -1, 2
    )

    ## World X-axis is to the right in the camera, Y-axis is up in the camera and
    ## Z-axis is towards the camera
    # center of pattern is origin for x and y axis
    objp[:, 0] = objp[:, 0] - (chessboard_shape[0] - 1) / 2
    objp[:, 1] = -(objp[:, 1] - (chessboard_shape[1] - 1) / 2)
    # scaling to size of chessboard square width
    objp[:, :2] *= chessboard_square_width
    objp[:, 2] = Z_position

    # plt.figure(2)
    # plt.plot(objp[:, 0], objp[:, 1], '-*r')
    # plt.plot(objp[0, 0], objp[0, 1], '*r', markersize=10)
    # plt.axis("equal")

    return objp


SUBPIXEL_CRITEREA = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)


def find_chessboard_corners(
    calibration_images,
    Z_positions,
    chessboard_shape,
    chessboard_square_width,
    verbose=False,
    camera_colors=None,
):
    """Finding chessboard corners and calculating their coorresponding 3D coordinates
    Arrays to store object points and image points from all the images.

    :param calibration_images: 4D numpy array
        Including the calibration images from all cameras at different positions:
        First dimension is the images at different Z positions
        Second dimension is the different cameras
        Third and fourth dimension is the image row and col dimensions
    :param Z_positions: 1D array of floats
        Z positions used (in mm or same unit as chessboard_square_width)
    :param chessboard_shape: (r, c) tuple
        Number of rows and cols of chessboard intersection points
    :param chessboard_square_width: float
        size of chessboard squares (mm or same unit Z_positions)
    :param verbose: print/show extra stuff
    :param camera_colors: give optional colors for verbose output
    :returns: tuple of two arrays. First is the image points of the found chessboard corners and second are the object point of the 3D points for each corresponding corner. Both arrays are 4 dimensional.
        First dimension is the points for each Z_position
        Second dimension is for each camera
        Third dimension is the point in the chessboard
        Fourth dimension is the point coordinates in the images (x, y) or (X, Y, Z)

    """

    shape = calibration_images.shape
    n_cameras = shape[1]
    chessboard_shape = tuple(chessboard_shape)  # must be tuple

    if Z_positions.dtype != np.float64:
        Z_positions = Z_positions.astype(np.float64)

    n_corners = np.prod(chessboard_shape)
    objpoints = np.empty(shape[:2] + (n_corners, 3))  # 3d point in real world space
    imgpoints = np.empty(shape[:2] + (n_corners, 2))  # 2d point in image plane
    Z_not_used = set()
    if verbose:
        fig, axs = plt.subplots(1, n_cameras, figsize=(5 * n_cameras, 4))
        for camera in range(n_cameras):
            axs[camera].set_title("Camera {}".format(camera + 1))

        if camera_colors is None:
            camera_colors = visualise_helpers.colorcycle[:n_cameras]

    for i, calibration_image in enumerate(calibration_images):
        Z_position = Z_positions[i]

        # Find the chess board corners
        for camera in range(n_cameras):
            ret, corners = cv2.findChessboardCorners(
                calibration_image[camera], chessboard_shape
            )

            # If found, add object points, image points (after refining them)
            if ret:
                objpoints[i, camera] = get_chessboard_coordinates(
                    chessboard_shape, chessboard_square_width, Z_position
                )

                cv2.cornerSubPix(
                    calibration_image[camera],
                    corners,
                    (11, 11),
                    (-1, -1),
                    SUBPIXEL_CRITEREA,
                )
                imgpoints[i, camera] = corners[:, 0, :]
            else:
                # raise ValueError(
                #     "Not all chessboard corners were found in Z_position {} and camera {}".format(
                #         Z_position, camera
                #     )
                # )
                print(
                    "Warning: Z_level {} in camera {} not used.".format(
                        Z_position, camera
                    )
                )
                Z_not_used.add(i)
                break
            if verbose:
                # Draw and display the corners
                # plt.subplot(1, n_cameras, camera + 1)
                # plt.title("Camera {}".format(camera + 1))
                [artist.remove() for artist in axs[camera].images]
                [artist.remove() for artist in axs[camera].lines]

                axs[camera].imshow(calibration_image[camera], cmap="gray")
                axs[camera].plot(
                    corners[:, 0, 0],
                    corners[:, 0, 1],
                    "-*",
                    color=camera_colors[camera],
                )
                axs[camera].plot(
                    corners[0, 0, 0],
                    corners[0, 0, 1],
                    "*",
                    color=camera_colors[camera],
                    markersize=10,
                )
        if verbose:
            axs[0].set_ylabel("Z = {:.1f}".format(Z_position))
            if isinstance(verbose, str):
                fig.savefig(osp.join(verbose, "finding_corners_Z{:02d}".format(i)))
            else:
                plt.pause(0.5)

    if verbose:
        plt.close(fig)
    valid_positions = np.ones(shape[0], dtype=bool)
    valid_positions[list(Z_not_used)] = False
    # valid_positions = np.array([not i in Z_not_used for i in range(shape[0])])
    # imgpoints = imgpoints[Z_used]
    # objpoints = objpoints[Z_used]
    return imgpoints, objpoints, valid_positions


def estimate_2d_projections(
    image_shape,
    point_correspondences,
    projection_deg=3,
    verbose=False,
    camera_colors=None,
    subplots_kwargs={},
):
    """Estimating transforms from 2D x y to 3D X Y coordinates"""
    shape = image_shape
    imgpoints, objpoints, valid_positions = point_correspondences
    imgpoints = imgpoints[valid_positions]
    objpoints = objpoints[valid_positions]
    n_calibration_planes, n_cameras = imgpoints.shape[:2]
    max_backprojection_error = 0

    y, x = np.mgrid[: shape[-2], : shape[-1]]
    all_pixels = np.dstack((x, y))
    threed_coordinates = np.empty(shape + (3,))

    if verbose:
        plot_lims = visualise_helpers.get_automated_plot_lims(
            objpoints.reshape((-1, 3))[..., :2]
        )
        fig, axs = plt.subplots(
            2, n_cameras, figsize=(5 * n_cameras, 8), **subplots_kwargs
        )
        for camera in range(n_cameras):
            axs[0, camera].set_title("Camera {}".format(camera + 1))
            axs[0, camera].set_aspect("equal", adjustable="box")
            axs[0, camera].set_xlim([0, image_shape[-1]])
            axs[0, camera].set_ylim([image_shape[-2], 0])

            axs[1, camera].set_aspect("equal", adjustable="datalim")
            axs[1, camera].set_xlim(plot_lims[0])
            axs[1, camera].set_ylim(plot_lims[1])
        if camera_colors is None:
            camera_colors = visualise_helpers.colorcycle[:2]

    for i in range(n_calibration_planes):
        for camera in range(n_cameras):
            poly = polyfit2d(
                imgpoints[i, camera], objpoints[i, camera, :, :2], projection_deg
            )

            # Checking backprojection error
            objpoints_backprojection = polyval2d(imgpoints[i, camera], poly)
            backprojection_error = np.linalg.norm(
                objpoints_backprojection - objpoints[i, camera, :, :2], axis=1
            )
            max_backprojection_error = max(
                max_backprojection_error, np.max(backprojection_error)
            )

            projected_pixels = polyval2d(all_pixels, poly)
            threed_coordinates[i, camera, :, :, :2] = projected_pixels
            threed_coordinates[i, camera, :, :, 2] = objpoints[i, camera, 0, 2]

            if verbose:
                [artist.remove() for artist in axs[0, camera].lines]
                axs[0, camera].plot(
                    *imgpoints[i, camera, :, :2].T, "*-", color=camera_colors[camera]
                )

                [artist.remove() for artist in axs[1, camera].lines]
                axs[1, camera].plot(*objpoints[i, camera, :, :2].T, "*-", color="Black")
                axs[1, camera].plot(
                    *objpoints_backprojection.T, "*-", color=camera_colors[camera]
                )

        if verbose:
            axs[1, 0].set_ylabel("Z = {:.1f}".format(objpoints[i, 0, 0, 2]))
            if isinstance(verbose, str):
                fig.savefig(osp.join(verbose, "2d_projections_Z{:02d}".format(i)))
            else:
                plt.pause(0.5)

    if verbose:
        # closing this figure does not work for some reason
        plt.close(fig)
    if max_backprojection_error > 2:
        print(
            "Warning: Unusually large backprojection error {}".format(
                max_backprojection_error
            )
        )
    if verbose:
        print("Maximal backprojection error: {}".format(max_backprojection_error))
    return threed_coordinates


def estimated_threed_lines(threed_coordinates, verbose=False):
    """
    Estimate the 3D lines corresponding to light rays for each pixel assuming that the light rays are straight lines
    Take a big array with 5 dimensions and estimate a line with 2 parameters for each camera, each pixel and each X, Y and Z.
    Example:

    If the input array has shape (n_calibration_images, n_cameras, r, c, 3) the output will be (n_cameras, r, c, 2, 3)
    Last two dimensions include a 3D point and a 3D vector for the line.
    """
    shape = threed_coordinates.shape

    # extracting the shape params
    n_calibration_images, n_cameras, r, c = shape[:-1]

    # using least squares estimation for the lines as usual
    # reshaping so that all input values are found in the final dimension same place
    input_array = threed_coordinates.reshape((n_calibration_images, -1, 3))
    # the -1 above should be the first dimension for simple looping
    input_array = np.transpose(input_array, (1, 0, 2))
    threed_lines = np.empty((input_array.shape[0], 2, 3))
    # special line model that estimate the Total least squares of a line
    line = measure.LineModelND()

    max_line_residual_rms = 0
    # this loop is what takes the most time I think
    for i, vals in enumerate(input_array):
        # vals is a (n_calibration_images x 3) array
        line.estimate(vals)
        threed_lines[i] = np.array(line.params)

        line_residual_rms = np.sqrt(np.mean(np.square(line.residuals(vals))))
        max_line_residual_rms = max(max_line_residual_rms, line_residual_rms)
    threed_lines = threed_lines.reshape((n_cameras, r, c, 2, 3))

    # flipping lines in the negative Z-direction
    direction_multiplier = (threed_lines[:, :, :, 1, 2] > 0) * 2 - 1
    direction_multiplier = np.expand_dims(direction_multiplier, -1)
    threed_lines[:, :, :, 1, :] *= direction_multiplier

    if max_line_residual_rms > 6:
        print("Warning: Unusually large line rms, {}".format(max_line_residual_rms))
    if verbose:
        print("Max line estimation residual rms: {}".format(max_line_residual_rms))
    return threed_lines


def classic_camera_calibration(
    shape,
    point_correspondences,
    intrinsic_indicator=None,
    calibration_flags=None,
    verbose=False,
):
    imgpoints, objpoints, valid_positions = point_correspondences

    imgpoints = imgpoints[valid_positions]
    objpoints = objpoints[valid_positions]
    imgpoints = imgpoints.astype(np.float32)
    objpoints = objpoints.astype(np.float32)
    n_calibration_planes, n_cameras = imgpoints.shape[:2]

    if intrinsic_indicator is None:
        intrinsic_indicator = np.zeros(len(imgpoints), dtype=bool)
    intrinsic_indicator = intrinsic_indicator[valid_positions]

    # if calibration_flags is None:
    #     # default is to fix all but one parameter in intrisic matrix, the focal length
    #     calibration_flags = cv2.CALIB_FIX_PRINCIPAL_POINT + cv2.CALIB_FIX_ASPECT_RATIO

    calibrations = []
    for camera in range(n_cameras):
        current_imgpoints = imgpoints[:, camera]
        current_objpoints = objpoints[:, camera].copy()
        current_objpoints[:, :, -1] = 0

        (
            ret,
            intrinsic_matrix,
            distortion_coefficients,
            rvecs,
            tvecs,
        ) = cv2.calibrateCamera(
            current_objpoints,
            current_imgpoints,
            shape[::-1],
            None,
            None,
            flags=calibration_flags,
        )

        current_objpoints = objpoints[~intrinsic_indicator, camera]
        current_imgpoints = current_imgpoints[~intrinsic_indicator]

        retval, rvec, tvec = cv2.solvePnP(
            current_objpoints.reshape((-1, 3)),
            current_imgpoints.reshape((-1, 2)),
            # current_objpoints[0],
            # current_imgpoints[0],
            intrinsic_matrix,
            distortion_coefficients,
        )
        # print(intrinsic_matrix)
        # print(distortion_coefficients)
        # print()

        # rvec = np.mean(rvecs, axis=0)  # rotation vectors
        # tvec = np.mean(tvecs, axis=0)  # translation vectors
        # print(np.array(rvecs).shape, np.array(tvecs).shape)
        # print()
        # print(np.array(rvecs[:2]))
        # print(np.array(tvecs[:2]))
        # print()
        # print(rvec)
        # print(np.std(rvecs, axis=0))
        # print(tvec)
        # print(np.std(tvecs, axis=0))
        # print()
        # print()
        calibrations.append([intrinsic_matrix, distortion_coefficients, rvec, tvec])

    return calibrations


def camera_calibration(
    calibration_images,
    Z_positions,
    chessboard_shape,
    chessboard_square_width,
    projection_deg=3,
    method="classic",
    verbose=False,
    **kwargs,
):
    """
    :param calibration_images: 4D numpy array
        Including the calibration images from all cameras at different positions:
        First dimension is the images at different Z positions
        Second dimension is the different cameras
        Third and fourth dimension is the image dimensions
    :param Z_positions: 1D array of floats
        Z positions used (in mm or same unit as chessboard_square_width)
    :param chessboard_shape: (r, c) tuple
        Number of rows and cols of chessboard intersection points
    :param chessboard_square_width: float
        size of chessboard squares (mm or same unit Z_positions)
    :param projection_deg: int
        polynomial degree for transformation between 2D and 3D planes
    :param method: string
        which method to use for calibration. Options are:
            classic, with camera matrix and distortion coefficients
            advanced, estimates a 3D line from each pixel in each camera which can handle more distortions than the standard method
        default is standard.
    :param with_uncertainties: bool, int
        should uncertainties of the calibration be estimated. Only implemented for the advanced method so far.
        if integer this describes the number of iterations in the monte carlo error propagation.
    :param verbose: bool
        should more stuff be shown during run
    :returns: 4D numpy array
        Estimated vectors from each pixel in each camera
    """
    ######## Part one in implementation ###########
    point_correspondences = find_chessboard_corners(
        calibration_images,
        Z_positions,
        chessboard_shape,
        chessboard_square_width,
        verbose,
    )

    if method == "classic":
        # Using the standard Tsai intrinsic and extrinsic camera matrix with distortion coefficients
        camera_calibrations = classic_camera_calibration(
            calibration_images.shape[-2:],
            point_correspondences,
            verbose=verbose,
            **kwargs,
        )
        return CameraCalibrationClassic(camera_calibrations)
    elif method == "advanced":
        # method described in article with doi 10.1063/1.5080743
        # A simplified and versatile calibration method for multi-camera optical systems in 3D particle imaging
        ######## Part two in implementation ###########
        threed_coordinates = estimate_2d_projections(
            calibration_images.shape, point_correspondences, projection_deg, verbose
        )

        ######## Part three in implementation ###########
        threed_lines = estimated_threed_lines(threed_coordinates, verbose)

        # put directions so that lines are diverging
        first_points = threed_lines[:, :, :, 0]
        second_points = first_points + threed_lines[:, :, :, 1]
        areas = np.zeros((2, threed_lines.shape[0]))
        for i, points in enumerate([first_points, second_points]):
            mins = np.min(points, axis=(1, 2))
            maxs = np.max(points, axis=(1, 2))
            diffs = maxs - mins
            diffs = np.sort(diffs, axis=-1)[:, 1:]
            areas[i] = np.prod(diffs, axis=-1)
        increasing_area = np.sign(areas[1] - areas[0])
        increasing_area[increasing_area == 0] = 1
        threed_lines[:, :, :, 1] *= increasing_area[(slice(None),) + (np.newaxis,) * 3]
        return CameraCalibration(threed_lines)
    else:
        raise NotImplementedError("Method {} not implemented".format(method))


BASE_CALIBRATION_OUTPUT_PATH = "camera_calibration.npy"


class CameraCalibration:
    def __init__(self, input_array):
        """
        input array threed_lines has shape
        (n_cameras, rows, cols, 2, 3)
        last two dimension is a 3D point and a 3D vector desribing a line for each pixel in each camera
        """
        self._threed_lines = input_array
        self.n_cameras = input_array.shape[0]

    def __load__(self):
        """Load the interpolators, this typically takes some time"""
        self.line_interpolators = []
        shape = self._threed_lines.shape

        y, x = np.mgrid[: shape[1], : shape[2]]
        points = np.column_stack((x.flatten(), y.flatten()))
        for i in range(self.n_cameras):
            values = self._threed_lines[i].reshape((-1, 2, 3))
            self.line_interpolators.append(
                interpolate.LinearNDInterpolator(points, values)
            )

    def quick_interpolation(self, points, camera, row, col):
        """
        Quick interpolation of 4 lines in a 2D array square starting in top right corner with row, col
        Is not the exact same as calling the object for interpolation using scipy but very close and it will not have to load the interpolators that take the most time

        Points should have length two in final dimension as x and y
        all x >= col and x <= col + 1
        all y >= row and y <= row + 1
        if this is not the case the results will be (even more) incorrect
        """
        lines = self._threed_lines[camera, row : row + 2, col : col + 2]
        x = np.expand_dims(points[..., 0] - col, (-2, -1))
        y = np.expand_dims(points[..., 1] - row, (-2, -1))
        rev_x = 1 - x
        rev_y = 1 - y
        output_lines = (
            lines[0, 0] * rev_x * rev_y
            + lines[1, 0] * rev_x * y
            + lines[0, 1] * x * rev_y
            + lines[1, 1] * x * y
        )
        return output_lines

    def __call__(self, points, camera):
        """
        :param camera : int or 1darray of size n
            int for the camera or ndarray describing which elements in points that are for which camera.
            Should have the same shape as points except for the final dimension
        :param points : [m..]xnx2 array
            array where for interpolating n points for x and y coordinates in image to interpolate. Must have at least 2 dimensions
        :returns: [m..]xnx2x3 array
            same first dimension as number of points and then the interpolated line point and direction
        """
        points = np.asarray(points)
        assert (
            points.shape[-1] == 2
        ), "Last dimension of points must have length 2, has length {}".format(
            points.shape[-1]
        )
        single_dim = False
        if len(points.shape) == 1:
            single_dim = True
            points = points.reshape((-1, 2))
        n_shape = points.shape[:-1]
        if isinstance(camera, (float, int)):
            camera = np.ones(n_shape) * int(camera)
        else:
            assert (
                camera.shape == n_shape
            ), "camera shape must be equal to point shape except final dimension"

        estimated_threed_lines = np.empty(n_shape + (2, 3))
        for c in range(self.n_cameras):
            current_indexs = np.where(camera == c)
            try:
                estimated_threed_lines[current_indexs] = self.line_interpolators[c](
                    points[current_indexs]
                )
            except AttributeError:
                # lazy loading of interpolators
                self.__load__()
                estimated_threed_lines[current_indexs] = self.line_interpolators[c](
                    points[current_indexs]
                )
        if single_dim:
            estimated_threed_lines = estimated_threed_lines[0]
        return estimated_threed_lines

    def projection_error(point, interpolator, threed_point):
        line = interpolator(point)
        return np.linalg.norm(np.cross(line[0] - threed_point, line[1]))

    def project(self, threed_points, use_quick_interpolation=False):
        """Project 3D points into coordinates of the cameras"""
        threed_points = np.asarray(threed_points)
        assert threed_points.shape[-1] == 3
        orig_shape = None
        if len(threed_points.shape) != 2:
            orig_shape = threed_points.shape[:-1]
            threed_points = threed_points.reshape((-1, 3))

        image_shape = self._threed_lines.shape[1:3]
        y, x = np.mgrid[: image_shape[0], : image_shape[1]]

        projections = np.zeros((len(threed_points), self.n_cameras, 2))
        for camera in range(self.n_cameras):
            points = self._threed_lines[camera, :, :, 0]
            normals = self._threed_lines[camera, :, :, 1]

            for i in range(len(threed_points)):
                threed_point = threed_points[i]
                errors = np.linalg.norm(
                    np.cross(points - threed_point, normals), axis=-1
                )
                min_row, min_col = np.unravel_index(np.argmin(errors), image_shape)
                if min_row > 0 and min_row < image_shape[0] - 1:
                    row = min_row - int(
                        errors[min_row - 1, min_col] < errors[min_row + 1, min_col]
                    )
                elif min_row == 0:
                    row = min_row
                else:
                    row = min_row - 1

                if min_col > 0 and min_col < image_shape[1] - 1:
                    col = min_col - int(
                        errors[min_row, min_col - 1] < errors[min_row, min_col + 1]
                    )
                elif min_col == 0:
                    col = min_col
                else:
                    col = min_col - 1

                if use_quick_interpolation:
                    interpolator = functools.partial(
                        self.quick_interpolation, camera=camera, row=row, col=col
                    )
                else:
                    interpolator = functools.partial(self.__call__, camera=camera)

                bounds = [(col, col + 1), (row, row + 1)]

                ## Power 3 gives quite a good guess estimator
                edge_features = errors[row : row + 2, col : col + 2] ** 3
                edge_features_sum = np.sum(edge_features)
                guess = [
                    col
                    + (edge_features[0, 0] + edge_features[1, 0]) / edge_features_sum,
                    row
                    + (edge_features[0, 0] + edge_features[0, 1]) / edge_features_sum,
                ]

                res = optimize.minimize(
                    CameraCalibration.projection_error,
                    guess,
                    args=(interpolator, threed_point),
                    bounds=bounds,
                )
                # assert res.success, "projection error minimisation failed"
                projections[i, camera] = res.x
        if orig_shape is not None:
            projections = projections.reshape(orig_shape + (self.n_cameras, 2))
        return projections

    @staticmethod
    def load(path=None):
        """Reads a calibration from file"""
        if path is None:
            path = BASE_CALIBRATION_OUTPUT_PATH
        if not osp.isfile(path):
            raise FileNotFoundError("Calibration file {} not found".format(path))
        input_array = np.load(path)
        if len(input_array.shape) == 1:
            return CameraCalibrationClassic.load(path)
        else:
            return CameraCalibration(input_array)

    def save(self, path=None):
        """Saves a calibration to file"""
        if path is None:
            path = BASE_CALIBRATION_OUTPUT_PATH
        np.save(path, self._threed_lines)


class CameraCalibrationClassic:
    def __init__(self, camera_calibrations):
        """
        camera_calibrations is a list of lists for each cameras where each sublist includes:
        [rvec, tvec, intrinsic_matrix, distortion_coefficients] for the cameras
        Rvec and tvec are concepts from the opencv library
        """
        self.n_cameras = len(camera_calibrations)
        self.camera_calibrations = camera_calibrations
        for camera_calibration in camera_calibrations:
            camera_calibration[2] = cv2.Rodrigues(camera_calibration[2])[0]

        self.camera_centers = [
            -np.sum(R.T * tvec, axis=1) for [K, dc, R, tvec] in camera_calibrations
        ]

    def __call__(self, points, camera):
        """
        same as for CameraCalibration.__call__
        """
        points = np.asarray(points)
        assert (
            points.shape[1] == 2
        ), "Second dimension of points must have length 2, has length {}".format(
            points.shape[1]
        )
        points = np.array(points)
        points = np.expand_dims(points, 1).astype(np.float32)
        n = points.shape[0]
        if isinstance(camera, (float, int)):
            camera = np.ones(n, dtype=int) * int(camera)
        else:
            camera = np.array(camera).flatten().astype(int)

        estimated_threed_lines = np.empty((n, 2, 3))
        for c in range(self.n_cameras):
            current_inds = camera == c
            current_n_points = np.sum(current_inds)
            if current_n_points == 0:
                continue
            cameraMatrix, distCoeffs, R, tvec = self.camera_calibrations[c]
            camera_center = self.camera_centers[c]
            current_camera_centers = np.tile(camera_center, (current_n_points, 1, 1))

            undistorted_points = cv2.undistortPoints(
                points[current_inds], cameraMatrix, distCoeffs
            )
            # removing extra dimension (opencv thing)
            undistorted_points = undistorted_points[:, 0]
            # making points homogeneous
            camera_plane_points = np.column_stack(
                (undistorted_points, np.ones(current_n_points))
            )
            # Estimating the ray projections
            current_ray_directions = (R.T @ camera_plane_points.T).T
            current_ray_directions = np.expand_dims(current_ray_directions, 1)

            current_threed_lines = np.hstack(
                (current_camera_centers, current_ray_directions)
            )
            estimated_threed_lines[current_inds] = current_threed_lines

        return estimated_threed_lines

    @staticmethod
    def load(path=None):
        """Reads a calibration from file"""
        if path is None:
            path = BASE_CALIBRATION_OUTPUT_PATH
        if not osp.isfile(path):
            raise FileNotFoundError("Calibration file {} not found".format(path))
        input_array = np.load(path)
        if len(input_array) % 16 != 0:
            raise ValueError("This is not a classic camera calibration")

        input_array = input_array.reshape(-1, 20)
        camera_calibrations = []
        for row in input_array:
            K = row[:9].reshape((3, 3))
            rvec = row[9:12].reshape((3, 1))
            tvec = row[12:15].reshape((3, 1))
            dc = row[15:].reshape((1, -1))
            camera_calibrations.append([K, dc, rvec, tvec])
        return CameraCalibrationClassic(camera_calibrations)

    def save(self, path=None):
        """Saves a calibration to file"""
        if path is None:
            path = BASE_CALIBRATION_OUTPUT_PATH
        output_array = []
        for K, dc, R, tvec in self.camera_calibrations:
            output_array.append(
                np.concatenate(
                    [K.ravel(), cv2.Rodrigues(R)[0].ravel(), tvec.ravel(), dc.ravel()]
                )
            )
        output_array = np.concatenate(output_array)
        np.save(path, output_array)


def visualise_camera_calibration(
    camera_calibration,
    plot_lims=None,
    axis_equal=True,
    n_pixels_showed=100,
    line_length=20,
    zaxis=1,
    colors=None,
    arrow_length_ratio=0.3,
    pivot="middle",
    **kwargs,
):
    fig, ax = plt.subplots(**kwargs, subplot_kw=dict(projection="3d"))
    # ax = fig.gca(projection="3d")

    plot_order = [zaxis - 2, zaxis - 1, zaxis]

    n_cameras = camera_calibration.n_cameras
    if colors is None:
        colors = visualise_helpers.colorcycle[:n_cameras]

    if isinstance(camera_calibration, CameraCalibration):
        # print("Visualise advanced camera calibration")
        threed_lines = camera_calibration._threed_lines
        shape = threed_lines.shape
        n_pixels = np.prod(shape[1:3])

        # take every nth pixel so that only 100 pixels per camera is showed
        nth = int(np.sqrt(n_pixels / n_pixels_showed))
        start = int(round(nth / 2))
        threed_lines = threed_lines[:, start::nth, start::nth]
        # reshape to merge the rows and columns
        threed_lines = threed_lines.reshape((n_cameras, -1, 2, 3))

        # Quiver the arrows
        all_coordinates = []
        for camera in range(n_cameras):
            coordinates = threed_lines[camera][:, 0].T
            directions = threed_lines[camera][:, 1].T

            ax.quiver(
                *coordinates[plot_order],
                *directions[plot_order],
                length=line_length,
                pivot=pivot,
                arrow_length_ratio=arrow_length_ratio,
                color=colors[camera],
            )

            all_coordinates.extend([coordinates.T, (coordinates + directions).T])

        if plot_lims is None:
            plot_lims = visualise_helpers.get_automated_plot_lims(
                np.concatenate(all_coordinates)
            )
    else:
        # print("Visualise classic camera calibration")
        coordinates = [[[0, 0, 0]]]
        for camera in range(n_cameras):
            (
                intrinsic_matrix,
                distortion_coefficients,
                rmat,
                tvec,
            ) = camera_calibration.camera_calibrations[camera]

            camera_position = camera_calibration.camera_centers[camera]
            camera_up = rmat[1]
            camera_direction = rmat[-1]
            # print()
            # print(camera_position)
            # print(camera_direction)
            # theta = np.arccos(camera_direction[2]) * 180 / np.pi
            # phi = np.arctan2(camera_direction[2], camera_direction[1]) * 180 / np.pi
            # print(theta, phi)

            abs_camera_position = np.linalg.norm(camera_position)

            ax.plot(
                *camera_position[plot_order],
                "o",
                color=colors[camera],
                markersize=3,
            )

            camera_direction_lines = np.array(
                [
                    camera_position,
                    camera_position + abs_camera_position / 10 * camera_direction,
                ]
            ).T
            ax.plot(
                *camera_direction_lines[plot_order],
                "-o",
                color=colors[camera],
                markersize=2,
            )
            coordinates.append(camera_direction_lines.T)
            camera_up_lines = np.array(
                [
                    camera_position,
                    camera_position + abs_camera_position / 20 * camera_up,
                ]
            ).T
            ax.plot(
                *camera_up_lines[plot_order],
                color=colors[camera],
            )
            coordinates.append(camera_up_lines.T)

        # ax.plot(
        #     objpoints[0, 0, :, 0], objpoints[0, 0, :, 1], objpoints[0, 0, :, 2], "-g*"
        # )
        # ax.plot(
        #     objpoints[0, 0, 0, 0],
        #     objpoints[0, 0, 0, 1],
        #     objpoints[0, 0, 0, 2],
        #     "g*",
        #     markersize=10,
        # )

        # length = 300
        # ax.set_xlim([-70, -70 + length])
        # ax.set_ylim([-40, -40 + length])
        # ax.set_zlim([0, 0 + length])
        # ax.set_xlabel("x")
        # ax.set_ylabel("y")
        # ax.set_zlabel("z")
        if plot_lims is None:
            plot_lims = visualise_helpers.get_automated_plot_lims(
                np.concatenate(coordinates)
            )

    axis_labels = ["X", "Y", "Z"]
    ax.set_xlabel(axis_labels[plot_order[0]])
    ax.set_ylabel(axis_labels[plot_order[1]])
    ax.set_zlabel(axis_labels[plot_order[2]])

    if axis_equal:
        plot_lims = visualise_helpers.plot_lims_equal(plot_lims)

    ax.set_xlim(plot_lims[plot_order[0]])
    ax.set_ylim(plot_lims[plot_order[1]])
    ax.set_zlim(plot_lims[plot_order[2]])
    return fig, ax


def _calc_X(x, deg):
    """Calculating the X matrix for X * beta = y for 2D polynomial model"""
    # calculating all combinations of powers of x and y that max sum up to deg
    degs = []
    for i in range(deg + 1):
        for j in range(deg + 1 - i):
            degs.append([i, j])
    degs = np.array(degs).T
    # calculating A matrix using broadcasting
    X = x[:, :1] ** degs[:1, :] * x[:, -1:] ** degs[-1:, :]
    return X


def polyfit2d(x, y, deg, return_uncertainties=False):
    """
    Make a 2D polyfit of certain degree, similar to:
    y_k = sum^deg_{ij} c_{kij} x_1^i x_2^j
    estimate the coefficients c_{kij}
    using least squares estimation from the basic equation
    Ax = y where x are the coefficients for the polynomial

    :param x: 2D array
        first column is the first input parameter second column the second parameter
    :param y: array
        must have the same number of rows as x but can have different number of columns
    :returns: coefficients to polynomial
    """
    x = np.asarray(x)
    y = np.asarray(y)
    assert x.shape[-1] == 2, "Last dimension of x must have size 2"
    x = x.reshape((-1, 2))
    y = y.reshape((-1, 2))

    X = _calc_X(x, deg)
    beta, residuals, rank, s = np.linalg.lstsq(X, y, rcond=None)

    if not return_uncertainties:
        return beta
    else:
        sigma_epsilon2 = residuals / (y.size - 1)
        var_beta = np.diag(np.linalg.inv(X.T @ X))[np.newaxis, 1] * sigma_epsilon2
        return unumpy.uarray(beta, np.sqrt(var_beta))


def polyval2d(x, beta):
    """
    Polyval with output from polyfit
    Last dimension of x should have length 2
    """
    x = np.asarray(x)
    beta = np.asarray(beta)
    assert x.shape[-1] == 2, "Last dimension of x must have size 2"
    # finding degree of polynomial by continuous substraction of larger numbers since
    # n_coeff = sum_i^{deg+1} i
    counter = beta.shape[0]
    deg = -1
    while counter > 0:
        deg += 1
        counter -= deg + 1

    out_shape = x.shape[:-1]
    n_channels = beta.shape[1]

    x = x.reshape((-1, 2))
    X = _calc_X(x, deg)
    y = X @ beta
    y = y.reshape(out_shape + (n_channels,))
    return y
