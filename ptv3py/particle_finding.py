"""
Module for finding particles in PTV images

One algorithm is implemented for this that is a simple threshold and centroid calculation. One can add gradient thresholding to avoid some overlaps as well.
"""
import cv2
import numpy as np
import pandas as pd
from typing import Sequence

import functools
import multiprocessing

try:
    from tqdm import tqdm
except ModuleNotFoundError:
    pass

from . import helpers


def max_sum_area_features_func(image, n_labels, labels):
    """Example extra_features_func to thresholding_single_frame"""
    max_intensities = np.zeros(n_labels - 1, dtype=int)
    sum_intensities = np.zeros(n_labels - 1, dtype=int)
    pixel_areas = np.zeros(n_labels - 1, dtype=int)
    for label in range(1, n_labels):
        area = labels == label
        max_intensities[label - 1] = np.max(image[area])
        sum_intensities[label - 1] = np.sum(image[area])
        pixel_areas[label - 1] = np.sum(area)
    return pd.DataFrame(
        {
            "max_intensity": max_intensities,
            "sum_intensity": sum_intensities,
            "pixel_area": pixel_areas,
        }
    )


# corresponding `special_merge_functions` for `merge_particle_clusters` if max_sum_area_features_func was used
max_sum_area_features_func.special_merge_functions = {
    "max_intensity": np.max,
    "sum_intensity": np.sum,
    "pixel_area": np.sum,
}


def divide_label_subpeaks_gradient(
    image, n_labels, labels, gradient_threshold, relative_gradient_threshold, saturation
):
    if relative_gradient_threshold:
        # sqrt of image values usually correspond to noise
        gradient_threshold_base = 0
        gradient_threshold_multiplier = gradient_threshold
        #     np.sqrt(np.max(list(area_values.values()))) * gradient_threshold
        # )
    else:
        gradient_threshold_base = gradient_threshold
        gradient_threshold_multiplier = 0

    next_label = 1
    divided_labels = np.zeros(image.shape, dtype=int)
    overlapping_areas = {}
    for label in range(1, n_labels):
        area = labels == label
        area_coordinates = set(zip(*np.nonzero(area)))
        area_values = {c: image[c] for c in area_coordinates}

        area_left = len(area_coordinates)
        if area_left == 1:
            divided_labels[area_coordinates.pop()] = next_label
            next_label += 1
            continue
        new_labels = []
        not_visited = area_values.copy()
        all_possible_labels = {coords: set() for coords in area_coordinates}
        forced_coordinate_labels = dict()

        while area_left > 0:
            max_coordinates = None
            max_value = 0
            for c, v in not_visited.items():
                if v > max_value:
                    max_coordinates = c
                    max_value = v
            # the label for this maximum value can not be overthrown by other labels
            forced_coordinate_labels[max_coordinates] = {next_label}
            queue = [[*max_coordinates, np.inf]]
            visited = set()
            while queue:
                ri, ci, vprev = queue.pop(0)
                coords = (ri, ci)
                if coords in area_coordinates and coords not in visited:
                    vi = area_values[ri, ci]
                    if (
                        np.isinf(vprev)
                        or vi - vprev
                        < gradient_threshold_multiplier * vprev
                        + gradient_threshold_base
                        or (vi == saturation and vprev == saturation)
                    ):
                        visited.add(coords)
                        all_possible_labels[coords].add(next_label)
                        if coords in not_visited:
                            area_left -= 1
                            not_visited.pop(coords)

                        queue.extend(
                            [
                                (ri, ci + 1, vi),
                                (ri, ci - 1, vi),
                                (ri + 1, ci, vi),
                                (ri - 1, ci, vi),
                                (ri + 1, ci + 1, vi),
                                (ri + 1, ci - 1, vi),
                                (ri - 1, ci + 1, vi),
                                (ri - 1, ci - 1, vi),
                            ]
                        )
            new_labels.append(next_label)
            next_label += 1

        if len(new_labels) > 1:
            overlapping_areas[new_labels[0]] = new_labels
        all_possible_labels.update(forced_coordinate_labels)
        new_label_dict = {}
        for overlapping_coords in all_possible_labels:
            possible_labels = all_possible_labels[overlapping_coords]
            if len(possible_labels) == 1:
                new_label_dict[overlapping_coords] = possible_labels.copy().pop()
                # not overlapping pixel
                continue
            elif overlapping_coords in new_label_dict:
                # pixel already visited in previous search and was set there using back_track
                continue

            # overlapping, take closest of possible labels as correct label
            queue = [[overlapping_coords, []]]
            visited = set()
            while queue:
                coords, back_track = queue.pop(0)
                ri, ci = coords
                if coords in all_possible_labels and coords not in visited:
                    new_possible_labels = all_possible_labels[coords]
                    if len(new_possible_labels) == 1:
                        new_label = new_possible_labels.copy().pop()
                        # Setting other possible new_labels using back_track
                        for current_coordinates, current_possible_labels in back_track[
                            1:
                        ]:
                            if new_label in current_possible_labels:
                                new_label_dict[current_coordinates] = new_label
                        ##
                        # Setting the overlapping pixel label if possible
                        if new_label in possible_labels:
                            new_label_dict[overlapping_coords] = new_label
                            break
                        ##
                    elif len(new_possible_labels) > 1:
                        # back_track will set coordinates found on the way to optimize
                        back_track.append([coords, new_possible_labels])
                    visited.add(coords)
                    queue.extend(
                        [
                            ((ri, ci + 1), back_track.copy()),
                            ((ri, ci - 1), back_track.copy()),
                            ((ri + 1, ci), back_track.copy()),
                            ((ri - 1, ci), back_track.copy()),
                            ((ri + 1, ci + 1), back_track.copy()),
                            ((ri + 1, ci - 1), back_track.copy()),
                            ((ri - 1, ci + 1), back_track.copy()),
                            ((ri - 1, ci - 1), back_track.copy()),
                        ]
                    )

        # setting the new labels
        for coords, new_label in new_label_dict.items():
            divided_labels[coords] = new_label
    return next_label, divided_labels, overlapping_areas


# def divide_label_subpeaks_circularity(n_labels, labels, min_circularity):
#   """Did not work sufficiently"""
#     overlapping_areas = {}
#     # next_label = 1
#     for label in range(1, n_labels):
#         area = labels == label
#         contours, _ = cv2.findContours(area.astype(np.uint8), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
#         contour = contours[0]
#         # coordinates = np.array(np.nonzero(area)).T
#         sum_area = cv2.contourArea(contour)
#         perimeter = cv2.arcLength(contour, closed=True)
#         current_circularity = 4 * np.pi * sum_area / perimeter**2
#         print(current_circularity)
#     return n_labels, labels, overlapping_areas


def find_particles_single_frame(
    images: np.ndarray,
    threshold: int,
    connectivity: int = 8,
    gradient_threshold: float = None,
    relative_gradient_threshold: bool = False,
    saturation: int = 4095,
    extra_features_func: callable = None,
    return_labels: bool = False,
):
    """
    If you want a special finding algorithm write a new function and give as input to `find_particles`
    It should take the image(s) as first argument and return a pd.DataFrame with coordinates and optionally camera and extra features for each found particle

    :param images: Images for all cameras at a single time point in the recording
    :param threshold: The threshold used for creating a binarized image where pixels above the threshold is assumed to be particles
    :param connectivity: used in the connectedComponents algorithm
    :param gradient_threshold: a threshold for finding gray value discontinuities in islands of connected pixels. This reduce problems with overlapping particles.
    :param relative_gradient_threshold: should the gradient_threshold be relative to the previous value in the breadth first search applied.
    :param saturation: value used to find avoid finding gray- value discontinuities connected to saturation.
    :param extra_features_func: function used to add extra columns of information of each found pixel position. The max_sum_area_features_func is an example for adding three columns.
    :param return_labels: should the labeled images be returned where each particle island has unique pixel values. Is used in the interactive visualisation for finding particle parameters
    :returns: A pandas dataframe with the columns detailed in find_particles docs.
    """
    ndims = len(images.shape)
    if ndims == 2:
        images = np.expand_dims(images, 0)
    elif ndims == 3:
        pass
    else:
        raise ValueError(
            "Wrong number of dimensions of images at a single frame, expected 3 or 2, found {}".format(
                ndims
            )
        )
    if not images.dtype == int:
        images = images.astype(int)

    particles = []
    all_labels = []
    for camera, image in enumerate(images):
        if isinstance(threshold, (float, int)):
            current_threshold = threshold
        else:
            current_threshold = threshold[camera]
        binary_image = (image > current_threshold).astype(np.uint8)
        (
            n_labels,
            labels,
        ) = cv2.connectedComponents(binary_image, connectivity=connectivity)
        overlapping_areas = {}
        if gradient_threshold is not None:
            n_labels, labels, overlapping_areas = divide_label_subpeaks_gradient(
                image,
                n_labels,
                labels,
                gradient_threshold,
                relative_gradient_threshold,
                saturation,
            )
        # if min_circularity is not None:
        #     n_labels, labels, overlapping_areas = divide_label_subpeaks_circularity(
        #         n_labels, labels, min_circularity
        #     )
        all_labels.append(labels)

        if n_labels <= 1:
            # no particles found in image
            continue

        coordinates = np.empty((n_labels - 1, 2))
        # dilating to get some information from values below threshold as well
        dilation_elem = cv2.getStructuringElement(0, (3, 3), (1, 1))
        background = labels == 0
        for label in range(1, n_labels):
            predilation_area = labels == label
            area = cv2.dilate(predilation_area.astype(np.uint8), dilation_elem) > 0
            area = predilation_area | (area & background)

            xdata = np.nonzero(area)
            ydata = image[xdata]
            xdata = xdata[::-1]

            x_center = np.average(xdata[0], weights=ydata)
            y_center = np.average(xdata[1], weights=ydata)
            coordinates[label - 1, 0] = x_center
            coordinates[label - 1, 1] = y_center

        # coordinates = np.array(
        #     ndimage.center_of_mass(image, labels, np.arange(1, n_labels))
        # )
        current_particles = {"x": coordinates[:, 0], "y": coordinates[:, 1]}

        extra_columns = pd.DataFrame(
            {
                key: values
                for key, values in current_particles.items()
                if key not in ["x", "y"]
            }
        )
        current_particles = pd.DataFrame(
            {"x": current_particles["x"], "y": current_particles["y"]}
        )

        current_particles.insert(0, "camera", camera)

        extra_features = pd.DataFrame()
        if extra_features_func is not None:
            extra_features = extra_features_func(image, n_labels, labels)
        current_particles = pd.concat(
            [current_particles, extra_columns, extra_features], axis=1
        )
        particles.append(current_particles)

    if len(particles) > 0:
        particles = pd.concat(particles, ignore_index=True)
        helpers.set_coordinate_columns(particles)
    if return_labels:
        return particles, all_labels
    else:
        return particles


def find_particles_part(
    images,
    find_particles_function,
    verbose=False,
    **find_particles_kwargs,
):
    """Function to simplify for multiprocessing"""
    images, start_frame = images
    if isinstance(images, np.ndarray):
        ndims = len(images.shape)
        if ndims == 3 or ndims == 4:
            pass  # Ok
        else:
            raise ValueError(
                "Wrong number of dimensions of images input, expected 3 or 4, found {}".format(
                    ndims
                )
            )
    progress = None
    if verbose and "tqdm" in globals():
        progress = tqdm(total=len(images))

    particles = []
    for i, image in enumerate(images):
        frame = i + start_frame

        current_particles = find_particles_function(image, **find_particles_kwargs)
        if len(current_particles) > 0:
            current_particles.insert(0, "frame", frame)
            particles.append(current_particles)
        if progress is not None:
            progress.update()
    if progress is not None:
        progress.close()

    if len(particles) > 0:
        particles = pd.concat(particles, ignore_index=True)
    return particles


def find_particles(
    images: Sequence[np.ndarray],
    find_particles_function: callable = find_particles_single_frame,
    with_multiprocessing: bool or int = False,
    verbose: bool = False,
    **find_particles_kwargs,
):
    """
    Uses a threshold and centroid to find particles
    images argument can be an iterator, but should give for each iteration images with shape (n_cameras, rows, cols)
    if images is an array you should have an array with shape ((n_images), n_cameras, rows, cols)

    :param images: input images as either a 3D or a 4D array where the dimensions are as follows:
        1. The different images with multiple cameras in each (if 3D array, images is expanded with a singular first dimension)
        2. The different cameras
        3. Row of single image
        4. Columns of single image
        If a different sequence class is used. A shape attribute should be available
    :param find_particles_function: function for finding particles in a single frame, it should take the image(s) of a single frame as first argument. See it for more information on what parameters are needed.
    :param with_multiprocessing: should multiple processes be used to speed up the computation, if int this corresponds to the number of processes used in the computation
    :param verbose: should progress be printed for each frame
    :param find_particles_kwargs: extra arguments for the find_particles_function
    :returns: A pandas DataFrame with the following output columns:
            particle_id
            frame
            camera
            coordinates, (x, y) two columns with the same name
                use helpers.unset_coordinate_columns to get "x" "y" columns instead
            [extra features see find_particles_function for more details
            ...]
    """
    assert len(images.shape) > 2, "must be at least 3D array"

    partial_function = functools.partial(
        find_particles_part,
        find_particles_function=find_particles_function,
        **find_particles_kwargs,
    )
    if with_multiprocessing:
        if not hasattr(images, "__len__") or not hasattr(images, "__getitem__"):
            raise ValueError(
                "Multiprocessing requires images object to have both a __len__ and a __getitem__ function"
            )
        if isinstance(with_multiprocessing, bool):
            # when reading from disk, using more processes than four makes it slower than single process on my system
            # Connected to max read speed of the hard drive
            n_workers = min(4, multiprocessing.cpu_count())
        else:
            n_workers = with_multiprocessing
        n_frames = len(images)
        # Divide frames into parts
        process_start_frame = np.linspace(0, n_frames, n_workers + 1, dtype=int)

        images_parts = []
        for i in range(len(process_start_frame) - 1):
            # create image iterator for each process
            images_parts.append(
                map(
                    images.__getitem__,
                    range(process_start_frame[i], process_start_frame[i + 1]),
                )
            )

        # Set static parameters to process function
        with multiprocessing.Pool(n_workers) as p:
            output_iterator = p.imap(
                partial_function, zip(images_parts, process_start_frame)
            )
            if verbose and "tqdm" in globals():
                output_iterator = tqdm(output_iterator, total=len(images_parts))
            particles = list(output_iterator)
        particles = [p for p in particles if len(p) > 0]
        if len(particles) > 0:
            particles = pd.concat(particles, ignore_index=True)
    else:
        particles = partial_function((images, 0), verbose=verbose)
    if len(particles) > 0:
        particles.insert(0, "id", np.arange(len(particles), dtype=int))
    return particles


def merge_particle_clusters(
    particles, min_point_distance=0, special_merge_functions=None
):
    """Takes particles and merges points in clusters smaller than min_point_distance to a single point in each frame

    :param particles: particles as 2D array from find particles, coordinates in 2D are assumed
    :param min_point_distance: if two points in the same frame are closer than this distance they are merged at their mean point
    :param special_merge_functions: dict with column keys and functions used for merging the column, for example special_merge_functions = { "intensity": np.max }
    :returns: merged_particles as a pandas DataFrame

    """
    frames = particles["frame"]
    cameras = particles["camera"]
    unique_frames = np.unique(frames)
    unique_cameras = np.unique(cameras)
    merged_particles = []
    for frame in unique_frames:
        for camera in unique_cameras:
            current_particles = particles[(frame == frames) & (camera == cameras)]
            coordinates = np.expand_dims(current_particles["coordinates"], 0)
            # broadcast the subtraction of coordinates to get every combination possible
            distance_matrix = np.linalg.norm(
                coordinates - np.transpose(coordinates, axes=(1, 0, 2)), axis=-1
            )
            # on the diagonal the distance is zero that is set to inf since they are not interesting
            distance_matrix[np.eye(distance_matrix.shape[0], dtype=bool)] = np.inf

            # extracting rows with at least one particle closer than min_point_distance
            merging_distance_matrix = distance_matrix < min_point_distance
            particles_to_merge = np.sum(merging_distance_matrix, axis=1) > 0
            particles_to_merge_inds = np.where(particles_to_merge)[0]
            already_merged = set()

            for current_particle_to_merge in particles_to_merge_inds:
                if current_particle_to_merge in already_merged:
                    continue
                # find which particles to merge this particle with
                merge_companions = np.flatnonzero(
                    merging_distance_matrix[current_particle_to_merge]
                )
                # remove already merged particles
                merge_companions = [
                    p for p in merge_companions if p not in already_merged
                ]

                # all particles in the same list
                current_particles_to_merge = np.hstack(
                    ([current_particle_to_merge], merge_companions)
                ).astype(int)
                # after the merge they should not be considered again
                already_merged.update(list(current_particles_to_merge))

                # estimate the merged particle parameters as the mean of all
                current_particles_to_merge_full = current_particles.iloc[
                    current_particles_to_merge
                ]
                merged_particle = current_particles_to_merge_full[:1].copy()
                merged_particle[["coordinates"]] = [
                    np.mean(
                        current_particles_to_merge_full["coordinates"].to_numpy(),
                        axis=0,
                    )
                ]
                if special_merge_functions is not None:
                    # use some other merge option than taking the first particle values
                    for key, merge_function in special_merge_functions.items():
                        merged_particle[key] = merge_function(
                            current_particles_to_merge_full[key]
                        )
                merged_particles.append(merged_particle)
            merged_particles.append(current_particles[~particles_to_merge])

    merged_particles = pd.concat(merged_particles, ignore_index=True)
    return merged_particles
