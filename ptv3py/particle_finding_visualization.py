from typing import Sequence, Tuple
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib.figure as mfigure
import matplotlib.widgets as mwidgets
import matplotlib.axes as maxes
import matplotlib.lines as mlines
import matplotlib.cm as mcm
import matplotlib.colors as mcolors
import matplotlib.patches as mpatches

import inspect
import functools
import itertools
import json

from . import particle_finding
from . import visualise_helpers
from . import helpers


class FoundParticlesPlotSlider(visualise_helpers.PlotSlider):
    def __init__(
        self,
        particles,
        images,
        frame_lims=None,
        clim=None,
        extent=None,
        cmap=None,
        values_cmap=None,
        values_clim=None,
        values=None,
        verbose=False,
        **kwargs,
    ):
        if isinstance(images, tuple):
            # If you do not want to show the images you can give the shape of the images as tuple
            shape = images
            images = None
        else:
            shape = images.shape
        n_cameras = shape[1]

        if frame_lims is None:
            frame_lims = [0, shape[0] - 1]
        else:
            if frame_lims[1] < 0:
                frame_lims[1] = shape[0] - frame_lims[1]
        self.set_framelims(frame_lims)
        self.valmin = frame_lims[0]
        self.valmax = frame_lims[1]

        if clim is None:
            clim = [None, None]
        norm = mcolors.Normalize(vmin=clim[0], vmax=clim[1])
        image_mappable = mcm.ScalarMappable(norm=norm, cmap=cmap)
        self.mappable = image_mappable

        if values_clim is None:
            values_clim = [None, None]
        norm = mcolors.Normalize(vmin=values_clim[0], vmax=values_clim[1], clip=True)
        values_mappable = mcm.ScalarMappable(norm=norm, cmap=values_cmap)

        ## Setting various variables
        self.plotting_variables = {}
        self.plotting_variables["n_cameras"] = n_cameras
        self.plotting_variables["images"] = images
        self.plotting_variables["frame_particles_dict"] = {}
        self.plotting_variables["with_size"] = True
        self.plotting_variables["color"] = "red"
        self.plotting_variables["extent"] = extent
        self.plotting_variables["image_mappable"] = image_mappable
        # self.plotting_variables["values_clim"] = values_mappable
        self.plotting_variables["values_mappable"] = values_mappable

        self.picking_variables = dict(
            verbose=verbose,
            on_pick_base_column_labels=["frame", "camera", "coordinates"],
        )
        self.pick_prefix = "finding"

        # Initialising PlotSlider
        super().__init__(n_plot_axs=n_cameras, **kwargs)

        self.update_plotting_variables(particles, values)

        if extent is None:
            extent = [0, shape[-2], 0, shape[-1]]
        for ax in self.plot_ax_list:
            ax.set_xlim(extent[-2:])
            ax.set_ylim(extent[1::-1])

    def update_plotting_variables(self, particles, values=None):
        n_cameras = self.plotting_variables["n_cameras"]

        frame_particles_dict = self.plotting_variables["frame_particles_dict"]
        if len(particles) > 0:
            # will change in the array so copying when sorting
            particles = particles.sort_values(["frame", "camera"], ignore_index=True)
            particles = helpers.unset_coordinate_columns(particles)

            # Possible coloring of the found particles
            if values is not None:
                if isinstance(values, str):
                    # taking column of values
                    values = particles[values].to_numpy()
                self.plotting_variables["values_mappable"].norm.autoscale_None(values)
                self.mappable = self.plotting_variables["values_mappable"]

                particles.insert(len(particles.columns), "color_value", values)
            ###

            frames_column = particles["frame"].to_numpy()
            frames, indicies = np.unique(frames_column, return_index=True)
            indicies = np.append(indicies, len(particles))
            cameras = particles["camera"].to_numpy()

            helpers.set_coordinate_columns(particles)
            for i, frame in enumerate(frames):
                current_cameras = cameras[indicies[i] : indicies[i + 1]]
                current_particles = particles.iloc[indicies[i] : indicies[i + 1]]
                particles_per_camera = []
                for camera in range(n_cameras):
                    particles_per_camera.append(
                        current_particles.loc[current_cameras == camera]
                    )
                frame_particles_dict[frame] = particles_per_camera
                if frame in self.lru_cache:
                    # deleting frames in cache that are updated
                    del self.lru_cache[frame]

            frame_lims = self.get_framelims()
            all_frames = np.arange(frame_lims[0], frame_lims[1] + 1, dtype=int)
            all_intensities = np.zeros((len(all_frames), n_cameras))
            for frame, particles_per_camera in frame_particles_dict.items():
                intensity = [len(p) for p in particles_per_camera]
                all_intensities[frame - all_frames[0]] = intensity
            self.update_intensities(all_frames, all_intensities)

        self.set_frame(self.get_frame())

    def make_frame_artists(self, frame):
        images = self.plotting_variables["images"]
        n_cameras = self.plotting_variables["n_cameras"]
        data = [[] for camera in range(n_cameras)]
        if images is not None:
            image = images[frame]
            for camera in range(n_cameras):
                data[camera].append(
                    functools.partial(
                        maxes.Axes.imshow,
                        X=image[camera],
                        extent=self.plotting_variables["extent"],
                        cmap=self.plotting_variables["image_mappable"].get_cmap(),
                        norm=self.plotting_variables["image_mappable"].norm,
                    )
                )

        frame_particles_dict = self.plotting_variables["frame_particles_dict"]
        if frame in frame_particles_dict:
            current_particles = frame_particles_dict[frame]
            for camera in range(n_cameras):
                current_camera_particles = current_particles[camera]
                current_coordinates = current_camera_particles["coordinates"].to_numpy()
                if "color_value" in current_camera_particles:
                    current_values = current_camera_particles["color_value"]
                    current_colors = self.plotting_variables["values_mappable"].to_rgba(
                        current_values
                    )
                else:
                    current_colors = itertools.repeat(self.plotting_variables["color"])
                # tried to use scatter here before but the picking did not work
                # to allow different colors per dot one line per dot is plotted
                sizes = None
                if (
                    self.plotting_variables["with_size"]
                    and "size" in current_camera_particles
                ):
                    current_sizes = current_camera_particles["size"].to_numpy()
                    current_sizes = np.tile(sizes, (2, 1)).T
                else:
                    current_sizes = itertools.repeat(None)
                for i, (current_coordinate, current_color, current_size) in enumerate(
                    zip(current_coordinates, current_colors, current_sizes)
                ):
                    data[camera].append(
                        mlines.Line2D(
                            xdata=[current_coordinate[0]],
                            ydata=[current_coordinate[1]],
                            color=current_color,
                            alpha=0.5,
                            marker=".",
                            linestyle="",
                            label="{} {} {}".format(self.pick_prefix, camera, i),
                            picker=True,
                        )
                    )
                    if sizes is not None:
                        data[camera].append(
                            mpatches.Ellipse(
                                current_coordinate,
                                *current_size,
                                edgecolor=current_color,
                                fill=False,
                            )
                        )

        if n_cameras == 1:
            data = data[0]
        return data

    def on_pick_data(self, event):
        label = event.artist.get_label()
        if not label.startswith(self.pick_prefix):
            # should only process right lines
            return
        frame = self.frame_slider.val
        line = event.artist
        camera, ind = [
            int(val) for val in line.get_label()[len(self.pick_prefix) + 1 :].split()
        ]
        # ind = event.ind[-1]
        particle = self.plotting_variables["frame_particles_dict"][frame][camera].iloc[
            [ind]
        ]
        coordinates = particle["coordinates"].to_numpy()[0]

        self.plot_ax_list[camera].plot(*coordinates, "*", color="blue")

        if self.picking_variables["verbose"]:
            text_list = []
            for column, value in particle.items():
                value = value.to_numpy()[0]
                if column in self.picking_variables["on_pick_base_column_labels"]:
                    continue
                if isinstance(value, float):
                    text_list.append("{}: {:.3f}".format(column, value))
                else:
                    text_list.append("{}: {}".format(column, value))
            text = "\n".join(text_list)
        elif "id" in particle.columns:
            text = "id: {}".format(particle["id"])
        else:
            text = None

        if text is not None:
            self.information_text(text)


def visualise_found_particles(
    particles: pd.DataFrame,
    images: Sequence[np.ndarray],
    frame_lims: Tuple[int, int] = None,
    vmin: int = None,
    vmax: int = None,
    extent: Tuple[int, int, int, int] = None,
    cmap: str or mcolors.Colormap = "gray",
    values_cmap: str or mcolors.Colormap = "viridis",
    values_clim: Tuple[float, float] = None,
    values: str or pd.DataFrame = None,
    verbose: bool or int = False,
    **kwargs,
):
    """
    Visualise the particles in the images by an interactive matplotlib plot
    if images is an iterator it also needs to provide a shape attribute similar to numpy ndarrays shape with dimensions (n_images, n_cameras, n_rows, n_columns)
    images can also be just the shape of the images
    frame_lims is a list with the first and last frame to show
    values_cmap values_clim and values can be used to plot the points with a color corresponding to the value in values
    **kwargs are  given to figure creation

    :param particles: the found particles
    :param images: The images used for finding particles
    :param frame_lims: limits of frames to view for the images. If not given, the limits are determined by the found particles
    :param vmin: given to imshow for plotting images
    :param vmax: given to imshow for plotting images
    :param extent: given to imshow for plotting images
    :param cmap: given to imshow for plotting images
    :param values_cmap: Colormap used to plot particle position in colors given a number for each position such as size
    :param values_clim: Used for the plotting colormap of points
    :param values: string for column name in the particles parameter to use to extract colors for the points plotted. Can alternatively be a dataframe with the same length as particles with the same numbers.
    :param verbose: show extra information when clicking on object in the plotted data
    :param **kwargs: remaining arguments are used in the creation of a matplotlib figure and the visualise_helpers.PlotSlider class
    :returns: subclass of a matplotlib figure called a FoundParticlesPlotSlider
    """
    plotslider_kwargs = dict(
        particles=particles,
        images=images,
        frame_lims=frame_lims,
        clim=[vmin, vmax],
        extent=extent,
        cmap=cmap,
        values_cmap=values_cmap,
        values_clim=values_clim,
        values=values,
        verbose=verbose,
        with_intensity=len(particles) > 0,
        **kwargs,
    )
    ps = plt.figure(
        FigureClass=FoundParticlesPlotSlider,
        **plotslider_kwargs,
    )
    return ps


def format_cursor_data(data):
    return "{}".format(data)


def particle_finding_parameters_helper(
    images,
    find_particles_function=particle_finding.find_particles_single_frame,
    saturation=2**12 - 1,
    **kwargs,
):
    """Creates one figure showing the images and one to tweak the parameters for finding particles"""

    def post_plotting(ps):
        for ax in ps.plot_ax_list:
            ax.images[0].format_cursor_data = format_cursor_data

    ps = plt.figure(
        FigureClass=FoundParticlesPlotSlider,
        particles=[],
        images=images,
        clim=[0, saturation],
        post_plotting=post_plotting,
        verbose=True,
        **kwargs,
    )
    fptf = plt.figure(
        FigureClass=Finding_particles_tweak_figure,
        plotslider=ps,
        images=images,
        find_particles_function=find_particles_function,
    )
    return fptf, ps


def read_single_parameter(text, annotation):
    value = None
    error = None
    if annotation in [int, float, str]:
        try:
            value = annotation(text)
        except ValueError as e:
            if text == "None":
                value = None
            else:
                error = e
    else:
        # try parsing with json
        text = (
            text.replace("False", "false")
            .replace("True", "true")
            .replace("None", "null")
        )
        try:
            value = json.loads(text)
        except ValueError as e:
            error = e
    return value, error


def read_parameters(current_parameters, function_parameters):
    valid_input = True
    for name, param in function_parameters.items():
        widget = param["widget"]
        text = widget.text
        annotation = param["annotation"]
        value, error = read_single_parameter(text, annotation)
        if error is not None:
            print("Invalid {} parameter: {}".format(name, error))
            valid_input = False
            widget.color = "lightcoral"
            widget.hovercolor = "indianred"
            continue
        widget.color = "1"
        widget.hovercolor = "0.95"
        current_parameters[name] = value
    return valid_input, current_parameters


class Finding_particles_tweak_figure(mfigure.Figure):
    def __init__(
        self,
        plotslider,
        images,
        find_particles_function,
        # find_particles_function_kwargs={},
        **kwargs,
    ):
        """
        find_particles_function is assumed to take image(s) as first argument and then parameters
        """
        self.find_particles_function = find_particles_function
        function_signature = inspect.signature(find_particles_function)
        # function_docs = find_particles_function.__doc__
        # self.find_particles_function_kwargs = find_particles_function_kwargs
        self.function_parameters = {}
        self.other_function_parameters = {}
        self.possible_other_parameters = set()

        for name in function_signature.parameters:
            param = function_signature.parameters[name]
            if (
                # param.annotation in [int, float, str]
                param.annotation not in [callable, param.empty]
                # and not name in find_particles_function_kwargs
                and not name == "images"
                and "return_" not in name
            ):
                annotation = param.annotation
                default = param.default if param.default is not param.empty else ""
                self.function_parameters[name] = dict(
                    annotation=annotation,
                    default=default,
                    empty=param.empty,
                    possible_values=None,
                )
            else:
                self.possible_other_parameters.add(name)

        # self.set_parameters(**find_particles_function_kwargs)

        figsize = (7, (len(self.function_parameters) + 2) / 2)
        kwargs["figsize"] = figsize
        super().__init__(**kwargs)

        self.plotslider = plotslider
        self.all_images = images

        self.create_input_axes()

    def create_input_axes(self):
        gridspec_kwargs = {
            "left": 0.2,
            "right": 0.85,
            "hspace": 0.1,
            "wspace": None,
        }
        gridspec = self.add_gridspec(
            1 + len(self.function_parameters) + 1, 2, **gridspec_kwargs
        )
        gridspec_kwargs = {
            "left": 0.05,
            "right": 0.99,
            "hspace": 0.1,
            "wspace": None,
        }
        gridspec = self.add_gridspec(
            1 + len(self.function_parameters) + 1,
            4,
            width_ratios=[0.1, 0.3, 0.4, 0.2],
            **gridspec_kwargs,
        )

        clims_ax = self.add_subplot(gridspec[0, 1:-1])
        orientation = "horizontal"
        # First is just simple rangeslider for changing the color limits of the cmap
        clims_norm = self.plotslider.plotting_variables["image_mappable"].norm
        clims_slider = mwidgets.RangeSlider(
            clims_ax,
            "clims",
            clims_norm.vmin,
            clims_norm.vmax,
            valinit=(clims_norm.vmin, clims_norm.vmax),
            valfmt="%d",
            valstep=1,
            orientation=orientation,
        )
        self.clims_slider = clims_slider

        def on_clims_update(clims):
            clims_norm.vmin = clims[0]
            clims_norm.vmax = clims[1]
            self.plotslider.canvas.draw_idle()

        clims_slider.on_changed(on_clims_update)

        def on_parameter_update(event):
            print("Finding")
            self.find_particles()
            print("Done")

        # Then inputs for all parameters are created
        for i, [name, param] in enumerate(self.function_parameters.items()):
            parameter_ax = self.add_subplot(gridspec[i + 1, 2:])
            annotation = " [{}]".format(param["annotation"].__name__)
            param["widget"] = mwidgets.TextBox(
                parameter_ax,
                "{}{}".format(name, annotation),
                str(param["default"]),
            )
        button_ax = self.add_subplot(gridspec[-1, 1])
        self.update_button = mwidgets.Button(button_ax, "Update")
        self.update_button.on_clicked(on_parameter_update)

    def set_clims(self, clims):
        self.clims_slider.set_val(clims)

    def set_parameters(self, **parameter_values):
        for name, val in parameter_values.items():
            if name in self.function_parameters:
                self.function_parameters[name]["widget"].set_val(
                    str(val).replace("'", '"')
                )
            elif name in self.possible_other_parameters:
                self.other_function_parameters[name] = val
            else:
                raise ValueError(
                    "Parameter {} is not valid for finding function".format(name)
                )

    def find_particles(self):
        frame = self.plotslider.get_frame()
        images = self.all_images[frame]

        current_parameters = {}
        current_parameters.update(self.other_function_parameters)
        valid_input, current_parameters = read_parameters(
            current_parameters, self.function_parameters
        )

        if not valid_input:
            self.canvas.draw_idle()
            return

        current_particles, labels = self.find_particles_function(
            images,
            return_labels=True,
            **current_parameters,
        )
        if len(current_particles) == 0:
            return
        current_particles["frame"] = frame

        ## adding current particles to frame_particles dict
        self.plotslider.update_plotting_variables(current_particles)

        for camera in range(len(labels)):
            current_labels = labels[camera].astype(float)
            current_labels[current_labels == 0] = np.nan
            self.plotslider.plot_ax_list[camera].imshow(
                current_labels, cmap="inferno", alpha=0.5
            )
        self.plotslider.canvas.draw_idle()
