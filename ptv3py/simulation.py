import numpy as np
import pandas as pd

from . import particle_3d_matching, particle_tracking, helpers
from .camera_calibration_module import CameraCalibration

################ Simulate tracks ##################


def random_unit_vectors(n, ndims):
    """n vectors with ndims length and norm of vectors all vectors equal to 1"""
    vectors = np.random.rand(n, ndims) - 0.5
    return vectors / np.expand_dims(np.linalg.norm(vectors, axis=1), 1)


def simulate_particle_tracks(
    n_particles,
    n_frames,
    ndims,
    volume,
    speed_mu,
    speed_std=0,
    acceleration_mu=0,
    acceleration_std=0,
    noise_std=0,
    remove_particles_outside_volume=False,
):
    """
    :param n_particles: number of simulated particles
    :param n_frames: over how many frames
    :param ndims: in how many dimensions, typically 2 or 3
    :param volume: the volume (or area in 2D) in which the particles reside
    :param speed_mu: the average speeds of the particles
    :param speed_std: the standard deviation of the particle speeds
    :param acceleration_mu: -||- acceleration
    :param acceleration_std: -||- acceleration
    :param noise_std: independent normal noise in the particle positions
    :param remove_particles_outside_volume: should particles that leave the volume be removed
    :returns: A pandas dataframe with the simulated tracks
    """
    particle_initial_positions = (
        np.random.rand(n_particles, ndims) * 0.8 + 0.1
    ) * volume.reshape((1, -1))
    particle_initial_velocities = random_unit_vectors(n_particles, ndims) * (
        speed_mu + np.random.randn(n_particles, 1) * speed_std
    )
    particle_accelerations = random_unit_vectors(n_particles, ndims) * (
        acceleration_mu + np.random.randn(n_particles, 1) * acceleration_std
    )

    particle_accelerations = np.ones((n_frames, 1, 1)) * np.expand_dims(
        particle_accelerations, 0
    )
    particle_velocities = np.expand_dims(particle_initial_velocities, 0) + np.cumsum(
        particle_accelerations, axis=0
    )
    particle_positions = np.expand_dims(particle_initial_positions, 0) + np.cumsum(
        particle_velocities, axis=0
    )

    noise = np.random.randn(*particle_positions.shape) * noise_std
    particle_positions += noise

    particle_numbers = np.tile(np.arange(n_particles, dtype=int), n_frames)
    particle_id = np.arange(n_particles * n_frames, dtype=int)
    particle_frames = np.repeat(np.arange(n_frames, dtype=int), n_particles)

    particle_positions = particle_positions.reshape((-1, ndims)).T
    tracked_particles = {
        "id_track": particle_numbers,
        "id": particle_id,
        "frame": particle_frames,
    }
    tracked_particles = pd.DataFrame(tracked_particles)
    for i in range(ndims):
        tracked_particles.insert(
            3 + i, "coordinates", particle_positions[i], allow_duplicates=True
        )
    tracked_particles.sort_values(
        ["id_track", "frame"], inplace=True, ignore_index=True
    )

    if remove_particles_outside_volume:
        invalid_rows = []
        coordinate_limits = np.column_stack((np.zeros(ndims), volume))
        for track_id, track in particle_tracking.TracksIterator(tracked_particles):
            is_valid = particle_3d_matching.inside_coordinate_limits(
                track["coordinates"], coordinate_limits
            )
            if not np.all(is_valid):
                first_outside_index = np.argmin(is_valid)
                invalid_rows.extend(track.index[first_outside_index:])
        tracked_particles.drop(invalid_rows, inplace=True)

    return tracked_particles


###################################################


###### Simulate projection ################
def get_simple_camera_calibration(shape, n_cameras=2):
    """
    A camera calibration where the two cameras are looking perfectly in the y and x direction respectively
    Shape defines the volume or image shapes that can be simulated using this calibration
    """
    if n_cameras != 2:
        raise ValueError("Only implemented for two cameras")
    # One camera looking perfectly in the x direction
    XY, Z = np.meshgrid(*[np.arange(v) for v in shape])
    Z = Z[::-1]
    zeros = np.zeros(shape)
    ones = np.ones(shape)
    threed_lines = np.zeros((n_cameras,) + shape + (2, 3))

    # preparing camera 0
    # looking perfectly in the y direction
    threed_lines[0, :, :, 0] = np.dstack((XY, zeros, Z))
    threed_lines[0, :, :, 1] = np.dstack((zeros, ones, zeros))

    # preparing camera 1
    # looking perfectly in the x direction
    threed_lines[1, :, :, 0] = np.dstack((zeros, XY[:, ::-1], Z))
    threed_lines[1, :, :, 1] = np.dstack((ones, zeros, zeros))

    camera_calibration = CameraCalibration(threed_lines)
    return camera_calibration


def simulate_projection(particles_3d, camera_calibration):
    """
    Project 3D particle positions using camera calibration
    """
    projected_coordinates = camera_calibration.project(
        particles_3d["coordinates"], use_quick_interpolation=False
    )

    n_particles = len(particles_3d)
    projected_particles = {
        "id": [],
        "frame": [],
        "camera": [],
        "x": [],
        "y": [],
        "id_3d": [],
    }
    for c in range(camera_calibration.n_cameras):
        projected_particles["id"] = np.concatenate(
            (projected_particles["id"], particles_3d["id"] + c * n_particles)
        )
        projected_particles["frame"] = np.concatenate(
            (projected_particles["frame"], particles_3d["frame"])
        )
        projected_particles["camera"] = np.concatenate(
            (projected_particles["camera"], np.ones(n_particles) * c)
        )
        projected_particles["x"] = np.concatenate(
            (projected_particles["x"], projected_coordinates[:, c, 0])
        )
        projected_particles["y"] = np.concatenate(
            (projected_particles["y"], projected_coordinates[:, c, 1])
        )
        projected_particles["id_3d"] = np.concatenate(
            (projected_particles["id_3d"], particles_3d["id"])
        )
    for key in ["id", "frame", "camera", "id_3d"]:
        projected_particles[key] = projected_particles[key].astype(int)
    projected_particles = pd.DataFrame(projected_particles)
    helpers.set_coordinate_columns(projected_particles)
    return projected_particles


###########################################

###### Simulate images ################


def gaussian_psf_base(
    xdata,
    amplitude: float,
    x: float,
    y: float,
    sigma_x: float,
    sigma_y: float = None,
    gaussian_theta: float = 0,
    offset: float = 0,
):
    if sigma_y is None:
        sigma_y = sigma_x
    xs, ys = xdata
    a = (np.cos(gaussian_theta) ** 2) / (2 * sigma_x**2) + (
        np.sin(gaussian_theta) ** 2
    ) / (2 * sigma_y**2)
    b = (np.sin(2 * gaussian_theta)) / (4 * sigma_x**2) - (
        np.sin(2 * gaussian_theta)
    ) / (4 * sigma_y**2)
    c = (np.sin(gaussian_theta) ** 2) / (2 * sigma_x**2) + (
        np.cos(gaussian_theta) ** 2
    ) / (2 * sigma_y**2)
    g = offset + amplitude * np.exp(
        -(a * ((xs - x) ** 2) + 2 * b * (xs - x) * (ys - y) + c * ((ys - y) ** 2))
    )
    return g.ravel()


def gaussian_psf(shape, coordinates, sigma_x, sigma_y=None, theta=0):
    y, x = np.mgrid[: shape[0], : shape[1]]
    gaussian_image = gaussian_psf_base((x, y), 1, *coordinates, sigma_x, sigma_y, theta)
    gaussian_image.shape = shape
    gaussian_image[gaussian_image < 0.01] = 0
    return gaussian_image


def simulate_particle_images(
    image_shape,
    particles,
    point_spread_sizes,
    point_spread_intensity=255,
    point_spread_function="gaussian",
    saturation=4095,
):
    """
    Create simulated images from particle positions and a predefined point spread function of how a particle look like in the image.

    :param image_shape:
    :param particles:
    :param point_spread_sizes: can be a sequence where each particle has its own size
    :param point_spread_intensity: the intensity of a single particle
    :param point_spread_function: if string gaussian the gaussian_psf function is used, otherwise a predefined function should be given
    :param saturation: Values above this is thresholded
    :returns: np.ndarray with the four dimensions corresponding to frame, camera, row, column
    """
    image_shape = np.asarray(image_shape)
    cameras = particles["camera"].to_numpy()
    unique_cameras = np.unique(cameras)
    n_cameras = len(unique_cameras)
    frames = particles["frame"].to_numpy()
    unique_frames = np.unique(frames)
    n_frames = len(unique_frames)

    coordinates = particles["coordinates"].to_numpy()
    point_spread_sizes = (
        np.ones(len(coordinates)) * np.asarray(point_spread_sizes).ravel()
    )

    images = np.zeros((n_frames, n_cameras) + tuple(image_shape), dtype=int)
    for frame in range(n_frames):
        for camera in range(n_cameras):
            current_inds = (cameras == camera) & (frames == frame)
            current_coordinates = coordinates[current_inds]
            current_point_spread_sizes = point_spread_sizes[current_inds]
            current_image = np.zeros(image_shape)
            for i in range(len(current_coordinates)):
                sum_image_size = int(round(6 * point_spread_sizes[i]))
                sub_shape = (sum_image_size, sum_image_size)
                half_size = sum_image_size // 2
                current_coordinate = current_coordinates[i]
                current_coordinate_int = np.round(current_coordinate).astype(int)
                current_coordinate -= current_coordinate_int - half_size

                if point_spread_function == "gaussian":
                    particle_image = (
                        gaussian_psf(
                            sub_shape,
                            current_coordinate,
                            current_point_spread_sizes[i],
                        )
                        * point_spread_intensity
                    )
                    output_column_lims = [
                        current_coordinate_int[0] - half_size,
                        current_coordinate_int[0] + half_size,
                    ]
                    output_row_lims = [
                        current_coordinate_int[1] - half_size,
                        current_coordinate_int[1] + half_size,
                    ]

                    input_column_lims = [
                        max(0, output_column_lims[0]) - output_column_lims[0],
                        sum_image_size
                        + min(image_shape[1], output_column_lims[1])
                        - output_column_lims[1],
                    ]
                    input_row_lims = [
                        max(0, output_row_lims[0]) - output_row_lims[0],
                        sum_image_size
                        + min(image_shape[0], output_row_lims[1])
                        - output_row_lims[1],
                    ]

                    output_column_lims = [
                        max(0, output_column_lims[0]),
                        min(image_shape[1], output_column_lims[1]),
                    ]
                    output_row_lims = [
                        max(0, output_row_lims[0]),
                        min(image_shape[0], output_row_lims[1]),
                    ]

                    current_image[
                        output_row_lims[0] : output_row_lims[1],
                        output_column_lims[0] : output_column_lims[1],
                    ] += particle_image[
                        input_row_lims[0] : input_row_lims[1],
                        input_column_lims[0] : input_column_lims[1],
                    ]
                else:
                    current_image += point_spread_function(
                        current_image.shape,
                        current_coordinates[i],
                        current_point_spread_sizes[i],
                    )
            current_image[current_image > saturation] = saturation
            images[frame, camera] = current_image.astype(int)
    if len(unique_frames) == 1:
        images = images[0]
    return images


#######################################


######## Helper functions for tracking accuracy ###########


def tracking_n_correctly_tracked(correct_tracks, estimated_tracks):
    """
    Get a value between 0 and one of how many tracks where correctly found
    This function is only considering the id and track id values
    """
    correct_track_ids = correct_tracks["id_track"]
    correct_particle_ids = correct_tracks["id"]
    unique_track_ids = np.unique(correct_track_ids)

    estimated_tracks.index = estimated_tracks["id"]
    estimated_track_ids = estimated_tracks["id_track"].copy()

    n_correct_tracks_found = 0
    track_id_correspondences = {
        reference_track_id: None
        for reference_track_id in np.unique(estimated_track_ids)
    }
    for correct_track_id in unique_track_ids:
        current_correct_particle_ids = correct_particle_ids[
            correct_track_ids == correct_track_id
        ]
        current_track_length = len(current_correct_particle_ids)

        reference_track_id = estimated_track_ids[current_correct_particle_ids.iloc[0]]
        found_tracks_with_same_track_id = 0
        for ccpi in current_correct_particle_ids[1:]:
            if estimated_track_ids[ccpi] == reference_track_id:
                found_tracks_with_same_track_id += 1
            else:
                break
        if found_tracks_with_same_track_id == current_track_length - 1:
            n_correct_tracks_found += 1
            track_id_correspondences[reference_track_id] = correct_track_id
        else:
            track_id_correspondences[reference_track_id] = None

    return n_correct_tracks_found, track_id_correspondences


def tracking_accuracy_estimator(correct_tracks, estimated_tracks):
    """
    Get a value between 0 and one of how many tracks where correctly found
    This function is only considering the id and track id values
    """
    correct_track_ids = correct_tracks["id_track"]
    unique_track_ids = np.unique(correct_track_ids)
    n_tracks = len(unique_track_ids)

    n_correct_tracks_found, _ = tracking_n_correctly_tracked(
        correct_tracks, estimated_tracks
    )
    return n_correct_tracks_found / n_tracks
