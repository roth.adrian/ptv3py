""" Helper functions for the package """
import numpy as np
import matplotlib.colors as mcolors
import matplotlib.cm as mcm
from collections import OrderedDict

import re

### Helping definitions of column indexes ###

COORDINATE_COLUMNS = ["x", "y", "z"]

FINDING_BASE_COLUMNS = ["id", "frame", "camera"] + COORDINATE_COLUMNS[:2]
MATCHED_BASE_COLUMNS = ["id", "frame"] + COORDINATE_COLUMNS  # + ["id_second"]
TRACKED_BASE_COLUMNS = ["id_track", "id", "frame"] + COORDINATE_COLUMNS

COORDINATES_NAME = "coordinates"


def set_coordinate_columns(particles, ignore_order=False):
    """Setting the coordinate columns of a dataframe to all have the same column name "coordinates" """
    columns = list(particles.columns)
    found_coordinate_columns = 0
    if ignore_order:

        def filter_function(x, _):
            return x.lower() in COORDINATE_COLUMNS

    else:

        def filter_function(x, j):
            return x.lower() == COORDINATE_COLUMNS[j]

    for i in range(len(columns)):
        if filter_function(
            columns[i], found_coordinate_columns
        ):  #  columns[i] == COORDINATE_COLUMNS[found_coordinate_columns].lower():
            columns[i] = COORDINATES_NAME
            found_coordinate_columns += 1
            if found_coordinate_columns == len(COORDINATE_COLUMNS):
                break
    assert (
        found_coordinate_columns == 2 or found_coordinate_columns == 3
    ), "Something is off with coordinate order"
    particles.columns = columns
    return particles


def unset_coordinate_columns(particles, upper=False):
    """
    unsetting to get x, y [,z] columns
    upper means the capital letters
    """
    coordinate_columns = COORDINATE_COLUMNS
    if upper:
        coordinate_columns = [n.upper() for n in coordinate_columns]
    columns = list(particles.columns)
    found_coordinate_columns = 0
    for i in range(len(columns)):
        if columns[i] == COORDINATES_NAME:
            columns[i] = coordinate_columns[found_coordinate_columns]
            found_coordinate_columns += 1
    particles.columns = columns
    return particles


duplicate_suffix = "_duplicate_"
duplicate_pattern = re.compile(r"{}\d+".format(duplicate_suffix))


def unset_column_list_duplicates(columns):
    columns = np.asarray(columns)
    unique_columns, counts = np.unique(columns, return_counts=True)
    for i in np.flatnonzero(counts > 1):
        duplicated_column = unique_columns[i]
        indexs = np.flatnonzero(columns == duplicated_column)
        for j, k in enumerate(indexs):
            columns[k] = "{}{}{}".format(columns[k], duplicate_suffix, j)
    return columns


def unset_column_duplicates(dataframe):
    columns = unset_column_list_duplicates(dataframe.columns)
    dataframe.columns = columns
    return dataframe


def reset_column_duplicates(dataframe):
    columns = np.array(dataframe.columns)
    for i in range(len(columns)):
        match = duplicate_pattern.search(columns[i])
        if match:
            columns[i] = columns[i][: match.span()[0]]
    dataframe.columns = columns

    return dataframe


############################################

COLOR_DICT = {
    "white": (255,) * 3,
    "black": (0,) * 3,
    "red": (0, 0, 255),
    "green": (0, 255, 0),
    "blue": (255, 0, 0),
}


def get_bgr_color(color):
    if isinstance(color, str):
        color = COLOR_DICT[color.lower()]
    return color + (255,)  # alpha


def linear_cmap(color, name=""):
    color = np.array(color) / 255
    cdict = {
        "red": [[0, 0, 0], [1, color[0], color[0]]],
        "green": [[0, 0, 0], [1, color[1], color[1]]],
        "blue": [[0, 0, 0], [1, color[2], color[2]]],
    }
    cmap = mcolors.LinearSegmentedColormap(name, segmentdata=cdict, N=256)

    def remade_cmap(values):
        return cmap(values / 255) * 255

    return remade_cmap


def saturated_cmap(
    cmap_name="viridis", saturation_value=2**12, saturation_color=[1, 0, 0, 1]
):
    original_cmap = mcm.get_cmap(cmap_name)
    cmap_colors = original_cmap(np.linspace(0, 1, saturation_value))
    cmap_colors[-1] = saturation_color
    saturated_cmap = mcolors.ListedColormap(cmap_colors)
    return saturated_cmap


class LRU(OrderedDict):
    """
    Limit size, evicting the least recently looked-up key when full
    """

    def __init__(self, maxsize=None, *args, **kwds):
        self.maxsize = maxsize
        super().__init__(*args, **kwds)

    def __getitem__(self, key):
        value = super().__getitem__(key)
        self.move_to_end(key)
        return value

    def __setitem__(self, key, value):
        if key in self:
            self.move_to_end(key)
        super().__setitem__(key, value)
        if self.maxsize is not None and len(self) > self.maxsize:
            oldest = next(iter(self))
            del self[oldest]
