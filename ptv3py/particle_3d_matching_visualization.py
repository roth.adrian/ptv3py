import inspect

import functools

import matplotlib.pyplot as plt

import matplotlib.figure as mfigure

import matplotlib.colors as mcolors
import matplotlib.cm as mcm
import matplotlib.axes as maxes
import matplotlib.widgets as mwidgets
import matplotlib.lines as mlines
from mpl_toolkits import mplot3d

from typing import Sequence, Tuple, List

import numpy as np
import pandas as pd

from . import particle_3d_matching
from . import particle_finding_visualization
from . import visualise_helpers

from . import helpers


def add_2d_coordinates_to_3d_matched(matched_particles, particles, n_cameras=None):
    """
    Adds columns:
    coordinates_2d_C{camera}
    where camera is for each camera used in the triangulation
    """
    if n_cameras is None:
        n_cameras = len(np.unique(particles["camera"]))
    particles.index = particles["id"]
    coordinates_2d = particles["coordinates"]

    camera_coordinates = [coordinates_2d.loc[matched_particles["id"]].to_numpy().T]
    for camera in range(1, n_cameras):
        camera_coordinates.append(
            coordinates_2d.loc[matched_particles[f"id_C{camera}"]].to_numpy().T
        )
    current_n_columns = len(matched_particles.columns)
    for camera, current_coordinates_2d in enumerate(camera_coordinates):
        for i in range(2):
            matched_particles.insert(
                current_n_columns,
                "coordinates_2d_C{}".format(camera),
                current_coordinates_2d[i],
                allow_duplicates=True,
            )
            current_n_columns += 1
    return matched_particles


class MatchedParticlesPlotSlider(visualise_helpers.PlotSlider):
    def __init__(
        self,
        matched_particles,
        particles,
        images=None,
        clim=None,
        plot_lims=None,
        axis_equal=False,
        zaxis=2,
        assume_sorted_by_frame=True,
        verbose=False,
        **kwargs,
    ):
        if isinstance(images, tuple):
            image_shape = images
            images = None
        elif hasattr(images, "shape"):
            image_shape = images.shape[-2:]
        else:
            images = None
            image_shape = None

        if not hasattr(self, "plotting_variables"):
            self.plotting_variables = {}
        n_cameras = len(np.unique(particles["camera"]))
        self.plotting_variables["n_cameras"] = n_cameras
        self.plotting_variables["back_color"] = "blue"
        self.plotting_variables["back_alpha"] = 0.5
        self.plotting_variables["zaxis"] = zaxis
        self.plotting_variables["plot_lims"] = plot_lims
        self.plotting_variables["axis_equal"] = axis_equal
        self.plotting_variables["frame_id_dict"] = {}
        self.plotting_variables["matched_particles"] = None
        particles.index = particles["id"]
        self.plotting_variables["particles"] = particles.copy()
        self.plotting_variables["images"] = images

        self.pick_prefix = "matching"

        if images is not None:
            if clim is None:
                clim = [None, None]
            norm = mcolors.Normalize(vmin=clim[0], vmax=clim[1])
            image_mappable = mcm.ScalarMappable(cmap="gray", norm=norm)
            self.plotting_variables["image_mappable"] = image_mappable
            self.mappable = image_mappable

        # Initialising PlotSlider
        frames_column = particles["frame"]
        frame_lims = (np.min(frames_column), np.max(frames_column))
        self.set_framelims(frame_lims)

        projection = ["3d"] + [None] * n_cameras
        super().__init__(n_plot_axs=n_cameras + 1, projection=projection, **kwargs)

        self.update_plotting_variables(matched_particles)

        all_frames = np.arange(frame_lims[0], frame_lims[1] + 1, dtype=int)
        all_intensities = np.zeros(len(all_frames))
        frames, intensity = np.unique(frames_column, return_counts=True)
        all_intensities[frames.astype(int) - all_frames[0]] = intensity
        self.update_intensities(all_frames, all_intensities)

        axs = self.plot_ax_list
        zaxis = self.plotting_variables["zaxis"]
        axis_equal = self.plotting_variables["axis_equal"]
        labels = ["X", "Y", "Z"]
        if zaxis is None:
            zaxis = 2
        axs[0].set_zlabel(labels[zaxis])
        axs[0].set_ylabel(labels[zaxis - 1])
        axs[0].set_xlabel(labels[zaxis - 2])

        if image_shape is None:
            plot_lims_2d = visualise_helpers.get_automated_plot_lims(
                particles["coordinates"].to_numpy()
            )
        else:
            plot_lims_2d = [[0, image_shape[1]], [image_shape[0], 0]]

        for ax in axs[1:]:
            ax.set_xlabel("x")
            ax.set_ylabel("y")

            ax.invert_yaxis()
            if plot_lims_2d is not None:
                ax.set_xlim(plot_lims_2d[0])
                ax.set_ylim(plot_lims_2d[1])
            if axis_equal:
                ax.set_aspect("equal")
        self.set_plot_lims()

    def set_plot_lims(self, matched_particles=None):
        if matched_particles is None:
            matched_particles = self.plotting_variables["matched_particles"]
        if self.plotting_variables["plot_lims"] is None and len(matched_particles) > 1:
            coordinates = matched_particles["coordinates"].to_numpy().astype(float)
            coordinates = coordinates[np.isfinite(coordinates[:, 0])]
            if len(coordinates) == 0:
                return
            plot_lims = visualise_helpers.get_automated_plot_lims(coordinates)

            axs = self.plot_ax_list
            zaxis = self.plotting_variables["zaxis"]
            axis_equal = self.plotting_variables["axis_equal"]
            if axis_equal:
                plot_lims = visualise_helpers.plot_lims_equal(plot_lims)
            axs[0].set_zlim(plot_lims[zaxis])
            axs[0].set_ylim(plot_lims[zaxis - 1])
            axs[0].set_xlim(plot_lims[zaxis - 2])

    def update_plotting_variables(self, matched_particles_update):
        particles = self.plotting_variables["particles"]
        n_cameras = self.plotting_variables["n_cameras"]
        if len(matched_particles_update) == 0:
            columns = list(particles.columns)
            columns.remove("camera")
            columns.insert(np.argmax(np.array(columns) == "coordinates"), "coordinates")
            columns += [f"id_C{camera}" for camera in range(1, n_cameras)]
            new_coordinates = np.concatenate(
                [[f"coordinates_2d_C{camera}"] * 2 for camera in range(n_cameras)]
            )
            columns += list(new_coordinates)
            matched_particles_update = pd.DataFrame(columns=columns)
        else:
            matched_particles_update = add_2d_coordinates_to_3d_matched(
                matched_particles_update.copy(), particles, n_cameras
            )
        matched_particles_update["camera"] = 0
        matched_particles_update["camera"] = np.nan

        frame_id_dict = self.plotting_variables["frame_id_dict"]
        if self.plotting_variables["matched_particles"] is None:
            # first update need to set extra stuff
            self.plotting_variables["n_cameras"] = n_cameras

            full_columns = matched_particles_update.columns
            particles_base_list = []
            for camera in range(n_cameras):
                current_camera_particles = particles[particles["camera"] == camera]
                new_dataframe = pd.DataFrame(
                    index=np.arange(len(current_camera_particles), dtype=int),
                    columns=full_columns,
                )
                new_dataframe[f"coordinates_2d_C{camera}"] = current_camera_particles[
                    "coordinates"
                ].to_numpy()
                for column in full_columns:
                    if (
                        column in current_camera_particles
                        and not column == "coordinates"
                    ):
                        new_dataframe[column] = current_camera_particles[
                            column
                        ].to_numpy()
                particles_base_list.append(new_dataframe)

            particles_base = pd.concat(particles_base_list)
            helpers.unset_column_duplicates(particles_base)
            particles_base.index = particles_base["frame"]
            self.plotting_variables["particles_base"] = particles_base
            matched_particles = particles_base.copy()
            helpers.reset_column_duplicates(matched_particles)
            matched_particles.reset_index(drop=True, inplace=True)
            matched_particles = matched_particles.sort_values(
                "frame", ignore_index=True
            )
            matched_particles.index = matched_particles["id"]
            self.plotting_variables["matched_particles"] = matched_particles

            frames_column = matched_particles["frame"]
            frames, indicies = np.unique(frames_column, return_index=True)
            splitted_ids = np.array_split(matched_particles["id"], indicies[1:])
            for frame, current_ids in zip(frames, splitted_ids):
                frame_id_dict[frame] = list(current_ids)

        helpers.unset_column_duplicates(matched_particles_update)

        if tuple(matched_particles_update.columns) != tuple(
            self.plotting_variables["particles_base"].columns
        ):
            # checking that columns correspond
            new_columns = matched_particles_update.columns
            self.plotting_variables["particles_base"] = self.plotting_variables[
                "particles_base"
            ].reindex(columns=new_columns)

            helpers.unset_column_duplicates(
                self.plotting_variables["matched_particles"]
            )
            self.plotting_variables["matched_particles"] = self.plotting_variables[
                "matched_particles"
            ].reindex(columns=new_columns)
            helpers.reset_column_duplicates(
                self.plotting_variables["matched_particles"]
            )

        frames_to_update = np.unique(matched_particles_update["frame"])
        current_particles_base = (
            self.plotting_variables["particles_base"].loc[frames_to_update].copy()
        )
        current_particles_base.index = current_particles_base["id"]

        matched_ids = set(matched_particles_update["id"])
        for camera in range(1, n_cameras):
            matched_ids = matched_ids.union(matched_particles_update[f"id_C{camera}"])
        not_matched_ids = list(
            set(current_particles_base["id"]).difference(matched_ids)
        )
        not_matched_particles = current_particles_base.loc[not_matched_ids]
        matched_particles_update = pd.concat(
            [matched_particles_update, not_matched_particles], ignore_index=True
        )
        matched_particles_update = matched_particles_update.sort_values(
            "frame", ignore_index=True
        )

        if len(matched_particles_update) > 0:
            current_ids = matched_particles_update["id"]
            matched_particles_update.index = current_ids
            helpers.unset_column_duplicates(
                self.plotting_variables["matched_particles"]
            )
            self.plotting_variables["matched_particles"].loc[
                current_ids
            ] = matched_particles_update
            helpers.reset_column_duplicates(
                self.plotting_variables["matched_particles"]
            )

            frames_column = matched_particles_update["frame"]
            frames, indicies = np.unique(frames_column, return_index=True)
            splitted_ids = np.array_split(current_ids, indicies[1:])
            for frame, current_ids in zip(frames, splitted_ids):
                frame_id_dict[frame] = list(current_ids)
                if frame in self.lru_cache:
                    # deleting frames in cache that are updated
                    del self.lru_cache[frame]

        self.set_frame(self.get_frame())

    def make_frame_artists(self, frame):
        n_cameras = self.plotting_variables["n_cameras"]
        data = [[] for i in range(n_cameras + 1)]

        if self.plotting_variables["images"] is not None:
            image = self.plotting_variables["images"][frame]
            for camera in range(n_cameras):
                data[camera + 1].append(
                    functools.partial(
                        maxes.Axes.imshow,
                        X=image[camera],
                        # extent=self.plotting_variables["extent"],
                        cmap=self.plotting_variables["image_mappable"].get_cmap(),
                        norm=self.plotting_variables["image_mappable"].norm,
                    )
                )

        frame_id_dict = self.plotting_variables["frame_id_dict"]
        if frame in frame_id_dict:
            matched_particles = self.plotting_variables["matched_particles"]
            for current_id in frame_id_dict[frame]:
                current_particle = matched_particles.loc[[current_id]]
                current_camera = current_particle["camera"].iloc[0]
                is_matched = np.isnan(current_camera)
                label = "{} {}".format(self.pick_prefix, current_id)
                if is_matched:
                    color = visualise_helpers.colorcycle[
                        current_id % len(visualise_helpers.colorcycle)
                    ]
                    data[0].append(
                        mplot3d.art3d.Line3D(
                            *current_particle["coordinates"].to_numpy().T,
                            color=color,
                            marker=".",
                            linestyle="",
                            picker=True,
                            label=label,
                        )
                    )
                    for camera in range(n_cameras):
                        data[camera + 1].append(
                            mlines.Line2D(
                                *current_particle[f"coordinates_2d_C{camera}"]
                                .to_numpy()
                                .T,
                                color=color,
                                marker=".",
                                linestyle="",
                                picker=True,
                                label=label,
                            )
                        )
                else:
                    current_camera = int(current_camera)
                    data[current_camera + 1].append(
                        mlines.Line2D(
                            *current_particle[f"coordinates_2d_C{current_camera}"]
                            .to_numpy()
                            .T,
                            color=self.plotting_variables["back_color"],
                            alpha=self.plotting_variables["back_alpha"],
                            marker=".",
                            linestyle="",
                            picker=True,
                            label=label,
                        )
                    )
        return data

    def on_pick_data(self, event):
        label = event.artist.get_label()
        if not label.startswith(self.pick_prefix):
            # should only pick the corrsponding points
            return

        n_cameras = self.plotting_variables["n_cameras"]
        matched_particles = self.plotting_variables["matched_particles"]
        current_id = int(label[len(self.pick_prefix) + 1 :])
        current_particle = matched_particles.loc[[current_id]]
        current_camera = current_particle["camera"].iloc[0]
        is_matched = np.isnan(current_camera)

        axs = self.plot_ax_list
        if is_matched:
            axs[0].plot(
                *current_particle["coordinates"].to_numpy().T, "+", color="black"
            )
            for camera in range(n_cameras):
                axs[camera + 1].plot(
                    *current_particle[f"coordinates_2d_C{camera}"].to_numpy().T,
                    "+",
                    color="black",
                )
        else:
            current_camera = int(current_camera)
            axs[current_camera + 1].plot(
                *current_particle[f"coordinates_2d_C{current_camera}"].to_numpy().T,
                "+",
                color="black",
            )

        text = ["id: {}".format(current_particle["id"].iloc[0])]
        for camera in range(1, n_cameras):
            text.append(
                "id_C{}: {}".format(camera, current_particle[f"id_C{camera}"].iloc[0])
            )
        if "matching_error" in current_particle:
            text.append(
                "matching_error: {:.3e}".format(
                    current_particle["matching_error"].iloc[0]
                )
            )
        if "matching_feature_error" in current_particle:
            text.append(
                "matching_feature_error: {:.3e}".format(
                    current_particle["matching_feature_error"].iloc[0]
                )
            )
        text = "\n".join(text)
        self.information_text(text)


def visualise_matched_particles(
    matched_particles: pd.DataFrame,
    particles: pd.DataFrame,
    images: Sequence[np.ndarray] = None,
    clim: Tuple[float, float] = None,
    plot_lims: List[Tuple[float, float]] = None,
    axis_equal: bool = False,
    zaxis: int = 2,
    assume_sorted_by_frame: bool = False,
    verbose: bool = False,
    **kwargs,
):
    """
    Visualise the matched and triangulated particles. Three axes are shown where the first is a 3D plot, the remaining are the views of the images.

    particles is the found particles
    if particles is none plot 3D otherwise plot the matching particles in 2D
    axis_equal is only used with 2D plotting

    :param matched_particles: the found matched particles
    :param particles: The particle pixel coordinates used in the matching. Same as output from ptv3py.find_particles.
    :param images: The images used for finding particles. If not given, the 2D particle positions will be without background
    :param clim: Used for the imshow function of the images
    :param plot_lims: 3D limits for the plotting. If not provided, the limits are automatically calculated from the triangulated positions.
    :param axis_equal: Should the 3D plotting have equal sizes for all axes.
    :param zaxis: For the 3D, which of the axes should be the z dimension in the data. Default is the last dimension.
    :param assume_sorted_by_frame: Should the matched positions be sorted.
    :param verbose: show extra information when clicking on object in the plotted data
    :param **kwargs: remaining arguments are used in the creation of a matplotlib figure and the visualise_helpers.PlotSlider class
    :returns: subclass of a matplotlib figure called a MatchedParticlesPlotSlider
    """
    if len(particles) == 0:
        return None

    plotslider_kwargs = dict(
        matched_particles=matched_particles,
        particles=particles,
        images=images,
        clim=clim,
        plot_lims=plot_lims,
        axis_equal=axis_equal,
        zaxis=zaxis,
        assume_sorted_by_frame=assume_sorted_by_frame,
        verbose=verbose,
        **kwargs,
    )
    ps = plt.figure(
        FigureClass=MatchedParticlesPlotSlider,
        **plotslider_kwargs,
    )
    return ps


######################### Matching parameter helper ########################


def particle_matching_parameters_helper(
    particles, camera_calibration, images, saturation=2**12 - 1, **kwargs
):
    """Creates one figure showing the images and one to tweak the parameters for finding particles"""
    ps = plt.figure(
        FigureClass=MatchedParticlesPlotSlider,
        matched_particles={},
        particles=particles,
        images=images,
        clim=[0, saturation],
        verbose=True,
        **kwargs,
    )
    mptf = plt.figure(
        FigureClass=Matching_particles_tweak_figure,
        plotslider=ps,
        camera_calibration=camera_calibration,
    )
    return mptf, ps


class Matching_particles_tweak_figure(mfigure.Figure):
    def __init__(self, plotslider, camera_calibration, **kwargs):
        function_signature = inspect.signature(particle_3d_matching.match_single_frame)
        self.function_parameters = {}
        self.other_function_parameters = {}
        self.possible_other_parameters = set()

        for name in function_signature.parameters:
            param = function_signature.parameters[name]
            if (
                # param.annotation in [int, float, str]
                param.annotation
                not in [callable, param.empty]
            ):
                annotation = param.annotation
                default = param.default if param.default is not param.empty else ""
                self.function_parameters[name] = dict(
                    annotation=annotation,
                    default=default,
                    empty=param.empty,
                    possible_values=None,
                )
            else:
                self.possible_other_parameters.add(name)

        figsize = (7, (len(self.function_parameters) + 2) / 2)
        kwargs["figsize"] = figsize
        super().__init__(**kwargs)

        self.camera_calibration = camera_calibration
        self.plotslider = plotslider
        self.particles = plotslider.plotting_variables["particles"].copy()
        helpers.unset_coordinate_columns(self.particles)
        self.particles.index = self.particles["frame"]

        self.create_input_axes()

    def set_clims(self, clims):
        self.clims_slider.set_val(clims)

    def create_input_axes(self):
        gridspec_kwargs = {
            "left": 0.05,
            "right": 0.99,
            "hspace": 0.1,
            "wspace": None,
        }
        with_images = self.plotslider.plotting_variables["images"] is not None
        gridspec = self.add_gridspec(
            with_images + len(self.function_parameters) + 1,
            4,
            width_ratios=[0.1, 0.3, 0.4, 0.2],
            **gridspec_kwargs,
        )

        if with_images:
            # First is just simple rangeslider for changing the color limits of the cmap
            clims_ax = self.add_subplot(gridspec[0, 1:-1])
            orientation = "horizontal"
            clims_norm = self.plotslider.plotting_variables["image_mappable"].norm
            clims_slider = mwidgets.RangeSlider(
                clims_ax,
                "clims",
                clims_norm.vmin,
                clims_norm.vmax,
                valinit=(clims_norm.vmin, clims_norm.vmax),
                valfmt="%d",
                valstep=1,
                orientation=orientation,
            )
            self.clims_slider = clims_slider

            def on_clims_update(clims):
                clims_norm.vmin = clims[0]
                clims_norm.vmax = clims[1]
                self.plotslider.canvas.draw_idle()

            clims_slider.on_changed(on_clims_update)

        def on_parameter_update(event):
            print("Matching")
            self.match_particles()
            print("Done")

        # Then inputs for all parameters are created
        for i, [name, param] in enumerate(self.function_parameters.items()):
            parameter_ax = self.add_subplot(gridspec[i + with_images, 2:])
            annotation = " [{}]".format(param["annotation"].__name__)
            param["widget"] = mwidgets.TextBox(
                parameter_ax,
                "{}{}".format(name, annotation),
                str(param["default"]),
            )
        button_ax = self.add_subplot(gridspec[-1, 1])
        self.update_button = mwidgets.Button(button_ax, "Update")
        self.update_button.on_clicked(on_parameter_update)

    def set_parameters(self, **parameter_values):
        for name, val in parameter_values.items():
            if name in self.function_parameters:
                self.function_parameters[name]["widget"].set_val(
                    str(val).replace("'", '"')
                )
            elif name in self.possible_other_parameters:
                self.other_function_parameters[name] = val
            else:
                raise ValueError(
                    "Parameter {} is not valid for finding function".format(name)
                )

    def match_particles(self):
        frame = self.plotslider.get_frame()

        current_parameters = {}
        current_parameters.update(self.other_function_parameters)
        (
            valid_input,
            current_parameters,
        ) = particle_finding_visualization.read_parameters(
            current_parameters, self.function_parameters
        )

        if not valid_input:
            self.canvas.draw_idle()
            return

        current_particles = self.particles.loc[frame].copy()

        matched_particles = particle_3d_matching.match_single_frame(
            current_particles, self.camera_calibration, **current_parameters
        )
        matched_particles["frame"] = frame
        helpers.set_coordinate_columns(matched_particles)

        self.plotslider.update_plotting_variables(matched_particles)
        self.plotslider.set_plot_lims()
