"""
Module for estimating 3D coordinated of particles by matching them between the cameras
"""
import numpy as np
import pandas as pd

from . import helpers
from . import camera_calibration_module


try:
    from tqdm import tqdm
except ModuleNotFoundError:
    pass


def calc_line_closest_points(lines1, lines2, combinations=True):
    if combinations:
        lines1 = np.expand_dims(lines1, 1)
        lines2 = np.expand_dims(lines2, 0)
        q1 = lines1[:, :, 0]
        q2 = lines2[:, :, 0]
        v1 = lines1[:, :, 1]
        v2 = lines2[:, :, 1]
    else:
        q1 = lines1[:, 0]
        q2 = lines2[:, 0]
        v1 = lines1[:, 1]
        v2 = lines2[:, 1]

    v1_norm_square = np.sum(np.square(v1), axis=-1)
    v2_norm_square = np.sum(np.square(v2), axis=-1)
    v1v2_norm_square = np.sum(v1 * v2, axis=-1)
    q_diff = q2 - q1
    v1_q_diff = np.sum(v1 * q_diff, axis=-1)
    v2_q_diff = np.sum(v2 * -q_diff, axis=-1)

    mul = 1 / (v1_norm_square * v2_norm_square - v1v2_norm_square**2)
    lambda1 = mul * (v2_norm_square * v1_q_diff + v1v2_norm_square * v2_q_diff)
    lambda2 = mul * (v1v2_norm_square * v1_q_diff + v1_norm_square * v2_q_diff)

    closest_p1 = q1 + np.expand_dims(lambda1, -1) * v1
    closest_p2 = q2 + np.expand_dims(lambda2, -1) * v2

    errors = np.linalg.norm(closest_p2 - closest_p1, axis=-1)
    return errors, closest_p1, closest_p2


def match_single_frame(
    particles,
    camera_calibration: callable,
    max_error: float,
    camera_to_keep: int = 0,
    coordinate_limits: list = None,
    feature_column: list = None,
):
    cameras = particles["camera"].to_numpy()
    unique_cameras = np.unique(cameras)
    if len(unique_cameras) < 2:
        # only particles in one camera
        return []

    pixel_coordinates = np.array(particles[helpers.COORDINATE_COLUMNS[:2]])
    lines = camera_calibration(pixel_coordinates, cameras)
    # find errors between the 3D lines from each camera

    lines1 = lines[cameras == 0]
    lines2 = lines[cameras == 1]
    errors, closest_p1, closest_p2 = calc_line_closest_points(lines1, lines2)
    errors[np.isnan(errors) | (errors > max_error)] = np.inf
    center_coordinates = (closest_p1 + closest_p2) / 2

    if coordinate_limits is not None:
        # this will be moved to inside the matching
        valid_inds = inside_coordinate_limits(center_coordinates, coordinate_limits)
        errors[~valid_inds] = np.inf

    if np.all(np.isinf(errors)):
        return []

    errors_scaled = None
    if feature_column is not None:
        # here for example image intensity can be used to help the matching
        column_name, feature_weight = feature_column[:2]
        weight_type = None
        if len(feature_column) > 2:
            weight_type = feature_column[2]
        feature = particles[column_name].to_numpy()
        if len(feature.shape) == 1:
            feature = np.expand_dims(feature, 1)
        feature1 = np.expand_dims(feature[cameras == 0], 1)
        feature2 = np.expand_dims(feature[cameras == 1], 0)
        feature_errors = np.linalg.norm(feature1 - feature2, axis=-1)
        if weight_type == "relative":
            # scale with maximum feature of each comparison
            max_feature = np.maximum(feature1, feature2)[..., 0]
            max_feature[max_feature == 0] = 1
            feature_weight = feature_weight / max_feature
        errors_scaled = errors / max_error + feature_errors * feature_weight
    if errors_scaled is None:
        errors_scaled = errors

    matched_inds = []
    matched_coordinates = []
    matched_errors = []
    matched_feature_errors = []
    for k in range(np.min(errors_scaled.shape)):
        index = np.argmin(errors_scaled)
        i, j = np.unravel_index(index, errors_scaled.shape)
        if np.isinf(errors_scaled[i, j]):
            break
        matched_errors.append(errors[i, j])
        if feature_column is not None:
            matched_feature_errors.append(feature_errors[i, j])
        errors_scaled[i, :] = np.inf
        errors_scaled[:, j] = np.inf
        matched_coordinate = center_coordinates[i, j]
        matched_coordinates.append(matched_coordinate)
        matched_inds.append([i, j])

    matched_coordinates = np.array(matched_coordinates)
    matched_inds = np.array(matched_inds)
    n_matched_particles = len(matched_inds)

    if n_matched_particles == 0:
        return []

    matched_particles = (
        particles[cameras == camera_to_keep]
        .iloc[matched_inds[:, camera_to_keep]]
        .copy()
    )

    matched_particles["x"] = matched_coordinates[:, 0]
    matched_particles["y"] = matched_coordinates[:, 1]
    matched_particles["z"] = matched_coordinates[:, 2]
    id_first = particles["id"][cameras == 0].iloc[matched_inds[:, 0]].to_numpy()
    id_second = particles["id"][cameras == 1].iloc[matched_inds[:, 1]].to_numpy()
    # always set camera 0 id
    matched_particles["id"] = id_first
    matched_particles["id_C1"] = id_second
    matched_particles["matching_error"] = matched_errors
    if feature_column is not None:
        matched_particles["matching_feature_error"] = matched_feature_errors
    return matched_particles


def threedmatch_particles(
    particles: pd.DataFrame,
    camera_calibration: camera_calibration_module.CameraCalibration,
    max_error: float,
    camera_to_keep: int = 0,
    coordinate_limits: list = None,
    feature_column: tuple = None,
    verbose: bool = False,
):
    """
    Currently only works with calibration of two cameras

    :param particles: A pandas.DataFrame with the followin columns:
        "id" of particles
        "frame" of particles
        "camera" of particles
        "x", "y" of the particles
        ... remainding columns are not used bur are copied to the returned array
    :param camera_calibration: The calibration of the cameras
    :param max_error: max error in particles triangulation to match particles
    :param camera_to_keep: remaining columns of which camera should the algorithm keep
        the id of the not kepy camera is added after the coordinates so you know
    :param coordinate_limits: list of 2 float lists, will be applied to limit the 3D matching, see function inside_coordinate_limits for details
    :param verbose: add progress bar to calculations

    :returns: matched_particles
        The columns are the following:
        "id" of the particle in camera_to_keep
        "frame" of the particle
        "coordinates", (x, y, z) three columns with same name for the particle in 3D
            use helpers.unset_coordinate_columns to get "x" "y" "z" columns instead
        "id_C1" of the other camera
        ... remaining columns of particles in camera_to_keep
    """
    unique_cameras = np.unique(particles["camera"])
    if len(unique_cameras) > 2:
        raise ValueError("3D matching currently only implemented for 2 cameras")

    particles = helpers.unset_coordinate_columns(particles.copy())
    frames_column = particles["frame"]
    frames = np.unique(frames_column)
    matched_particles = []

    progress = None
    if verbose and "tqdm" in globals():
        progress = tqdm(total=len(frames))
    for frame in frames:
        current_inds = frames_column == frame
        current_particles = particles[current_inds]
        current_matched_particles = match_single_frame(
            current_particles,
            camera_calibration,
            max_error,
            camera_to_keep,
            coordinate_limits,
            feature_column,
        )
        if len(current_matched_particles) > 1:
            matched_particles.append(current_matched_particles)
        if progress is not None:
            progress.update()
    if progress is not None:
        progress.close()

    columns = ["id", "frame", "x", "y", "z", "id_C1", "matching_error"]
    [columns.append(c) for c in particles.columns if c not in columns]

    if len(matched_particles) > 0:
        matched_particles = pd.concat(matched_particles, ignore_index=True)
        matched_particles = matched_particles[columns]
    else:
        matched_particles = pd.DataFrame(columns=columns)

    helpers.set_coordinate_columns(matched_particles)
    return matched_particles


def inside_coordinate_limits(coordinates, coordinate_limits):
    """
    Get a boolean array of which elements of coordinates are inside the coordinate limits
    inclusive lower limit and upper limit

    :param coordinates: array with a last dimension of size two or three
    :param coordinate_limits: [[xmin, xmax], [ymin, ymax], ([zmin, zmax])].
    :returns: boolean array
    """
    coordinates = np.asarray(coordinates)
    shape = coordinates.shape
    ndims = shape[-1]
    assert ndims == len(coordinate_limits)

    valid_coordinates = np.ones(shape[:-1], dtype=bool)
    for i in range(ndims):
        valid_coordinates = (
            valid_coordinates
            & (coordinates[..., i] >= coordinate_limits[i][0])
            & (coordinates[..., i] <= coordinate_limits[i][1])
        )
    return valid_coordinates


def apply_coordinate_limits(
    particles,
    coordinate_limits,
):
    """
    Remove coordinates outside a volume / rectangle defined by,
    coordinate_limits : [[xmin, xmax], [ymin, ymax], ([zmin, zmax])].
    """
    valid_particles = inside_coordinate_limits(
        particles["coordinates"].to_numpy(), coordinate_limits
    )
    return particles[valid_particles]
