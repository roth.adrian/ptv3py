"""
Module for tracking particles in time and 2D or 3D
The algorithm is called recursive 4 frame best estimate and it is based on the 4
frame best estimate with added recursion to get better results and collision handling
"""
import numpy as np
import pandas as pd

import collections
import functools
import copy

# import logging


try:
    from tqdm import tqdm
except ModuleNotFoundError:
    pass

from .particle_3d_matching import apply_coordinate_limits

# from . import helpers


def find_possible_next_particles(
    track_state,
    particle_frames_dict,
    verbose,
):
    """
    Helper function to find all possible next
    particles with the limitations of the input parameters
    The output is sorted firstly by lowest frame, then
    lowest acceleration and finally by lowest speed
    """
    (
        current_id,
        current_frame,
        track_length,
        jumped_frames_left,
        parameter_tracks,
    ) = track_state
    acctual_current_frame = current_frame

    possible_particles = []
    possible_states = []
    possible_scores = []
    # possible_tracks = []
    for n_jumped_frames in range(jumped_frames_left + 1):
        current_frame += 1
        if (
            current_frame not in particle_frames_dict
            or len(particle_frames_dict[current_frame][0]) == 0
        ):
            # If no particles found skip this frame
            continue

        # possible next particles in this frame to choose
        next_ids, next_particles = particle_frames_dict[current_frame]
        (
            possible_inds,
            possible_next_tracks,
            possible_next_scores,
        ) = parameter_tracks.validate_and_update(next_particles, n_jumped_frames)
        n_possible_inds = len(possible_next_tracks)
        if n_possible_inds == 0:
            continue

        sorted_inds = np.argsort(possible_next_tracks)
        possible_next_tracks = possible_next_tracks[sorted_inds]
        # possible_tracks.append(possible_next_tracks)

        possible_next_ids = next_ids[possible_inds][sorted_inds]
        possible_particles.append(possible_next_ids)

        possible_states.append(
            list(
                zip(
                    possible_next_ids,
                    np.ones(n_possible_inds, dtype=int) * current_frame,
                    np.ones(n_possible_inds, dtype=int) * (track_length + 1),
                    np.ones(n_possible_inds, dtype=int)
                    * (jumped_frames_left - n_jumped_frames),
                    possible_next_tracks,
                )
            )
        )
        possible_scores.append(possible_next_scores[sorted_inds])

    if len(possible_states) > 0:
        # possible_tracks = np.concatenate(possible_tracks)
        possible_states = np.concatenate(np.array(possible_states, dtype=object))
        possible_scores = np.concatenate(possible_scores)
        possible_particles = np.concatenate(possible_particles)

    if verbose:
        # For debugging
        print(
            '\tCurrent particle: {{"id": {}, "frame": {}, "jumps_left": {}, "length": {}}}'.format(
                current_id, acctual_current_frame, jumped_frames_left, track_length
            )
        )
        possible_particles_str = "[\n\t\t{}\n\t]".format(
            ",\n\t\t".join(
                [
                    '{{"id": {:.0f}, "frame": {:.0f}, "jumps_left": {:.0f}  {}}}'.format(
                        i, frame, jumps_left, str(trackers)
                    )
                    for i, frame, _, jumps_left, trackers in possible_states
                ]
            )
        )
        print("\tPossible particles: \n\t{}".format(possible_particles_str))

    return possible_particles, possible_scores, possible_states


def recursive_graph_tracking(
    start_back_track: list,
    start_track_state: list,
    start_track_score: float,
    dynamic_cache: dict,
    recursive_max_branches: int,
    initial_recursive_max_branches: int,
    find_possible_next_states: callable,
    dynamic_key_length: int = 3,
    **kwargs,
):
    """
    Recursive function (written as a loop) that performs a depth first search of possible states  of the graph tries to choose the optimal state with the score when finding a track using the find_possible_next_states function

    :param start_back_track: previous track to use with dynamic_cache mainly
    :param start_track_state: the state of the track, includes usually velocity and acceleration
    :param start_track_score: the score of this state when added to the track
    :param dynamic_cache: used to cache intermediate results in a dynamic programming approach
    :param recursive_max_branches: max number of branching for the recursion
    :param initial_recursive_max_branches: same as previous but only used in the first two recursive steps
    :param find_possible_next_states: function used to find which states are available from the current state. Here all args and kwargs will be inserted.
    :param dynamic_key_length: how many values back in time is required to know that the sub tree will only have one optimal value
    :param kwargs: these values are passed to the find_possible_next_states function
    :returns: Queue of the best track for a single starting position
    """

    def create_recursion_state(back_track, track_state, score, recursion_depth):
        return {
            "back_track": back_track,
            "state": track_state,
            "score": score,
            "recursion_depth": recursion_depth,
            "recursed": False,
            "children": [],
            "track": None,  # the final track
        }

    # Here we have a filo queue for the recursion
    tracking_queue = []
    base_recursion_state = create_recursion_state(
        start_back_track, start_track_state, start_track_score, 1
    )
    tracking_queue.append(base_recursion_state)

    while tracking_queue:
        recursion_state = tracking_queue.pop()
        score = recursion_state["score"]

        back_track = recursion_state["back_track"].copy()
        this_particle = int(back_track[-1])
        tuple_back_track = tuple(back_track[-dynamic_key_length:])

        if not recursion_state["recursed"]:
            state = recursion_state["state"]
            recursion_depth = recursion_state["recursion_depth"]

            if tuple_back_track in dynamic_cache:
                # this has already been calculated so return answer
                track, score_next = dynamic_cache[tuple_back_track]
                recursion_state["score"] = score + score_next
                recursion_state["track"] = track.copy()
                continue

            (
                possible_next_particles,
                possible_next_scores,
                possible_next_states,
            ) = find_possible_next_states(
                state,
                **kwargs,
            )

            if len(possible_next_particles) == 0:
                track = collections.deque([this_particle])
                dynamic_cache[tuple_back_track] = (track.copy(), 0)
                recursion_state["score"] = score
                recursion_state["track"] = track
                continue

            recursion_state["recursed"] = True
            tracking_queue.append(recursion_state)

            if recursion_depth < dynamic_key_length:
                if initial_recursive_max_branches > 0:
                    possible_next_particles = possible_next_particles[
                        :initial_recursive_max_branches
                    ]
            elif recursive_max_branches > 0:
                possible_next_particles = possible_next_particles[
                    :recursive_max_branches
                ]

            if len(back_track) >= dynamic_key_length:
                back_track.pop(0)
            back_track.append(None)
            for i, [next_particle, next_state, next_score] in enumerate(
                zip(
                    # reversing to go through the best candidate first and cache those results
                    possible_next_particles[::-1],
                    possible_next_states[::-1],
                    possible_next_scores[::-1],
                )
            ):
                back_track[-1] = next_particle
                child_recursion_state = create_recursion_state(
                    back_track.copy(), next_state, next_score, recursion_depth + 1
                )
                recursion_state["children"].append(child_recursion_state)
                tracking_queue.append(child_recursion_state)
        else:
            # Already passed this state once and now children of the state should be handled
            # Reversing to give preference to candidates ranked better in sorting by find_possible_next_states
            possible_tracks = [
                child["track"] for child in reversed(recursion_state["children"])
            ]
            possible_scores = [
                child["score"] for child in reversed(recursion_state["children"])
            ]

            best_track_ind = np.argmax(possible_scores)
            best_score = possible_scores[best_track_ind]
            new_track = possible_tracks[best_track_ind]
            new_track.appendleft(this_particle)
            dynamic_cache[tuple_back_track] = (new_track.copy(), best_score)

            # del recursion_state["children"]  # freeing memory
            recursion_state["track"] = new_track
            recursion_state["score"] = best_score + score

    return base_recursion_state


def sanitize_dynamic_cache(dynamic_cache, frame, particle_frames_dict):
    ids = set(particle_frames_dict[frame][0])
    for key in list(dynamic_cache.keys()):
        # particles in this frame is assumed to not be reused after this
        if key[0] in ids:
            del dynamic_cache[key]


def r4fbe_part(
    particles,
    parameter_tracks_base,
    min_track_length,
    recursive_max_branches,
    initial_recursive_max_branches,
    max_jumped_frames,
    verbose,
):
    """
    This is a seperate function that previously enabled multiprocessing. Not anymore (or what do you think?)
    The particles are assumed to be sorted by frame ascending
    """
    verbose_extra = False
    verbose_id = None
    if verbose > 1:
        verbose_extra = verbose - 1
        verbose = True
    elif verbose < 0:
        verbose_id = int(-verbose)
        verbose = True

    ids = particles["id"].to_numpy()
    particles.index = ids

    frames_column_series = particles["frame"]
    frames_column = frames_column_series.to_numpy()
    frames, indicies = np.unique(frames_column, return_index=True)
    max_frame = frames[-1]
    # coordinates = particles["coordinates"].to_numpy()
    # ndims = coordinates.shape[1]
    # Dict to easier find the particles in each frame,
    splitted_ids = np.array_split(ids, indicies[1:])
    splitted_particles = np.array_split(particles, indicies[1:])
    particle_frames_dict = {
        frame: [current_ids, parameter_tracks_base.extract_columns(current_particles)]
        for frame, current_ids, current_particles in zip(
            frames, splitted_ids, splitted_particles
        )
    }

    # Dict to keep track of the used particles
    used_particles = {current_id: False for i, current_id in enumerate(ids)}

    dynamic_cache = dict()  # helpers.LRU(1500000)
    particles_left = len(particles)
    tracked_particles = []
    progress = None

    partial_recursive_graph_tracking = functools.partial(
        recursive_graph_tracking,
        recursive_max_branches=recursive_max_branches,
        initial_recursive_max_branches=initial_recursive_max_branches,
        find_possible_next_states=find_possible_next_particles,
        # Below are kwargs for find_possible_next_particles
        particle_frames_dict=particle_frames_dict,
    )

    while particles_left > 0:
        if verbose and "tqdm" in globals():
            progress = tqdm(total=particles_left)
        all_tracks = np.empty(particles_left, dtype=object)
        track_scores = np.empty(particles_left)
        track_lengths = np.empty(particles_left)
        n_finished_particles = 0
        dynamic_cache.clear()
        for frame in frames:
            particle_ids, current_particles = particle_frames_dict[frame]
            for i, particle_id in enumerate(particle_ids):
                current_particle = parameter_tracks_base.getitem_from_columns(
                    current_particles, [i]
                )
                # current_particle = current_particles.loc[[particle_id]]

                if verbose_id is not None:
                    verbose_extra = verbose_id == particle_id
                if verbose_extra:
                    print(
                        "\n***** New track, id: {}, frame: {} *****".format(
                            particle_id, frame
                        )
                    )

                parameter_tracks = parameter_tracks_base.create_new_track(
                    current_particle
                )
                state = [
                    particle_id,
                    frame,
                    1,  # initial track length
                    max_jumped_frames,
                    parameter_tracks,
                ]

                recursion_results = partial_recursive_graph_tracking(
                    [particle_id],
                    state,
                    0,  # initial score of this particle
                    dynamic_cache,
                    verbose=verbose_extra,
                )
                track = recursion_results["track"]
                score = recursion_results["score"]
                if verbose_extra:
                    print("Final track: [{}, {}]".format(list(track), score))

                # Must have at least min_track_length particles to be a valid track
                n_valid_in_track = len(track)
                if n_valid_in_track < min_track_length:
                    # too short track
                    track = [track[0]]
                    score = 0
                all_tracks[n_finished_particles] = track
                track_scores[n_finished_particles] = score
                track_lengths[n_finished_particles] = len(track)
                n_finished_particles += 1
                if progress is not None:
                    progress.update()

            # removing results that will never be reused
            sanitize_dynamic_cache(dynamic_cache, frame, particle_frames_dict)
        if progress is not None:
            progress.close()

        all_single_particles = np.all(track_lengths == 1)
        argsorted_scores = np.argsort(track_scores)[::-1]
        for i in argsorted_scores:
            track = all_tracks[i]

            if np.sum([used_particles[current_id] for current_id in track]) > 0 or (
                not all_single_particles
                and track_lengths[i] == 1
                and max_frame - frames_column_series.loc[track[0]] < min_track_length
            ):
                # if any of the particles in the track has been used do not use track
                # or if a single particle is too close to the end and might be reused after collision control
                if verbose_extra:
                    print("Not accepted track,")
                    print(list(track))
                continue
            [used_particles.__setitem__(current_id, True) for current_id in track]
            tracked_particle = particles.loc[track]
            tracked_particles.append([track_scores[i], tracked_particle])
            if verbose_extra:
                print("Accepted track, score {}".format(track_scores[i]))
                print(tracked_particle)

        # Remove the particles that are used
        for frame in frames:
            current_ids, current_particles = particle_frames_dict[frame]
            if len(current_ids) == 0:
                continue
            current_used_particles = np.array(
                [used_particles[current_id] for current_id in current_ids]
            )
            current_unused_particles = ~current_used_particles
            # removing all the used particles
            particle_frames_dict[frame] = [
                current_ids[current_unused_particles],
                # current_particles[current_unused_particles],
                parameter_tracks_base.getitem_from_columns(
                    current_particles, current_unused_particles
                ),
            ]
            particles_left -= np.sum(current_used_particles)
    return tracked_particles


def create_parameter_tracks(
    coordinate_parameter_tracker,
    particles,
    min_speed=0,
    max_speed=-1,
    max_acceleration=-1,
    max_extrapolation_error=-1,
    initial_velocity=None,
    initial_max_acceleration=None,
    extrapolation_error_scaling=None,
    jumped_frames_penalty=0,
    track_columns=None,
):
    if coordinate_parameter_tracker is None:
        limits = np.array(
            [
                [min_speed, max_speed],
                [np.nan, max_acceleration],
            ]
        )
        initial_derivatives = None
        initial_limits = None
        if initial_velocity is not None:
            initial_derivatives = [initial_velocity]
        if initial_max_acceleration is not None:
            initial_limits = np.array(
                [
                    [np.nan, np.nan],
                    [np.nan, initial_max_acceleration],
                ]
            )

        coordinate_parameter_tracker = Parameter_derivative_tracking(
            "coordinates",
            n_derivatives=2,
            limits=limits,
            max_extrapolation_error=max_extrapolation_error,
            jumped_frames_penalty=jumped_frames_penalty,
            initial_derivatives=initial_derivatives,
            initial_limits=initial_limits,
            extrapolation_error_scaling=extrapolation_error_scaling,
        )
    elif isinstance(coordinate_parameter_tracker, dict):
        if "name" not in coordinate_parameter_tracker:
            coordinate_parameter_tracker["name"] = "coordinates"
        if jumped_frames_penalty > 0:
            coordinate_parameter_tracker[
                "jumped_frames_penalty"
            ] = jumped_frames_penalty
        coordinate_parameter_tracker = Parameter_derivative_tracking(
            **coordinate_parameter_tracker
        )
    elif not isinstance(coordinate_parameter_tracker, Parameter_derivative_tracking):
        raise ValueError(
            "coordinate_parameter_tracker must be either a dict or Parameter_derivative_tracking"
        )

    if track_columns is not None:
        for tc in track_columns:
            if isinstance(tc, str):
                column_name = tc
                scale = np.max(particles[column_name])
                tc = dict(
                    name=column_name,
                    limits=[0, -1],
                    max_extrapolation_error=-1,
                    n_derivatives=0,
                    extrapolation_error_scaling=scale,
                )
            sub_tracker = Parameter_derivative_tracking(**tc)
            coordinate_parameter_tracker.add_sub_tracker(sub_tracker)
    return coordinate_parameter_tracker


def recursive_4frame_best_estimate(
    particles,
    min_speed: float = 0,
    max_speed: float = -1,
    max_acceleration: float = -1,
    max_extrapolation_error: float = -1,
    initial_velocity: list = None,
    initial_max_acceleration: float = None,
    extrapolation_error_scaling: float = None,
    track_columns: list = None,
    coordinate_parameter_tracker=None,
    max_jumped_frames: int = 0,
    jumped_frames_penalty: float = 1,
    min_track_length: int = 4,
    recursive_max_branches: int = -1,
    initial_recursive_max_branches: int = -1,
    add_track_score: bool = False,
    verbose: bool or int = False,
):
    """
    Use special version of the four frames algorithm to track particles
    First guess the second tracked particle as the closest one.
    If this particle confides to the speed limits the algorithm tries to find a third particle using the velocity between the first two particles for the expected location.
    If the closest particle to this expected location corresponds to a speed larger than min_speed and lower than max_speed and the difference betweel the velocities are lower than max_acceleration the third particle is added.
    The fourth is found similarily but through minimising the change of acceleration
    Continue to find particles as the fourth until not all of the requirements are met then the track is returned.
    This is implemented by propagating a recursive tree found in the function recursive_graph_tracking

    particles,
    :param particles: The required columns are the following:
        "id" of particles
        "frame" of particles
        "coordinates", (x, y[, z]) two or three columns with same name
        ... remainding columns are not used bur are copied to the returned output
    :param min_speed: the minimum euclidiean distance a particle is allowed to move between two frames
    :param max_speed: the maximum euclidiean distance a particle is allowed to move between two frames
    :param max_acceleration: how large is the particle absolute acceleration allowed to be between two frames
    :param max_extrapolation_error: how large is the acceleration allowed to change between two frames
    :param initial_velocity: Arraylike with length equal to ndims. When tracking and searching for the second particle use this velocity to search
    :param initial_max_acceleration: max acceleration that only applies for the second particle to be found, default is to accept all if initial_velocity is unset and otherwise set it to 0.5. If used this is relative and will be multiplied with the absolute initial_velocity to get the max_acceleration
    :param extrapolation_error_scaling: The scale used when calculating the score using extrapolation error.
    :param track_columns: Extra columns to track such as particle intensity. This is then a list of either strings or dicts. If strings, each should name a column that will be used in the tracking. If a dict then it is the kwargs for creating a Parameter_derivative_tracking object as sub_tracker to the coordinates tracking.
    :param coordinate_parameter_tracker: Precreated Parameter_derivative_tracking object to be used in the tracking
    :param max_jumped_frames: allow the algorithm to jump a few frames in an attempt to keep tracking a particle
    :param jumped_frames_penalty: penalty for jumping frames
    :param min_track_length: minimum number of particles for the track to be valid
    :param recursive_max_branches: how many branches per particle are allowed, default is -1 which accepts everything. To lower computation time this can be changed.
    :param initial_recursive_max_branches: same as previous but with how many allowed in the first two recursive steps
    :param add_track_score: if true a column with the "track_score" is added to output
    :param verbose : print some extra information, can also be verbosity levels where 1 is general info and 2 is more detailed info for each recursion
    :returns: tracked_particles
        The columns are the following:
        "id_track", each particle track has its own id
        "id" of particles
        "frame" of particles
        "coordinates", (x, y[, z]) two or three columns with same name for the particle in 3D
            use helpers.unset_coordinate_columns to get ("x" "y"[, "z"]) columns instead
        ... remainding columns of input array
    """
    if len(particles) == 0:
        return []

    parameter_tracks_base = create_parameter_tracks(
        coordinate_parameter_tracker,
        particles=particles,
        min_speed=min_speed,
        max_speed=max_speed,
        max_acceleration=max_acceleration,
        max_extrapolation_error=max_extrapolation_error,
        initial_velocity=initial_velocity,
        initial_max_acceleration=initial_max_acceleration,
        extrapolation_error_scaling=extrapolation_error_scaling,
        jumped_frames_penalty=jumped_frames_penalty,
        track_columns=track_columns,
    )

    particles = particles.sort_values("frame", ignore_index=True)

    tracking_kwargs = {
        "parameter_tracks_base": parameter_tracks_base,
        "min_track_length": min_track_length,
        "recursive_max_branches": recursive_max_branches,
        "initial_recursive_max_branches": initial_recursive_max_branches,
    }

    tracked_particles = r4fbe_part(
        particles,
        **tracking_kwargs,
        max_jumped_frames=max_jumped_frames,
        verbose=verbose,
    )

    # sorting by the start frame of each track
    tracked_particles = sorted(
        tracked_particles, key=lambda track: track[1]["frame"].iloc[0]
    )
    # adding track number before concatenation
    for track_id, [track_score, tracked_particle] in enumerate(tracked_particles):
        tracked_particle.insert(0, "id_track", track_id)
        if add_track_score:
            tracked_particle["track_score"] = track_score

    tracked_particles = [tracked_particle for _, tracked_particle in tracked_particles]

    # return everything as a single DataFrame
    tracked_particles = pd.concat(tracked_particles, ignore_index=True)
    return tracked_particles


# Current base tracking algorithm
track_particles = recursive_4frame_best_estimate


########## Helper functions to use after the track estimation ###########


def filter_by_track_length(tracked_particles, min_track_length):
    unique_track_ids, track_inverse, track_lengths = np.unique(
        tracked_particles["id_track"],
        return_inverse=True,
        return_counts=True,
    )
    unique_track_ids[track_lengths < min_track_length] = -1
    valid_tracks = unique_track_ids[track_inverse] >= 0
    tracked_particles = tracked_particles[valid_tracks].copy()
    return tracked_particles


def tracked_first_occurence(tracked_particles, add_track_lengths=True):
    """Find the first occurence of each tracked particle
    :param tracked_particles : pandas.DataFrame
        output from track particles
    :param add_track_lengths : bool
        if true a column will be added last with the lenght of each track
    """
    _, first_indicies, track_lengths = np.unique(
        tracked_particles["id_track"],
        return_index=True,
        return_counts=True,
    )
    particles_first_occurence = tracked_particles.iloc[first_indicies].copy()
    if add_track_lengths:
        particles_first_occurence["track_length"] = track_lengths

    return particles_first_occurence


def tracked_largest_occurence(
    tracked_particles,
    size_column="size",
    add_track_lengths=True,
    add_relative_max_indexs=False,
    add_jump_ratios=False,
):
    """Find the largest occurence of each tracked particle
    Assume that there is some kind of size for the tracked particles in the last column

    :param tracked_particles : pandas.DataFrame
        output from track particles
    :param size_column : string
        which column contains information of particle_size
    :param add_track_lengths : bool
        if true a column will be added last with the lenght of each track
    :param add_relative_max_indexs : bool
        if true the relative index of the largest occurence (between 0 and len(track) - 1) is also added as column
    :param add_jump_ratios : bool
        if true a jump ratio column will be added (connected to max_jumped_frames in the tracking) for each tracked particle. If frames is the frame for each particle in the track and n_particles is the number of particles in the track,
        jump_ratio = ((frames[-1] - frames[0]) - (n_particles - 1)) / (n_particles - 1). In other words it is the average number of jumps per particle in track.
    """
    track_ids = tracked_particles["id_track"].to_numpy()
    size_column = tracked_particles[size_column].to_numpy()

    unique_particles, track_lengths = np.unique(track_ids, return_counts=True)
    if add_relative_max_indexs:
        relative_max_indexs = np.zeros(len(unique_particles)) * np.nan
    if add_jump_ratios:
        frame_column = tracked_particles["frame"].to_numpy()
        jump_ratios = np.zeros(len(unique_particles)) * np.nan

    largest_particle_indexs = []
    largest_particles = np.empty((len(unique_particles), tracked_particles.shape[1]))
    for i, particle in enumerate(unique_particles):
        current_track_indexs = np.flatnonzero(track_ids == particle)
        max_index = np.argmax(size_column[current_track_indexs])
        largest_particle_indexs.append(current_track_indexs[max_index])
        if add_relative_max_indexs:
            relative_max_indexs[i] = max_index
        if add_jump_ratios and track_lengths[i] > 1:
            n_particles_anti_inc = track_lengths[i] - 1
            frames = frame_column[current_track_indexs]
            jump_ratios[i] = (frames[-1] - frames[0] - n_particles_anti_inc) / (
                n_particles_anti_inc
            )

    largest_particles = tracked_particles.iloc[largest_particle_indexs].copy()
    if add_track_lengths:
        largest_particles["track_length"] = track_lengths
    if add_relative_max_indexs:
        largest_particles["relative_max_index"] = relative_max_indexs
    if add_jump_ratios:
        largest_particles["jump_ratio"] = jump_ratios

    return largest_particles


class TracksIterator:
    def __init__(
        self, tracked_particles, assume_sorted_by_frame=False, id_key="id_track"
    ):
        if not assume_sorted_by_frame:
            tracked_particles = tracked_particles.sort_values([id_key, "frame"])

        self.track_ids, track_indicies = np.unique(
            tracked_particles[id_key],
            return_index=True,
        )
        self.splitted_tracked_particles = np.array_split(
            tracked_particles, track_indicies[1:]
        )
        self.tracks_dict = {
            track_id: track
            for track_id, track in zip(self.track_ids, self.splitted_tracked_particles)
        }

        self.i = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.i >= len(self):
            raise StopIteration()
        next_values = (self.track_ids[self.i], self.splitted_tracked_particles[self.i])
        self.i += 1
        return next_values

    def __len__(self):
        return len(self.track_ids)

    def __getitem__(self, i):
        if isinstance(i, int):
            return (self.track_ids[i], self.splitted_tracked_particles[i])
        elif isinstance(i, (list, tuple, np.ndarray)):
            # return new iterator with the indexes
            indexs = np.asarray(i)
            assert len(indexs.shape) == 1
            assert indexs.dtype in [bool, int]
            new_iterator = self.copy()
            new_iterator.track_ids = new_iterator.track_ids[indexs]
            new_iterator.splitted_tracked_particles = [
                new_iterator.splitted_tracked_particles[j] for j in indexs
            ]
            new_iterator.reset()
            return new_iterator
        else:
            raise ValueError("Index type {} not supported".format(type(i)))

    def get_track_id(self, track_id):
        return self.tracks_dict[track_id]

    def reset(self):
        self.i = 0

    def copy(self):
        return copy.copy(self)


def tracked_velocities(
    tracked_particles,
    dt,
    with_accelerations=False,
    with_extrapolation_error=False,
    no_jumps=False,
    return_average_particles=False,
    add_jump_ratios=False,
    add_turning_ratios=False,
    with_progress=False,
):
    """
    Return estimated velocities and some interesting information for all tracks together with the frame for the event
    The output columns are:
        "frame"
        "id_track"
        "coordinates"
        "velocities",
        ["accelerations"]
        ["extrapolation_error"]
    input of time dt between frames is required
    if no_jumps is true then particles tracks with jumps are altered
    if return_average_particles another frame will be returned aswell where the average of each track is taken. In addition, a column is added with the average speed (and normed acceleration) that can be different from the average displacement otherwise found.

    Some extra information can be added to the average particle output:
        add_jump_ratios, if true a jump ratio column will be added (connected to max_jumped_frames in the tracking) for each tracked particle. If frames is the frame for each particle in the track and n_particles is the number of particles in the track,
            jump_ratio = ((frames[-1] - frames[0]) - (n_particles - 1)) / (n_particles - 1). In other words it is the average number of jumps per particle in track.
        add_turning_ratios, average degrees the particle is turning during the track. sum(abs(turns)) / (n_particles - 1)
    """
    if with_accelerations:
        if with_extrapolation_error:
            coordinate_slice = slice(2, -1)
            velocity_slice = slice(1, -1)
            acceleration_slice = slice(-1)
            min_track_length = 4
        else:
            coordinate_slice = slice(1, -1)
            velocity_slice = slice(-1)
            acceleration_slice = slice(None)
            min_track_length = 3
    else:
        coordinate_slice = slice(-1)
        velocity_slice = slice(None)
        acceleration_slice = slice(None)
        min_track_length = 2

    _, track_inverse, track_lengths = np.unique(
        tracked_particles["id_track"], return_inverse=True, return_counts=True
    )
    valid_particles = track_lengths[track_inverse] >= min_track_length
    tracked_particles = tracked_particles.loc[valid_particles]

    # unique_particles, track_indicies = np.unique(
    #     tracked_particles["id_track"],
    #     return_index=True,
    # )
    # splitted_tracked_particles = np.array_split(tracked_particles, track_indicies[1:])
    tracks_iterator = TracksIterator(tracked_particles)

    track_information = []
    averaged_track_information = []
    progress = None
    if with_progress:
        progress = tqdm(total=len(tracks_iterator))
    for track_id, current_track in tracks_iterator:
        current_frames = current_track["frame"].to_numpy().astype(int)
        diff_frames = np.expand_dims(np.diff(current_frames), 1)
        current_coordinates = current_track["coordinates"].to_numpy().astype(np.float64)
        current_velocities = np.diff(current_coordinates, axis=0) / (dt * diff_frames)
        sliced_current_velocities = current_velocities[velocity_slice]

        sliced_current_coordinates = current_coordinates[coordinate_slice]
        information = [
            ["coordinates", sliced_current_coordinates.T],
            ["velocities", sliced_current_velocities.T],
            ["speed", [np.linalg.norm(sliced_current_velocities, axis=1)]],
        ]
        if with_accelerations:
            current_accelerations = np.diff(current_velocities, axis=0) / (
                dt * (diff_frames[:-1] + diff_frames[1:]) / 2
            )
            sliced_current_accelerations = current_accelerations[acceleration_slice]
            information.append(["accelerations", sliced_current_accelerations.T])
            information.append(
                [
                    "normed_acceleration",
                    [np.linalg.norm(sliced_current_accelerations, axis=1)],
                ]
            )
            if with_extrapolation_error:
                current_extrapolations = (
                    sliced_current_coordinates
                    + sliced_current_velocities
                    + sliced_current_accelerations
                )
                current_extrapolation_errors = np.linalg.norm(
                    current_extrapolations - current_coordinates[3:]
                )
                information.append(
                    ["extrapolation_error", [current_extrapolation_errors]]
                )

        current_frames = current_frames[coordinate_slice]
        current_ids = current_track["id"].to_numpy()[coordinate_slice]

        current_track_information = pd.DataFrame(
            {"id": current_ids, "frame": current_frames}
        )
        current_track_information.insert(0, "id_track", track_id)
        ind = current_track_information.shape[1]
        for label, values in information:
            for i in range(len(values)):
                current_track_information.insert(
                    ind, label, values[i], allow_duplicates=True
                )
                ind += 1

        if no_jumps:
            valid = diff_frames[velocity_slice].ravel() == 1
            if isinstance(no_jumps, (int, float)) and np.sum(valid) < no_jumps:
                # no_jumps is min length as well and this is too short
                continue
            current_track_information = current_track_information.loc[valid]

        if return_average_particles:
            temp_average = np.mean(current_track_information, axis=0)
            current_averaged_track_information = pd.DataFrame(
                temp_average.to_numpy().reshape((1, -1)),
                index=[0],
                columns=temp_average.index,
            )
            current_averaged_track_information["id_track"] = track_id

            # average_speed = np.mean(
            #     np.linalg.norm(current_track_information["velocities"], axis=1)
            # )
            # current_averaged_track_information["speed"] = average_speed
            # if with_accelerations:
            #     average_normed_acceleration = np.mean(
            #         np.linalg.norm(current_track_information["accelerations"], axis=1)
            #     )
            #     current_averaged_track_information[
            #         "normed_acceleration"
            #     ] = average_normed_acceleration

        if add_jump_ratios:
            n_particles_anti_inc = len(current_track_information) - 1
            frames = current_track_information["frame"].to_numpy()
            jump_ratio = (frames[-1] - frames[0] - n_particles_anti_inc) / (
                n_particles_anti_inc
            )
            if return_average_particles:
                current_averaged_track_information["jump_ratio"] = jump_ratio
            current_track_information["jump_ratio"] = jump_ratio
        if add_turning_ratios:
            phi = np.arctan2(*current_velocities.T[1::-1])
            if current_velocities.shape[1] == 2:
                abs_angle_diffs = np.abs(np.diff(phi))
                abs_angle_diffs = np.mod(abs_angle_diffs, np.pi)
                # turning_ratio = np.sum(np.abs(np.diff(phi))) / n_particles_anti_inc
            else:
                raise NotImplementedError("For 3D tracks")
                r = np.linalg.norm(current_velocities[:, :2], axis=1)
                theta = np.arctan2(r, current_velocities[:, 2])
                abs_angle_diffs = np.sqrt(np.diff(phi) ** 2 + np.diff(theta) ** 2)
            turning_ratio = np.mean(abs_angle_diffs)
            if return_average_particles:
                current_averaged_track_information["turning_ratio"] = turning_ratio
            current_track_information["turning_ratio"] = turning_ratio

        if return_average_particles:
            averaged_track_information.append(current_averaged_track_information)

        track_information.append(current_track_information)
        if progress is not None:
            progress.update()
    if progress is not None:
        progress.close()

    if len(track_information) > 0:
        track_information = pd.concat(track_information, copy=False, ignore_index=True)

    if not return_average_particles:
        return track_information
    else:
        if len(averaged_track_information) > 0:
            averaged_track_information = pd.concat(
                averaged_track_information, copy=False, ignore_index=True
            )
        return track_information, averaged_track_information


def estimate_displacements(
    tracked_particles,
    coordinate_limits=None,
    correct_drift=False,
    no_jumps=False,
    with_progress=False,
):
    # setting dt to 1 to get displacement
    displacement_information = tracked_velocities(
        tracked_particles, 1, add_jump_ratios=True, with_progress=with_progress
    )
    if coordinate_limits is not None:
        displacement_information = apply_coordinate_limits(
            displacement_information, coordinate_limits
        )
    if len(displacement_information) == 0:
        return np.zeros(0), np.zeros(0)

    drift = 0
    if correct_drift:
        drift = np.mean(displacement_information["velocities"], axis=0)

    if no_jumps:
        displacement_information = displacement_information[
            displacement_information["jump_ratio"] == 0
        ]

    # frames = tracked_particles["frame"]
    frames = displacement_information["frame"]
    displacements = np.linalg.norm(
        displacement_information["velocities"] - drift, axis=1
    )

    all_frames = np.arange(np.min(frames), np.max(frames), dtype=int)
    average_displacements = np.zeros(len(all_frames)) * np.nan
    for i, frame in enumerate(all_frames):
        current_displacements = displacements[frames == frame]
        if len(current_displacements) == 0:
            continue
        average_displacements[i] = np.mean(current_displacements)
    return all_frames, average_displacements


def estimate_average_displacement(
    tracked_particles, coordinate_limits=None, with_progress=False
):
    return np.nanmean(
        estimate_displacements(tracked_particles, coordinate_limits, with_progress)[1]
    )


def estimate_spacings(
    tracked_particles,
    coordinate_limits=None,
    max_n_samples=None,
    min_particles_per_frame=2,
    with_progress=False,
):
    if coordinate_limits is not None:
        tracked_particles = apply_coordinate_limits(
            tracked_particles, coordinate_limits
        )
    frames = tracked_particles["frame"].to_numpy()
    all_coordinates = tracked_particles["coordinates"].to_numpy()

    all_frames = np.arange(np.min(frames), np.max(frames), dtype=int)
    average_spacings = np.zeros(len(all_frames)) * np.nan
    # When one found min distance a second for the other points is also found
    progress = None
    if with_progress:
        progress = tqdm(total=len(all_frames))
    for i, frame in enumerate(all_frames):
        # Estimate the mean min distance between particles
        current_coordinates = all_coordinates[frames == frame]
        if len(current_coordinates) < min_particles_per_frame:
            if progress is not None:
                progress.update()
            continue

        if max_n_samples is None:
            current_indicies = range(len(current_coordinates))
        else:
            current_indicies = np.random.permutation(len(current_coordinates))[
                :max_n_samples
            ]
        current_spacing_sum = 0
        for j in current_indicies:
            # Take the difference of all possible combinations of the particles
            current_coordinate_diffs = current_coordinates[j] - current_coordinates
            current_diffs = np.linalg.norm(current_coordinate_diffs, axis=-1)
            current_diffs[j] = np.inf
            current_spacing_sum += np.min(current_diffs)

            # coordinate_diffs = np.expand_dims(current_coordinates, 0) - np.expand_dims(
            #     current_coordinates, 1
            # )
            # diffs = np.linalg.norm(coordinate_diffs, axis=-1)
            # # diagonal elements is the distance to itself which is not interesting
            # np.fill_diagonal(diffs, np.inf)
            # min_distances = np.min(diffs, axis=1)
        average_spacings[i] = current_spacing_sum / len(current_indicies)
        # average_spacings[i] = np.mean(min_distances)
        if progress is not None:
            progress.update()
    if progress is not None:
        progress.close()
    return all_frames, average_spacings


def estimate_average_spacing(
    tracked_particles, coordinate_limits=None, max_n_samples=None, with_progress=False
):
    return np.nanmean(
        estimate_spacings(
            tracked_particles, coordinate_limits, max_n_samples, with_progress
        )[1]
    )


def estimate_displacement_spacing_ratio(
    tracked_particles,
    return_averaged=False,
    coordinate_limits=None,
    max_n_frames=None,
    max_n_samples=None,
    displacement_tuple=None,
    spacing_tuple=None,
    with_progress=False,
):
    """
    Estimate the ratio between the displacement between each frame and the distance between particles.
    This value indicates how hard the tracking of this dataset is/was and can be used to estimate how many of the tracks are wrong.
    A ratio is estimated for each frame found in the tracked particles.
    Remember that this is an estimate given that the tracking performed has worked. It will probably not give large values even though it might be the case.

    tracked_particles: similar as output from track_particles function.
    ndims: number of coordinate dimensions
    return frames, ratios
    if return_averaged:
        return mean(ratios)
    """
    if max_n_frames is not None:
        frames = tracked_particles["frame"].to_numpy()
        min_frame = np.min(frames)
        tracked_particles = tracked_particles[frames <= min_frame + max_n_frames]
    if displacement_tuple is None:
        displacement_tuple = estimate_displacements(
            tracked_particles, coordinate_limits, with_progress=with_progress
        )
    displacement_frames, average_displacements = displacement_tuple
    if spacing_tuple is None:
        spacing_tuple = estimate_spacings(
            tracked_particles,
            coordinate_limits,
            max_n_samples,
            with_progress=with_progress,
        )
    frames, average_spacings = spacing_tuple
    average_displacements = np.concatenate(
        (
            np.zeros(displacement_frames[0] - frames[0]) * np.nan,
            average_displacements,
            np.zeros(frames[-1] - displacement_frames[-1]) * np.nan,
        )
    )
    if return_averaged:
        return np.nanmean(average_displacements / average_spacings)
    else:
        return frames, average_displacements / average_spacings


class Parameter_derivative_tracking:
    """
    Class for helping the tracking of coordinates and other parameters such as particle intensity
    Is also used to estimate the score (feature of merit) of a track that is a number corresponding to how "good" the track is.
    """

    def __init__(
        self,
        name: str,
        limits: np.ndarray = None,
        max_extrapolation_error: float = -1,
        n_derivatives: int = 0,
        jumped_frames_penalty: float = 0,
        initial_derivatives: np.ndarray = None,
        initial_limits: np.ndarray = None,
        extrapolation_error_scaling: float = None,
        min_extrapolation_error: float = 0,
        relative_limits: bool = False,
    ):
        """
        Initalizing an parameter tracking object

        :param name: name of the column that should be tracked. The default use is for "coordinates" that is either 2 or 3 values for each row.
        :param limits: the minimum and maximum limiting values for each derivative as a single row. nan values can be used to set no limits
        :param max_extrapolation_error: the limiting extrapolation error in the tracking
        :param n_derivatives: number of derivatives to use for extrapolation
        :param jumped_frames_penalty: penalty for jumping frames in the tracking
        :param initial_derivatives: Assumed starting derivatives for a track
        :param initial_limits: Separate limitations in the first frame of a track
        :param extrapolation_error_scaling: how to scale extrapolation errors in the score
        :param min_extrapolation_error: limits the minimum error, not commonly used
        :param relative_limits: should the limits be relative to the previous value
        """
        # dict for everything to avoid copying stuff for every new particle
        self.static_attributes = dict()
        self.static_attributes["name"] = name
        self.static_attributes["relative_limits"] = relative_limits
        self.static_attributes["value_score"] = n_derivatives < 0
        n_derivatives = max(0, n_derivatives)
        ### Current max of derivatives in tracking
        if n_derivatives > 2:
            raise ValueError("Derivative larger than 2 is currently not possible")
        self.static_attributes["n_derivatives"] = n_derivatives

        self.static_attributes["limits"] = np.empty((n_derivatives + 1, 2))
        if n_derivatives > 0:
            if limits is None:
                limits = np.ones(2) * np.nan
            limits = np.asarray(limits, dtype=float)
            # broadcast to correct shape
            limits = np.ones((n_derivatives, 1)) * limits
            lower_limits = limits[:, 0]
            lower_limits[np.isnan(lower_limits)] = 0
            limits[:, 0] = lower_limits
            upper_limits = limits[:, 1]
            upper_limits[(upper_limits <= 0) | np.isnan(upper_limits)] = np.inf
            limits[:, 1] = upper_limits
            self.static_attributes["limits"][:-1] = limits
        max_extrapolation_error = (
            max_extrapolation_error if max_extrapolation_error > 0 else np.inf
        )
        self.static_attributes["limits"][-1] = [
            min_extrapolation_error,
            max_extrapolation_error,
        ]

        if extrapolation_error_scaling is None:
            extrapolation_error_scaling = max_extrapolation_error
        self.static_attributes[
            "extrapolation_error_scaling"
        ] = extrapolation_error_scaling
        self.static_attributes["jumped_frames_penalty"] = jumped_frames_penalty

        if initial_derivatives is not None and n_derivatives > 0:
            initial_derivatives = np.asarray(initial_derivatives, dtype=float)
            assert (
                len(initial_derivatives) <= n_derivatives
            ), "initial derivatives longer than n_derivatives"
            self.static_attributes["initial_derivatives"] = initial_derivatives

        if initial_limits is not None and n_derivatives > 0:
            initial_limits = np.asarray(initial_limits, dtype=float)
            # broadcast to correct shape
            initial_limits = np.ones((n_derivatives, 1)) * initial_limits
            initial_upper_limits = initial_limits[:, 1]
            initial_upper_limits[initial_upper_limits <= 0] = np.inf
            initial_limits[:, 1] = initial_upper_limits
            # adding initial extrapolation_errors as nans
            initial_limits = np.vstack((initial_limits, [np.nan, np.nan]))
            self.static_attributes["notnan_initial_limits"] = ~np.isnan(initial_limits)
            self.static_attributes["initial_limits"] = initial_limits[
                self.static_attributes["notnan_initial_limits"]
            ]
        self.parent = None

        self.sub_trackers = []

    def add_sub_tracker(self, sub_tracker):
        # only mother should account for this
        if self.parent is not None:
            raise ValueError("Sub trackers are only possible in one level")
        sub_tracker.parent = self
        # only one tracker should punish jumped frames
        sub_tracker.static_attributes["jumped_frames_penalty"] = 0
        self.sub_trackers.append(sub_tracker)

    def extract_columns(self, particles):
        columns = [particles[[self.static_attributes["name"]]].to_numpy(), []]
        for sub_tracker in self.sub_trackers:
            columns[1].append(sub_tracker.extract_columns(particles))
        return columns

    def getitem_from_columns(self, columns, indexer):
        extracted_part = [columns[0][indexer], []]
        for sub_tracker, sub_columns in zip(self.sub_trackers, columns[1]):
            extracted_part[1].append(
                sub_tracker.getitem_from_columns(sub_columns, indexer)
            )
        return extracted_part

    def create_new_track(self, particle):
        if isinstance(particle, pd.DataFrame):
            particle = self.extract_columns(particle)
        new_particle = self.copy()
        new_particle.initial = True
        new_value = particle[0]  # particle[self.static_attributes["name"]].to_numpy()
        new_particle.derivatives = new_value

        new_particle.last_n_jumped_frames = 0
        new_particle.last_extrapolation_error = np.nan
        if "initial_derivatives" in self.static_attributes:
            new_particle.derivatives = np.vstack(
                (
                    new_particle.derivatives,
                    self.static_attributes["initial_derivatives"],
                )
            )
        ### new_particle.derivatives[-1] = extrapolation_error
        new_particle.derivatives = np.vstack(
            (new_particle.derivatives, np.zeros(new_value.size))
        )
        new_particle.sub_trackers = [
            sub_tracker.create_new_track(sub_particle)
            for sub_tracker, sub_particle in zip(self.sub_trackers, particle[1])
        ]
        return new_particle

    def calc_new_derivatives(self, particles, n_jumped_frames):
        new_values = particles[0]
        n_rows, n_cols = self.derivatives.shape
        new_n_rows = min(n_rows + 1, self.static_attributes["n_derivatives"] + 2)

        new_derivatives = np.zeros((len(new_values), new_n_rows, n_cols))
        new_derivatives[:, 0] = new_values
        new_derivatives[:, 1] = (new_derivatives[:, 0] - self.derivatives[0]) / (
            n_jumped_frames + 1
        )
        for i in range(2, new_n_rows - 1):
            new_derivatives[:, i] = new_derivatives[:, i - 1] - self.derivatives[i - 1]

        if len(self.derivatives) == self.static_attributes["n_derivatives"] + 2:
            extrapolation_errors = new_values - (
                self.derivatives[0]
                + np.sum(self.derivatives[1:-1], axis=0) * (n_jumped_frames + 1)
            )
            new_derivatives[:, -1] = extrapolation_errors

        new_derivatives_list = [new_derivatives, []]
        for sub_tracker, sub_particles in zip(self.sub_trackers, particles[1]):
            new_derivatives_list[1].append(
                sub_tracker.calc_new_derivatives(sub_particles, n_jumped_frames)
            )

        return new_derivatives_list

    def update(self, new_derivatives_list, n_jumped_frames):
        new_derivatives = new_derivatives_list[0]
        if self.initial and len(new_derivatives) > 3:
            new_derivatives = np.delete(new_derivatives, 2, axis=0)
        self.initial = False
        if len(self.derivatives) == self.static_attributes["n_derivatives"] + 2:
            self.last_extrapolation_error = np.linalg.norm(new_derivatives[-1])
            if self.static_attributes["relative_limits"]:
                self.last_extrapolation_error /= np.linalg.norm(self.derivatives[0])
        self.last_n_jumped_frames = n_jumped_frames
        self.derivatives = new_derivatives
        try:
            del self.compare_key
        except AttributeError:
            pass
        for sub_tracker, sub_tracker_derivatives in zip(
            self.sub_trackers, new_derivatives_list[1]
        ):
            sub_tracker.update(sub_tracker_derivatives, n_jumped_frames)
        return self

    def is_valid_updates(self, new_derivatives_list):
        # new_derivatives_list = self.calc_new_derivatives(particles, n_jumped_frames)
        new_derivatives = new_derivatives_list[0]
        if (
            new_derivatives.shape[-1] == 1
            and self.static_attributes["limits"][-1, 0] < 0
        ):
            new_normed_derivatives = new_derivatives[..., 0]
        else:
            new_normed_derivatives = np.linalg.norm(new_derivatives, axis=-1)

        current_limits = self.static_attributes["limits"].copy()
        if self.initial and "initial_limits" in self.static_attributes is not None:
            current_limits[
                self.static_attributes["notnan_initial_limits"]
            ] = self.static_attributes["initial_limits"]

        if self.static_attributes["relative_limits"]:
            current_limits *= np.linalg.norm(self.derivatives[0])

        is_valid = np.ones(len(new_derivatives), dtype=bool)
        for i in range(1, new_derivatives.shape[1]):
            is_valid = (
                is_valid
                & (current_limits[i - 1, 0] <= new_normed_derivatives[:, i])
                & (new_normed_derivatives[:, i] <= current_limits[i - 1, 1])
            )

        for sub_tracker, sub_new_derivatives in zip(
            self.sub_trackers, new_derivatives_list[1]
        ):
            sub_is_valid = sub_tracker.is_valid_updates(sub_new_derivatives)
            is_valid = is_valid & sub_is_valid
        return is_valid

    def validate_and_update(self, particles, n_jumped_frames):
        new_derivatives_list = self.calc_new_derivatives(particles, n_jumped_frames)
        is_valid = self.is_valid_updates(new_derivatives_list)
        is_valid_indexs = np.flatnonzero(is_valid)
        n_new_tracks = len(is_valid_indexs)

        new_tracks = np.empty(n_new_tracks, dtype=object)
        new_scores = np.empty(n_new_tracks)
        for i, valid_index in enumerate(is_valid_indexs):
            current_derivatives = self.getitem_from_columns(
                new_derivatives_list, valid_index
            )
            new_tracks[i] = self.copy().update(
                current_derivatives,
                n_jumped_frames,
            )
            new_scores[i] = new_tracks[i].get_score()
        return is_valid, new_tracks, new_scores

    def get_score(self):
        score = 0
        if self.static_attributes["value_score"]:
            # higher absolute value gives higher score here
            score += (
                np.linalg.norm(self.derivatives[0])
                / self.static_attributes["extrapolation_error_scaling"]
            )
        elif not np.isnan(self.last_extrapolation_error):
            score += (
                1
                - self.last_extrapolation_error
                / self.static_attributes["extrapolation_error_scaling"]
            )

        score -= (
            self.last_n_jumped_frames * self.static_attributes["jumped_frames_penalty"]
        )
        score += np.sum([sub_tracker.get_score() for sub_tracker in self.sub_trackers])
        return score

    def copy(self):
        the_copy = copy.copy(self)
        the_copy.sub_trackers = [
            sub_tracker.copy() for sub_tracker in self.sub_trackers
        ]
        return the_copy

    def get_compare_key(self):
        # used for sorting, no sub_trackers are used
        try:
            return self.compare_key
        except AttributeError:
            normed_derivatives = np.linalg.norm(self.derivatives, axis=1)[1:]
            # sort priority: score, extrapolation_error acceleration speed etc
            self.compare_key = (self.get_score(),) + tuple(
                iter(normed_derivatives[::-1])
            )
            return self.compare_key

    def __lt__(self, other_track):
        return self.get_compare_key() < other_track.get_compare_key()

    def __str__(self):
        if not hasattr(self, "derivatives"):
            return "Not initiated parameter tracker"
        normed_derivatives = np.linalg.norm(self.derivatives, axis=1)
        if self.static_attributes["name"] == "coordinates":
            if len(normed_derivatives) < 3:
                normed_derivatives = np.concatenate(
                    (normed_derivatives, [np.nan] * (3 - len(normed_derivatives)))
                )
            stringified = (
                '"coordinates": {}, "velocity": {:.3f}, "acceleration": {:.3f}'.format(
                    "[{}]".format(
                        ", ".join(["{:.3f}".format(c) for c in self.derivatives[0]])
                    ),
                    normed_derivatives[1],
                    normed_derivatives[2],
                )
            )
            if self.static_attributes["n_derivatives"] > 2:
                stringified = "{}, {}".format(
                    stringified,
                    ", ".join(
                        [
                            "{}: {:.3f}".format(i, normed_derivatives[i])
                            for i in range(
                                3, self.static_attributes["n_derivatives"] + 1
                            )
                        ]
                    ),
                )
        else:
            stringified = ", ".join(
                [
                    "{}: {:.3f}".format(i, normed_derivatives[i])
                    for i in range(self.static_attributes["n_derivatives"] + 1)
                ]
            )

        sub_trackes_stringified = ", ".join(
            [str(sub_tracker) for sub_tracker in self.sub_trackers]
        )
        stringified = '"tracker/{}": {{"score": {:.3f}, {}, "extrapolation_error": {:.3f}, {}}}'.format(
            self.static_attributes["name"],
            self.get_score(),
            stringified,
            self.last_extrapolation_error,
            sub_trackes_stringified,
        )
        return stringified
