from typing import Tuple, List
import inspect
import functools
import json

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.figure as mfigure
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import matplotlib.cm as mcm
import matplotlib.colors as mcolors
from mpl_toolkits import mplot3d
import matplotlib.widgets as mwidgets

import numpy as np
import pandas as pd

from . import particle_tracking
from . import particle_finding_visualization
from . import particle_3d_matching_visualization
from . import visualise_helpers
from . import helpers


class TrackedParticlesPlotSlider(visualise_helpers.PlotSlider):
    def __init__(
        self,
        tracked_particles,
        threed2twod=False,
        particles_2d=None,
        plot_lims=None,
        axis_equal=False,
        zaxis=None,
        accumulate_tracks=False,
        dt=1,
        cmap=None,
        clim=None,
        cmap_information=None,
        verbose=False,
        tracking_kwargs={},
        **kwargs,
    ):
        ## Setting various variables
        if not hasattr(self, "plotting_variables"):
            self.plotting_variables = {}
        self.plotting_variables["track_id_frame_dict"] = {}
        self.plotting_variables["id_track_indexed_tracked_particles"] = pd.DataFrame()
        self.plotting_variables["track_coloring_values"] = {}
        self.plotting_variables["threed2twod"] = threed2twod
        self.plotting_variables["chosen_dimensions"] = None
        self.plotting_variables["dt"] = dt
        self.plotting_variables["plot_lims"] = plot_lims
        self.plotting_variables["axis_equal"] = axis_equal
        self.plotting_variables["zaxis"] = zaxis
        self.plotting_variables["accumulate_tracks"] = accumulate_tracks
        self.plotting_variables["plot_last_line"] = True
        self.plotting_variables["particle_in_single_frame"] = False
        self.plotting_variables["start_marker"] = "o"
        self.plotting_variables["start_markersize"] = 4
        self.plotting_variables["marker"] = "."
        self.plotting_variables["markersize"] = None
        # if black background, background particles should be visible
        if matplotlib.rcParams["axes.facecolor"] == "white":
            self.plotting_variables["back_track_color"] = mcolors.to_rgba("black")
        else:
            self.plotting_variables["back_track_color"] = mcolors.to_rgba("white")

        self.plotting_variables["cmap"] = cmap
        if clim is None:
            clim = (None, None)
        norm = mcolors.Normalize(vmin=clim[0], vmax=clim[1], clip=True)
        mappable = mcm.ScalarMappable(norm=norm, cmap=cmap)
        self.plotting_variables["mappable"] = mappable

        ndims = np.sum(tracked_particles.columns == "coordinates")
        if ndims == 2 or (ndims == 3 and threed2twod):
            self.plotting_variables["new_line"] = mlines.Line2D
            projection = None
            n_axes = 1
        elif ndims == 3:
            self.plotting_variables["new_line"] = mplot3d.art3d.Line3D
            projection = "3d"
            n_axes = 1
        else:
            raise ValueError(f"Number of dimensions {ndims} not valid")

        if (
            "camera" in tracked_particles
            and len(np.unique(tracked_particles["camera"])) > 1
        ):
            raise ValueError("Plotting of tracks take one camera at a time")

        if particles_2d is not None:
            n_axes += 1
            projection = [projection, None]
            self.plotting_variables[
                "plot_lims_2d"
            ] = visualise_helpers.get_automated_plot_lims(
                particles_2d[particles_2d["camera"] == 0]["coordinates"].to_numpy()
            )
        self.plotting_variables["particles_2d"] = particles_2d

        self.plotting_variables["ndims"] = ndims

        tracking_kwargs2 = dict(
            min_speed=0,
            max_speed=-1,
            max_acceleration=-1,
            max_extrapolation_error=-1,
            max_jumped_frames=0,
            min_track_length=1,
            initial_velocity=None,
            initial_max_acceleration=None,
            track_columns=[],
            coordinate_parameter_tracker=None,
        )
        tracking_kwargs2.update(tracking_kwargs)

        if verbose:
            ## Initialising variables for the verbose
            initial_velocity = tracking_kwargs2["initial_velocity"]
            if initial_velocity is not None and not hasattr(
                initial_velocity, "__call__"
            ):
                tracking_kwargs2["initial_velocity"] = np.asarray(initial_velocity)
        self.picking_variables = dict(
            verbose=verbose,
            tracking_kwargs=tracking_kwargs2,
            circles_kwargs={
                "facecolor": "green",
                "edgecolor": "green",
                "alpha": 0.3,
                "zorder": -2,
            },
            extra_float_columns=[],
        )
        self.picking_variables["min_speed_circles_kwargs"] = self.picking_variables[
            "circles_kwargs"
        ].copy()
        self.picking_variables["min_speed_circles_kwargs"]["facecolor"] = "white"
        self.pick_prefix = "tracking"

        if "coordinate_parameter_tracker" in tracking_kwargs:
            # overriding some parameters
            self.update_coordinate_parameter_tracker(
                tracking_kwargs["coordinate_parameter_tracker"]
            )

        # Initialising PlotSlider
        frames = tracked_particles["frame"]
        if len(frames) > 0:
            frame_lims = (np.min(frames), np.max(frames))
            self.set_framelims(frame_lims)
        super().__init__(n_plot_axs=n_axes, projection=projection, **kwargs)

        self.update_plotting_variables(tracked_particles, cmap_information)

        if self.plotting_variables["plot_lims"] is None:
            raise ValueError("If no tracked particles are given plot_lims must be set")
        self.update_plot_lims()

    def update_coordinate_parameter_tracker(self, coordinate_parameter_tracker):
        if coordinate_parameter_tracker is None:
            return
        # overriding some parameters
        tracking_kwargs = self.picking_variables["tracking_kwargs"]
        if isinstance(coordinate_parameter_tracker, dict):
            if "name" not in coordinate_parameter_tracker:
                coordinate_parameter_tracker["name"] = "coordinates"
            coordinate_parameter_tracker_attributes = (
                particle_tracking.Parameter_derivative_tracking(
                    **coordinate_parameter_tracker
                ).static_attributes
            )
        else:
            coordinate_parameter_tracker_attributes = (
                coordinate_parameter_tracker.static_attributes
            )
        if coordinate_parameter_tracker_attributes["n_derivatives"] > 0:
            tracking_kwargs["min_speed"] = coordinate_parameter_tracker_attributes[
                "limits"
            ][0, 0]
            tracking_kwargs["max_speed"] = coordinate_parameter_tracker_attributes[
                "limits"
            ][0, 1]
        else:
            tracking_kwargs["max_speed"] = np.nan
        if coordinate_parameter_tracker_attributes["n_derivatives"] > 1:
            tracking_kwargs[
                "max_acceleration"
            ] = coordinate_parameter_tracker_attributes["limits"][1][1]
        else:
            tracking_kwargs["max_acceleration"] = np.nan
        tracking_kwargs[
            "max_extrapolation_error"
        ] = coordinate_parameter_tracker_attributes["limits"][-1, 1]

    def update_plot_lims(self):
        ndims = self.plotting_variables["ndims"]
        zaxis = self.plotting_variables["zaxis"]
        axis_equal = self.plotting_variables["axis_equal"]
        plot_lims = self.plotting_variables["plot_lims"]
        chosen_dimensions = self.plotting_variables["chosen_dimensions"]
        ax = self.plot_ax_list[0]
        if ndims == 3 and not self.plotting_variables["threed2twod"]:
            labels = ["X", "Y", "Z"]
            if zaxis is None:
                zaxis = 2
            ax.set_zlabel(labels[zaxis])
            ax.set_ylabel(labels[zaxis - 1])
            ax.set_xlabel(labels[zaxis - 2])

            if axis_equal == "box":
                plot_lengths = np.abs(np.diff(plot_lims, axis=1))
                ax.set_box_aspect(plot_lengths)
            elif axis_equal:
                plot_lims = visualise_helpers.plot_lims_equal(plot_lims)
            ax.set_zlim(plot_lims[zaxis])
            ax.set_ylim(plot_lims[zaxis - 1])
            ax.set_xlim(plot_lims[zaxis - 2])
        else:
            if chosen_dimensions is None:
                chosen_dimensions = [0, 1]
            labels = ["x", "y", "z"]
            ax.set_xlabel(labels[chosen_dimensions[0]])
            ax.set_ylabel(labels[chosen_dimensions[1]])

            ax.set_xlim(plot_lims[chosen_dimensions[0]])
            ax.set_ylim(plot_lims[chosen_dimensions[1]])
            if axis_equal:
                ax.set_aspect("equal")
        if "plot_lims_2d" in self.plotting_variables:
            ax = self.plot_ax_list[1]
            plot_lims_2d = self.plotting_variables["plot_lims_2d"]
            ax.set_xlim(plot_lims_2d[0])
            ax.set_ylim(plot_lims_2d[1])

    def set_plot_lims(self, plot_lims):
        self.plotting_variables["plot_lims"] = plot_lims
        self.update_plot_lims()

    def set_axis_equal(self, axis_equal):
        self.plotting_variables["axis_equal"] = axis_equal
        self.update_plot_lims()

    def threed2twod_particles(self, tracked_particles):
        if self.plotting_variables["threed2twod"]:
            # chosing the two dimensions with most variance
            dimension_variance = np.var(
                tracked_particles["coordinates"].to_numpy(), axis=0
            )
            sorted_dimensions = np.argsort(dimension_variance)
            dimension_to_remove = sorted_dimensions[0]
            helpers.unset_coordinate_columns(tracked_particles)
            tracked_particles.drop(
                columns=["x", "y", "z"][dimension_to_remove], inplace=True
            )
            helpers.set_coordinate_columns(tracked_particles, ignore_order=True)
            chosen_dimensions = sorted_dimensions[
                -2:
            ]  # take the last two in reverse order
            self.plotting_variables["chosen_dimensions"] = np.sort(chosen_dimensions)

        return tracked_particles

    def update_plotting_variables(
        self, tracked_particles, cmap_information=None, update_intensities=True
    ):
        if len(tracked_particles) > 0:
            # copying and better row iteration
            tracked_particles = tracked_particles.astype("object")
            if self.plotting_variables["particles_2d"] is not None:
                tracked_particles = (
                    particle_3d_matching_visualization.add_2d_coordinates_to_3d_matched(
                        tracked_particles,
                        self.plotting_variables["particles_2d"],
                        n_cameras=1,
                    )
                )
            self.threed2twod_particles(tracked_particles)

            tracking_kwargs = self.picking_variables["tracking_kwargs"]

            if self.plotting_variables["zaxis"] is not None:
                tracked_particles[
                    "coordinates"
                ] = visualise_helpers.new_coordinates_zaxis(
                    tracked_particles["coordinates"].to_numpy(),
                    self.plotting_variables["zaxis"],
                )

            unique_track_ids, track_inverse, track_lengths = np.unique(
                tracked_particles["id_track"],
                return_inverse=True,
                return_counts=True,
            )
            if tracking_kwargs["min_track_length"] > 1:
                unique_track_ids[
                    track_lengths < tracking_kwargs["min_track_length"]
                ] = -1
                valid_tracks = unique_track_ids[track_inverse] >= 0
                tracked_particles = tracked_particles[valid_tracks]
                if len(tracked_particles) == 0:
                    return
                track_lengths = track_lengths[unique_track_ids >= 0]
                unique_track_ids = unique_track_ids[unique_track_ids >= 0]

            coordinates = tracked_particles["coordinates"]
            if self.plotting_variables["plot_lims"] is None:
                self.plotting_variables[
                    "plot_lims"
                ] = visualise_helpers.get_automated_plot_lims(coordinates.to_numpy())

            ### Preparing dictionaries for the plotting ###
            track_ids = tracked_particles["id_track"]
            frames = tracked_particles["frame"]

            id_track_indexed_tracked_particles = tracked_particles.copy()
            id_track_indexed_tracked_particles.index = track_ids

            # plotting the track one frame after it is gone
            # (and more if verbose and max_jumped_frames)
            extra_frames = (
                2
                + (self.picking_variables["verbose"] is not False)
                * tracking_kwargs["max_jumped_frames"]
            )
            max_frame = np.max(frames) + extra_frames
            track_id_frame_dict = {  # dict with list of all track ids for each frame
                frame: set() for frame in range(np.min(frames), max_frame)
            }
            for track_id in unique_track_ids:
                current_track = id_track_indexed_tracked_particles.loc[[track_id]]
                current_frames = current_track["frame"]

                if (
                    self.plotting_variables["particle_in_single_frame"]
                    and len(current_track) == 1
                ):
                    current_max_frame = np.max(current_frames) + 1
                else:
                    # If accumulate the track should be in every frame until the end
                    current_max_frame = (
                        np.max(current_frames) + extra_frames
                        if not self.plotting_variables["accumulate_tracks"]
                        else max_frame
                    )
                [
                    track_id_frame_dict[frame].add(track_id)
                    for frame in range(np.min(current_frames), current_max_frame)
                ]

            track_coloring_values = {}
            if (
                self.plotting_variables["cmap"] is not None
                or cmap_information is not None
            ):
                if not isinstance(cmap_information, pd.DataFrame):
                    # If no information the speed is estimated and used
                    (
                        cmap_information1,
                        cmap_information2,
                    ) = particle_tracking.tracked_velocities(
                        tracked_particles,
                        dt=self.plotting_variables["dt"],
                        return_average_particles=True,
                    )
                    # if average speed or instantaneous speed should
                    # be plotted instantaneous
                    if cmap_information:
                        cmap_information = cmap_information1
                        cmap_information["values"] = np.linalg.norm(
                            cmap_information["velocities"], axis=-1
                        )
                    else:
                        # average
                        cmap_information = cmap_information2
                        cmap_information["values"] = cmap_information["speed"]
                track_ids = cmap_information["id_track"]
                values = cmap_information["values"]
                values.index = track_ids

                mappable = self.plotting_variables["mappable"]
                mappable.norm.autoscale_None(values)
                self.mappable = mappable

                for track_id in np.unique(track_ids):
                    track_coloring_values[track_id] = values.loc[track_id]
            self.plotting_variables["track_coloring_values"] = track_coloring_values

            self.plotting_variables["track_id_frame_dict"] = track_id_frame_dict
            self.plotting_variables[
                "id_track_indexed_tracked_particles"
            ] = id_track_indexed_tracked_particles

            if update_intensities:
                unique_frames, intensity = np.unique(frames, return_counts=True)
                unique_frames = unique_frames.astype(int)
                all_frames = np.arange(
                    unique_frames[0], unique_frames[-1] + 1, dtype=int
                )
                all_intensities = np.zeros(len(all_frames))
                all_intensities[unique_frames - unique_frames[0]] = intensity
                self.update_intensities(all_frames, all_intensities)

            self.clear_cache()
            self.set_frame(self.get_frame())

    def make_frame_artists(self, frame):
        track_id_frame_dict = self.plotting_variables["track_id_frame_dict"]
        if frame not in track_id_frame_dict:
            return []
        id_track_indexed_tracked_particles = self.plotting_variables[
            "id_track_indexed_tracked_particles"
        ]
        track_coloring_values = self.plotting_variables["track_coloring_values"]
        extra_axes = len(self.plot_ax_list) > 1
        artists = [[] for i in range(1 + extra_axes)]
        for track_id in reversed(list(track_id_frame_dict[frame])):
            current_full_track = id_track_indexed_tracked_particles.loc[[track_id]]
            current_frames = current_full_track["frame"]

            # are we after the finish of the particle
            is_on = frame <= np.max(current_frames)

            # removing the frames that have yet to occur
            current_track = current_full_track[current_frames <= frame]
            all_current_coordinates = [current_track["coordinates"].to_numpy().T]
            if extra_axes:
                all_current_coordinates.append(
                    current_track["coordinates_2d_C0"].to_numpy().T
                )

            alpha = 1 if is_on or self.plotting_variables["accumulate_tracks"] else 0.5
            if len(current_full_track) == 1:
                color = self.plotting_variables["back_track_color"]
            elif track_id in track_coloring_values:
                color = self.plotting_variables["mappable"].to_rgba(
                    track_coloring_values[track_id]
                )
            else:
                color = visualise_helpers.colorcycle[
                    track_id % len(visualise_helpers.colorcycle)
                ]
                color = mcolors.to_rgba(color)
            color = np.asarray(color)

            label = "{} {}".format(self.pick_prefix, track_id)

            for i, current_coordinates in enumerate(all_current_coordinates):
                if i == 0:
                    new_line = self.plotting_variables["new_line"]
                else:
                    new_line = mlines.Line2D

                if current_coordinates.shape[1] > 1:
                    if not self.plotting_variables["plot_last_line"] and is_on:
                        artists[i].append(
                            new_line(
                                *current_coordinates[:, -1:],
                                marker=".",
                                color=self.plotting_variables["back_track_color"],
                                alpha=alpha,
                                picker=True,
                                label="{} -1".format(label),
                            )
                        )
                        current_coordinates = current_coordinates[:, :-1]

                # Showing the start of the particle clearly
                if len(color.shape) == 1:
                    artists[i].append(
                        new_line(
                            *current_coordinates[:, :1],
                            marker=self.plotting_variables["start_marker"],
                            markersize=self.plotting_variables["start_markersize"],
                            color=color,
                            alpha=alpha,
                        )
                    )
                    artists[i].append(
                        new_line(
                            *current_coordinates,
                            marker=self.plotting_variables["marker"],
                            markersize=self.plotting_variables["markersize"],
                            color=color,
                            alpha=alpha,
                            picker=True,
                            label=label,
                        )
                    )
                elif len(color.shape) == 2:
                    assert (
                        len(color) == len(current_full_track) - 1
                    ), f"wrong number of colors for track id {track_id}, expected \
                            {len(current_full_track) - 1} got {len(color)}"
                    artists[i].append(
                        new_line(
                            *current_coordinates[:, :1],
                            marker=self.plotting_variables["start_marker"],
                            markersize=self.plotting_variables["start_markersize"],
                            color=color[0],
                            alpha=alpha,
                        )
                    )
                    for j in range(current_coordinates.shape[1] - 1):
                        artists[i].append(
                            new_line(
                                *current_coordinates[:, j : j + 2],
                                marker=self.plotting_variables["marker"],
                                markersize=self.plotting_variables["markersize"],
                                # markevery=[1],  # does this change speed somehow?
                                color=color[j],
                                alpha=alpha,
                                picker=True,
                                label=label,
                            )
                        )
        if not extra_axes:
            artists = artists[0]
        return artists

    def on_pick_data(self, event):
        label = event.artist.get_label()
        if not label.startswith(self.pick_prefix):
            # should only process the right lines
            return
        track_id_frame_dict = self.plotting_variables["track_id_frame_dict"]
        id_track_indexed_tracked_particles = self.plotting_variables[
            "id_track_indexed_tracked_particles"
        ]
        dt = self.plotting_variables["dt"]
        verbose = self.picking_variables["verbose"]
        tracking_kwargs = self.picking_variables["tracking_kwargs"]

        extra_axes = len(self.plot_ax_list) > 1
        ax = self.plot_ax_list[0]

        frame = self.get_frame()
        # line = event.artist
        ind = event.ind[-1]
        label_splitted = label.split()
        track_id = int(label_splitted[1])
        if len(label_splitted) > 2:
            ind = int(label_splitted[2])
        if track_id not in track_id_frame_dict[frame]:
            return
        current_track = id_track_indexed_tracked_particles.loc[[track_id]]
        n_inds_from_start = np.sum(current_track["frame"] <= frame) - 1
        if ind < 0:
            ind += n_inds_from_start + 1
        current_particle = current_track.iloc[ind]
        particle_frame = int(current_particle["frame"])
        particle_id = int(current_particle["id"])
        particle_text = "id_track: {}\nid {}\nframe: {}".format(
            track_id, particle_id, particle_frame
        )
        if "track_score" in current_particle:
            particle_text = "{}\ntrack_score: {}".format(
                particle_text,
                "{:.2f}".format(
                    np.linalg.norm(current_particle["track_score"]) / dt**2
                ),
            )
        for column in tracking_kwargs["track_columns"]:
            if not isinstance(column, str):
                column = column["name"]
            v = float(current_particle[column])
            particle_text = f"{particle_text}\n{column}: {v:.4e}"

        # Plotting acceptance circles if verbose
        current_frames = current_track["frame"].to_numpy()
        n_frames_from_start = frame - current_frames[0]
        plot_circles = self.plotting_variables["ndims"] == 2 or (
            self.plotting_variables["ndims"] == 3
            and self.plotting_variables["threed2twod"]
        )
        if verbose and n_frames_from_start >= 0 and ind == n_inds_from_start:
            # are we after the finish of the particle
            is_on = frame <= current_frames[-1]
            # If not on the track is drawn semi transparent
            # alpha = 1 if is_on else 0.5

            # removing the frames that have yet to occur
            current_track = current_track[current_frames <= frame]
            current_frames = current_track[
                "frame"
            ].to_numpy()  # after removing the final ones
            current_coordinates = current_track["coordinates"].to_numpy()

            # check if this frame is in a jump
            not_in_jump = current_frames[-1] == frame

            # last ind is second to last if is_on and this particle is in this frame,
            # otherwise last
            last_ind = len(current_coordinates) - 1 - (is_on and not_in_jump)
            last_coordinates = current_coordinates[last_ind]

            # More plotting and information for each particle
            last_velocity = None
            last_acceleration = None
            if (
                n_frames_from_start == 1
                and tracking_kwargs["initial_velocity"] is not None
            ):
                if hasattr(tracking_kwargs["initial_velocity"], "__call__"):
                    last_velocity = tracking_kwargs["initial_velocity"](
                        *last_coordinates
                    )
                else:
                    last_velocity = tracking_kwargs["initial_velocity"]
                if tracking_kwargs["initial_max_acceleration"] is None:
                    current_max_acceleration = tracking_kwargs["max_acceleration"]
                else:
                    current_max_acceleration = tracking_kwargs[
                        "initial_max_acceleration"
                    ]
            elif n_frames_from_start > 1 and last_ind > 0:
                # n_frames_from_start > 1 means that ???
                # last_ind > 0 means that there is at least one particle before
                # to use for velocity estimation
                last_jumped_frames = (
                    current_frames[last_ind] - current_frames[last_ind - 1]
                )
                last_velocity = (
                    current_coordinates[last_ind - 0]
                    - current_coordinates[last_ind - 1]
                ) / last_jumped_frames
                current_max_acceleration = tracking_kwargs["max_acceleration"]
                if n_frames_from_start > 2 and last_ind > 1:
                    last_last_jumped_frames = (
                        current_frames[last_ind - 1] - current_frames[last_ind - 2]
                    )
                    last_last_velocity = (
                        current_coordinates[last_ind - 1]
                        - current_coordinates[last_ind - 2]
                    ) / last_last_jumped_frames
                    last_acceleration = last_velocity - last_last_velocity

            jumped_frames = max(1, frame - current_frames[last_ind])
            if last_velocity is not None:
                particle_text = "{}\nv {}".format(
                    particle_text, "{:.2e}".format(np.linalg.norm(last_velocity) / dt)
                )
                # Guessed new coordinates for this particle
                guessed_coordinates = last_coordinates + last_velocity * jumped_frames
                ax.plot(
                    *guessed_coordinates,
                    "x",
                    color=visualise_helpers.colorcycle[
                        track_id % len(visualise_helpers.colorcycle)
                    ],
                    # alpha=alpha,
                )

                ## Plotting acceptance circles for acceleration
                if plot_circles and current_max_acceleration > 0:
                    acceleration_circle = mpatches.Circle(
                        guessed_coordinates,
                        current_max_acceleration * jumped_frames,
                        **self.picking_variables["circles_kwargs"],
                    )
                    ax.add_artist(acceleration_circle)

                if not np.isnan(current_max_acceleration):
                    # if above is nan then acceleration is assumed to not be used
                    if last_acceleration is not None:
                        particle_text = "{}\na {}".format(
                            particle_text,
                            "{:.2e}".format(
                                np.linalg.norm(last_acceleration) / dt**2
                            ),
                        )
                        guessed_coordinates2 = (
                            guessed_coordinates + last_acceleration * jumped_frames
                        )
                        ax.plot(
                            *guessed_coordinates2,
                            "*",
                            color=visualise_helpers.colorcycle[
                                track_id % len(visualise_helpers.colorcycle)
                            ],
                            # alpha=alpha,
                        )
                    else:
                        guessed_coordinates2 = None

                else:
                    guessed_coordinates2 = guessed_coordinates

                if guessed_coordinates2 is not None:
                    ## Plotting acceptance circles for extrapolation error
                    if plot_circles and tracking_kwargs["max_extrapolation_error"] > 0:
                        extrapolation_error_circle = mpatches.Circle(
                            guessed_coordinates2,
                            tracking_kwargs["max_extrapolation_error"] * jumped_frames,
                            **self.picking_variables["circles_kwargs"],
                        )
                        ax.add_artist(extrapolation_error_circle)

                    extrapolation_error = last_coordinates - guessed_coordinates2
                    particle_text = "{}\nerr {}".format(
                        particle_text,
                        "{:.2e}".format(np.linalg.norm(extrapolation_error)),
                    )

            ## Plotting acceptance circles for speed
            if plot_circles and tracking_kwargs["max_speed"] > 0:
                max_speed_circle = mpatches.Circle(
                    last_coordinates,
                    tracking_kwargs["max_speed"] * jumped_frames,
                    **self.picking_variables["circles_kwargs"],
                )
                ax.add_artist(max_speed_circle)
            if (
                plot_circles and tracking_kwargs["min_speed"] > 0
            ):  # put this one last to get it on top of the others
                min_speed_circle = mpatches.Circle(
                    last_coordinates,
                    tracking_kwargs["min_speed"] * jumped_frames,
                    **self.picking_variables["min_speed_circles_kwargs"],
                )
                ax.add_artist(min_speed_circle)

        if verbose > 1:
            self.information_text(particle_text)
            ax.plot(
                *current_particle["coordinates"],
                "+",
                markersize=8,
                alpha=0.7,
                color="black",
            )
            if extra_axes:
                self.plot_ax_list[1].plot(
                    *current_particle["coordinates_2d_C0"],
                    "+",
                    markersize=8,
                    alpha=0.7,
                    color=self.plotting_variables["back_track_color"],
                )
        return track_id


def visualise_tracked_particles(
    tracked_particles: pd.DataFrame,
    threed2twod: bool = False,
    particles_2d: pd.DataFrame = None,
    plot_lims: List[Tuple[float, float]] = None,
    axis_equal: bool = False,
    zaxis: int = None,
    accumulate_tracks: bool = False,
    dt: float = 1,  # only to get the correct speeds
    cmap: str or mcolors.Colormap = None,
    clim: Tuple[float, float] = None,
    cmap_information: str or pd.DataFrame = None,
    verbose: bool or int = False,
    # remaining arguments are the same as given to recursive_4frame_best_estimate function
    min_speed: float = np.nan,
    max_speed: float = np.nan,
    max_acceleration: float = -1,
    max_extrapolation_error: float = -1,
    extrapolation_error_scaling: float = None,
    max_jumped_frames: int = 0,
    jumped_frames_penalty: float = None,
    min_track_length: int = 1,
    initial_velocity=None,
    initial_max_acceleration=None,
    coordinate_parameter_tracker=None,
    track_columns=[],
    FigureClass=TrackedParticlesPlotSlider,
    **kwargs,
):
    """
    Visualise the tracks with a interactive matplotlib figure where a slider gives you the power to move in time through your data.

    :param tracked_particles: the tracked_particles
    :param threed2twod: can be used to reduce the visualisation to two dimensions of 3D data. Then the verbose = 2 can show circles of the limiting derivatives in the tracking
    :param particles_2d: Same as output from ptv3py.find_particles. If given, a 2D plot of the first camera will also be shown to see how the tracking looks like in the camera.
    :param plot_lims: 2D or 3D limits for the plotting. If not provided, the limits are automatically calculated from the triangulated positions.
    :param axis_equal: Should the plotting axes have equal sizes for all axes.
    :param zaxis: For the 3D, which of the axes should be the z dimension in the data. Default is the last dimension.
    :param accumulate_tracks: when frames are increased should found tracks that ends still be plotted
    :param dt: the time difference between two frames, used to estimate velocity of particles
    :param cmap: colormap used for plotting the speed of the tracks, if this is given, each track will receive a color respective to its average velocity
    :param clim: limits for the colorbar in plotting speed information
    :param cmap_information: This can be specific information for each particle to use for plotting colors such as a particle size. If it is a boolean with value True, the instantaneous speeds will be plotted instead of the average.
    :param verbose: defines how much information should the interactive viewer show when the user clicks on a particle track or particle position. Maximum is verbose = 2. If 3D data some verbose options are not shown, you can send in 3D data with ndims=2
    ... remaining named arguments are explained in the particle_tracking.recursive_4frame_best_estimate function documentation.
    :param **kwargs: remaining arguments are used in the creation of a matplotlib figure and the visualise_helpers.PlotSlider class
    :returns: subclass of a matplotlib figure called a TrackedParticlesPlotSlider
    """
    if len(tracked_particles) == 0:
        return None

    plotslider_kwargs = dict(
        tracked_particles=tracked_particles,
        threed2twod=threed2twod,
        particles_2d=particles_2d,
        plot_lims=plot_lims,
        axis_equal=axis_equal,
        zaxis=zaxis,
        accumulate_tracks=accumulate_tracks,
        dt=dt,
        cmap=cmap,
        clim=clim,
        cmap_information=cmap_information,
        verbose=verbose,
        tracking_kwargs=dict(
            min_speed=min_speed,
            max_speed=max_speed,
            max_acceleration=max_acceleration,
            max_extrapolation_error=max_extrapolation_error,
            max_jumped_frames=max_jumped_frames,
            min_track_length=min_track_length,
            initial_velocity=initial_velocity,
            initial_max_acceleration=initial_max_acceleration,
            track_columns=track_columns,
            coordinate_parameter_tracker=coordinate_parameter_tracker,
        ),
        **kwargs,
    )
    ps = plt.figure(
        FigureClass=FigureClass,
        **plotslider_kwargs,
    )
    return ps


########################## Track parameter finding helper ##########################


def particle_tracking_parameters_helper(
    particles, particles_2d=None, images=None, **kwargs
):
    """
    Creates one figure showing the images and
    one to tweak the parameters for tracking particles
    """
    image_mappable = mcm.ScalarMappable()
    ndims = np.sum(particles.columns == "coordinates")
    if (
        ndims == 2
        and images is not None
        or images is not None
        and particles_2d is not None
    ):

        def post_plotting(ps):
            frame = ps.get_frame()
            ps.plot_ax_list[-1].imshow(
                images[frame][0],
                cmap=image_mappable.get_cmap(),
                norm=image_mappable.norm,
                zorder=-3,
            )

    else:
        post_plotting = None

    kwargs["verbose"] = 2
    ps = plt.figure(
        FigureClass=FBTrackedParticlesPlotSlider,
        particles=particles.copy(),
        particles_2d=particles_2d,
        post_plotting=post_plotting,
        **kwargs,
    )
    ps.plotting_variables["image_mappable"] = image_mappable

    tptf = plt.figure(
        FigureClass=Tracking_particles_tweak_figure,
        plotslider=ps,
        particles=particles,
    )

    return tptf, ps


class FBTrackedParticlesPlotSlider(TrackedParticlesPlotSlider):
    def __init__(self, particles, **kwargs):
        ndims = np.sum(particles.columns == "coordinates")
        frames_column = particles["frame"]
        unique_frames, intensity = np.unique(
            frames_column,
            return_counts=True,
        )
        self.set_framelims((unique_frames[0], unique_frames[-1]))

        if "plot_lims" not in kwargs:
            kwargs["plot_lims"] = visualise_helpers.get_automated_plot_lims(
                particles["coordinates"]
            )
        empty_particles = pd.DataFrame(columns=["frame"] + ["coordinates"] * ndims)
        super().__init__(empty_particles, **kwargs)

        all_frames = np.arange(unique_frames[0], unique_frames[-1] + 1, dtype=int)
        all_intensities = np.zeros(len(all_frames))
        all_intensities[unique_frames.astype(int) - all_frames[0]] = intensity
        self.update_intensities(all_frames, all_intensities)

        # setting background tracks
        background_track_ids = particles["id"]
        particles.insert(0, "id_track", background_track_ids)
        if self.plotting_variables["particles_2d"] is not None:
            particles = (
                particle_3d_matching_visualization.add_2d_coordinates_to_3d_matched(
                    particles, self.plotting_variables["particles_2d"], n_cameras=1
                )
            )
        particles = self.threed2twod_particles(particles)
        frames = particles["frame"]
        max_frame = np.max(frames) + 3  # extra_frames
        background_track_id_frame_dict = {
            frame: set() for frame in range(np.min(frames), max_frame)
        }
        for track_id, frame in zip(background_track_ids, frames):
            background_track_id_frame_dict[frame].add(track_id)
        particles.index = background_track_ids
        self.plotting_variables[
            "background_track_id_frame_dict"
        ] = background_track_id_frame_dict
        self.plotting_variables[
            "background_id_track_indexed_tracked_particles"
        ] = particles

        self.set_frame(self.get_frame())

    def save_foreground_data(self):
        self.plotting_variables[
            "foreground_track_id_frame_dict"
        ] = self.plotting_variables["track_id_frame_dict"]
        self.plotting_variables[
            "foreground_id_track_indexed_tracked_particles"
        ] = self.plotting_variables["id_track_indexed_tracked_particles"]

    def update_plotting_variables(self, tracked_particles, cmap_information=None):
        super().update_plotting_variables(
            tracked_particles, cmap_information, update_intensities=False
        )
        if len(self.plotting_variables["id_track_indexed_tracked_particles"]) > 0:
            self.plotting_variables["tracked_ids"] = set(
                self.plotting_variables["id_track_indexed_tracked_particles"]["id"]
            )
        else:
            self.plotting_variables["tracked_ids"] = set()

    def set_foreground_particles(self):
        # from background to foreground
        self.pick_prefix = "tracking"
        self.plotting_variables["track_id_frame_dict"] = self.plotting_variables[
            "foreground_track_id_frame_dict"
        ]
        self.plotting_variables[
            "id_track_indexed_tracked_particles"
        ] = self.plotting_variables["foreground_id_track_indexed_tracked_particles"]

    def set_background_particles(self):
        # from foreground to background
        self.save_foreground_data()
        self.pick_prefix = "background_tracking"
        self.plotting_variables["track_id_frame_dict"] = self.plotting_variables[
            "background_track_id_frame_dict"
        ]
        self.plotting_variables[
            "id_track_indexed_tracked_particles"
        ] = self.plotting_variables["background_id_track_indexed_tracked_particles"]

    def make_frame_artists(self, frame):
        tracking_artists = super().make_frame_artists(frame)
        self.set_background_particles()
        background_artists = super().make_frame_artists(frame)
        self.set_foreground_particles()
        if len(tracking_artists) > 0 and isinstance(tracking_artists[0], list):
            all_artists = [
                ba + ta for ba, ta in zip(background_artists, tracking_artists)
            ]
        else:
            all_artists = background_artists + tracking_artists
        return all_artists

    def on_pick_data(self, event):
        label = event.artist.get_label()
        if label.startswith("background"):
            label_splitted = label.split()
            particle_id = int(label_splitted[1])
            if particle_id not in self.plotting_variables["tracked_ids"]:
                self.set_background_particles()
                super().on_pick_data(event)
                self.set_foreground_particles()
        else:
            super().on_pick_data(event)


class Tracking_particles_tweak_figure(mfigure.Figure):
    def __init__(self, plotslider, particles, **kwargs):
        """
        track_particles_function is assumed to take
        image(s) as first argument and then parameters
        """
        self.track_particles_function = particle_tracking.recursive_4frame_best_estimate
        function_signature = inspect.signature(self.track_particles_function)
        self.function_parameters = {}
        self.other_function_parameters = {}
        self.possible_other_parameters = set()
        self.first_tracking = True

        for name in function_signature.parameters:
            param = function_signature.parameters[name]
            if param.annotation not in [callable] and not name == "particles":
                annotation = param.annotation
                if name == "add_track_score":
                    default = True
                else:
                    default = param.default if param.default is not param.empty else ""
                self.function_parameters[name] = dict(
                    annotation=annotation,
                    default=default,
                    empty=param.empty,
                    possible_values=None,
                )
            else:
                self.possible_other_parameters.add(name)

        figsize = (7, (len(self.function_parameters) + 2) / 2)
        kwargs["figsize"] = figsize
        super().__init__(**kwargs)

        self.plotslider = plotslider
        self.particles = particles

        self.create_input_axes()

    def create_input_axes(self):
        # ndims = np.sum(self.particles.columns == "coordinates")
        n_dims_sliders = 0
        # if ndims == 3:
        #     n_dims_sliders = 3

        gridspec_kwargs = {
            "left": 0.05,
            "right": 0.99,
            "hspace": 0.1,
            "wspace": None,
        }
        gridspec = self.add_gridspec(
            1 + n_dims_sliders + len(self.function_parameters) + 1,
            4,
            width_ratios=[0.1, 0.3, 0.4, 0.2],
            **gridspec_kwargs,
        )

        framelims_axes = self.add_subplot(gridspec[0, 1:-1])
        orientation = "horizontal"
        # First is just simple rangeslider for changing the color limits of the cmap
        framelims_slider = mwidgets.RangeSlider(
            framelims_axes,
            "frame_lims",
            *self.plotslider.get_framelims(),
            valinit=self.plotslider.get_framelims(),
            valfmt="%d",
            valstep=1,
            orientation=orientation,
        )
        self.framelims_slider = framelims_slider

        # if ndims == 3:
        #     dim_sliders = []

        #     def dim_slider_update(event, dim):
        #         current_centers = [dim_slider.val for dim_slider in dim_sliders]
        #         plot_ax = self.plotslider.plot_ax
        #         current_plot_lims = [
        #             plot_ax.get_xlim(),
        #             plot_ax.get_ylim(),
        #             plot_ax.get_zlim(),
        #         ]
        #         new_plot_lims = visualise_helpers.plot_lims_symmetrical(
        #             current_plot_lims, current_centers
        #         )
        #         plot_ax.set_xlim(new_plot_lims[0])
        #         plot_ax.set_ylim(new_plot_lims[1])
        #         plot_ax.set_zlim(new_plot_lims[2])

        #     # adding posibility to slide on the axes
        #     plot_lims = visualise_helpers.get_automated_plot_lims(self.particles)
        #     mid_lims = np.mean(plot_lims, axis=1)
        #     dim_names = ["x", "y", "z"]
        #     for i in range(3):
        #         dim_slider_ax = self.add_subplot(gridspec[1 + i, 1:-1])
        #         dim_slider = mwidgets.Slider(
        #             dim_slider_ax,
        #             dim_names[i],
        #             plot_lims[i, 0],
        #             plot_lims[i, 1],
        #             mid_lims[i],
        #         )
        #         dim_slider.on_changed(functools.partial(dim_slider_update, dim=i))
        #         dim_sliders.append(dim_slider)

        def on_parameter_submit(text, name, annotation, widget):
            value, error = particle_finding_visualization.read_single_parameter(
                text, annotation
            )
            if error is not None:
                print("Invalid {} parameter: {}".format(name, error))
                widget.color = "lightcoral"
                widget.hovercolor = "indianred"
            else:
                widget.color = "1"
                widget.hovercolor = "0.95"
                tracking_kwargs = self.plotslider.picking_variables["tracking_kwargs"]
                if name in tracking_kwargs:
                    tracking_kwargs[name] = value

        def on_parameter_update(event):
            print("Tracking")
            self.track_particles()
            print("Done")

        tracking_kwargs = self.plotslider.picking_variables["tracking_kwargs"]
        # Then inputs for all parameters are created
        for i, [name, param] in enumerate(self.function_parameters.items()):
            parameter_ax = self.add_subplot(gridspec[n_dims_sliders + i + 1, 2:])
            annotation = " [{}]".format(param["annotation"].__name__)
            if name in tracking_kwargs:
                start_value = tracking_kwargs[name]
            elif name == "verbose":
                start_value = True
            else:
                start_value = param["default"]
            param["widget"] = mwidgets.TextBox(
                parameter_ax,
                "{}{}".format(name, annotation),
                str(start_value),
            )
            param["widget"].on_submit(
                functools.partial(
                    on_parameter_submit,
                    name=name,
                    annotation=annotation,
                    widget=param["widget"],
                )
            )
        button_ax = self.add_subplot(gridspec[-1, -1])
        self.update_button = mwidgets.Button(button_ax, "Update")
        self.update_button.on_clicked(on_parameter_update)

    def get_framelims(self):
        return self.framelims_slider.val

    def set_framelims(self, framelims):
        self.framelims_slider.set_val(framelims)

    def set_parameters(self, **parameter_values):
        tracking_kwargs = self.plotslider.picking_variables["tracking_kwargs"]
        for name, val in parameter_values.items():
            if val is None:
                str_val = str(val)
            else:
                str_val = json.dumps(val)
            valid = True
            if name in self.function_parameters:
                self.function_parameters[name]["widget"].set_val(str_val)
            elif name in self.possible_other_parameters:
                self.other_function_parameters[name] = val
            else:
                valid = False
            if name in tracking_kwargs:
                tracking_kwargs[name] = val
                if name == "coordinate_parameter_tracker":
                    self.plotslider.update_coordinate_parameter_tracker(val)
                valid = True

            if not valid:
                raise ValueError(
                    "Parameter {} is not valid for tracking function".format(name)
                )

    def debug_particle_id(
        self,
        particles,
        particle_id,
        current_parameters,
    ):
        particles = particles.sort_values("frame", ignore_index=True)
        ids = particles["id"].to_numpy()
        particles.index = ids

        frames_column_series = particles["frame"]
        frames_column = frames_column_series.to_numpy()
        frames, indicies = np.unique(frames_column, return_index=True)
        # coordinates = particles["coordinates"].to_numpy()
        # ndims = coordinates.shape[1]
        # Dict to easier find the particles in each frame,
        splitted_ids = np.array_split(ids, indicies[1:])
        splitted_particles = np.array_split(particles, indicies[1:])

        parameter_tracks_base = particle_tracking.create_parameter_tracks(
            coordinate_parameter_tracker=current_parameters[
                "coordinate_parameter_tracker"
            ],
            particles=particles,
            min_speed=current_parameters["min_speed"],
            max_speed=current_parameters["max_speed"],
            max_acceleration=current_parameters["max_acceleration"],
            max_extrapolation_error=current_parameters["max_extrapolation_error"],
            initial_velocity=current_parameters["initial_velocity"],
            initial_max_acceleration=current_parameters["initial_max_acceleration"],
            extrapolation_error_scaling=current_parameters[
                "extrapolation_error_scaling"
            ],
            jumped_frames_penalty=current_parameters["jumped_frames_penalty"],
            track_columns=current_parameters["track_columns"],
        )

        particle_frames_dict = {
            frame: [
                current_ids,
                parameter_tracks_base.extract_columns(current_particles),
            ]
            for frame, current_ids, current_particles in zip(
                frames, splitted_ids, splitted_particles
            )
        }

        current_particle = particles.loc[[particle_id]]
        frame = current_particle.loc[particle_id, "frame"]

        print("\n***** New track, id: {}, frame: {} *****".format(particle_id, frame))

        parameter_tracks = parameter_tracks_base.create_new_track(current_particle)
        state = [
            particle_id,
            frame,
            1,
            current_parameters["max_jumped_frames"],
            parameter_tracks,
        ]

        recursion_results = particle_tracking.recursive_graph_tracking(
            [particle_id],
            state,
            0,  # score of this particle
            dict(),
            verbose=True,
            recursive_max_branches=current_parameters["recursive_max_branches"],
            initial_recursive_max_branches=current_parameters[
                "initial_recursive_max_branches"
            ],
            find_possible_next_states=particle_tracking.find_possible_next_particles,
            particle_frames_dict=particle_frames_dict,
        )
        tracks_list = []
        max_id = np.max(particles["id"])
        queue = [[[], 0, recursion_results, True]]
        next_track_id = 0
        while queue:
            back_track, back_score, crr, add = queue.pop()
            if add:
                current_track_ids = back_track + list(crr["track"])
                current_track = particles.loc[current_track_ids]
                current_track.insert(0, "id_track", next_track_id)
                current_track["track_score"] = crr["score"] + back_score
                # current_track.loc[back_track, "id"] =
                # np.arange(len(back_track)) + max_id + 1
                tracks_list.append(current_track)
                next_track_id += 1
                max_id += len(back_track)
            if len(crr["children"]) == 0:
                continue
            scores = np.array([c["score"] for c in crr["children"]])
            score_argmax = np.argmax(scores)
            new_back_track = back_track + [crr["track"].popleft()]
            new_back_score = back_score + crr["score"] - scores[score_argmax]
            for i, c in enumerate(crr["children"]):
                queue.append([new_back_track, new_back_score, c, i != score_argmax])
        tracks = pd.concat(tracks_list, ignore_index=True)
        return tracks

    def track_particles(self):
        current_parameters = {}
        current_parameters.update(self.other_function_parameters)
        (
            valid_input,
            current_parameters,
        ) = particle_finding_visualization.read_parameters(
            current_parameters, self.function_parameters
        )

        if not valid_input:
            self.canvas.draw_idle()
            return

        framelims = self.get_framelims()
        frames = self.particles["frame"]
        indexs = np.flatnonzero((framelims[0] <= frames) & (frames <= framelims[1]))
        current_particles = self.particles.iloc[indexs]

        if current_parameters["verbose"] < 0:
            current_tracks = self.debug_particle_id(
                current_particles,
                -int(current_parameters["verbose"]),
                current_parameters,
            )
        else:
            current_tracks = self.track_particles_function(
                current_particles,
                **current_parameters,
            )
        # remove tracks shorter than 2 length
        temp_min_track_length = self.plotslider.picking_variables["tracking_kwargs"][
            "min_track_length"
        ]
        self.plotslider.picking_variables["tracking_kwargs"]["min_track_length"] = 2
        self.plotslider.update_plotting_variables(current_tracks)
        if self.first_tracking:
            self.plotslider.update_plot_lims()
            self.first_tracking = False
        self.plotslider.picking_variables["tracking_kwargs"][
            "min_track_length"
        ] = temp_min_track_length
