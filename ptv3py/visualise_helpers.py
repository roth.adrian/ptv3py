import matplotlib.pyplot as plt
import matplotlib.figure as mfigure

import matplotlib.lines as mlines
import matplotlib.artist as martist
import matplotlib.widgets as mwidgets
from mpl_toolkits import mplot3d

from abc import ABCMeta, abstractmethod


import numpy as np

from . import helpers


colorcycle = plt.rcParams["axes.prop_cycle"].by_key()["color"]


class PlotSlider(mfigure.Figure, metaclass=ABCMeta):
    """
    Helper class to show a frame slider for the data
    Open with plotslider function above
    Class is meant to be subclassed where make_frame_artists and on_pick_data is overridden
    """

    def __init__(
        self,
        n_plot_axs: int = 1,
        gridspec_kwargs: dict = None,
        with_intensity: bool = True,
        projection: str or list = None,
        max_cached_frames: int = 100,
        plot_only: bool = False,
        post_plotting: callable = None,
        **figure_kwargs,
    ):
        """
        :param n_plot_axs: number of axes that should be created
        :param gridspec_kwargs: Arguments for creating the gridspec for the figure
        :param with_intensity: Should the number of particles or similar for each frame be plotted in the slider axes
        :param projection: should the projection be normal, 3D or why not polar
        :param max_cached_frames: how many frames should be cached at maximum
        :param plot_only: if true do not show the slider axes for changing frames
        :param post_plotting: a function that is called after the plotting of each frame. Can be used to add images or other advanced stuff
        :param **figure_kwargs: arguments for the matplotlib figure such as figsize
        """
        # Creating the figure (which is itself)
        super().__init__(**figure_kwargs)
        self.fig = self  # for backwards compatibility

        # Setting attributes
        self.text_artist = None
        if not hasattr(self, "pick_prefix"):
            self.pick_prefix = ""

        if gridspec_kwargs is None:
            gridspec_kwargs = {"hspace": 0.15, "wspace": 0.1, "height_ratios": [1]}
            if n_plot_axs == 2:
                # this small distance works since y labels are moved to right for second plot
                gridspec_kwargs["wspace"] = 0.01

        if not plot_only:
            if "height_ratios" not in gridspec_kwargs:
                gridspec_kwargs["height_ratios"] = [1]
            if with_intensity:
                gridspec_kwargs["height_ratios"].append(0.2)
            else:
                gridspec_kwargs["height_ratios"].append(0.05)

        if not isinstance(projection, list):
            projection = [projection] * n_plot_axs

        gridspec = self.add_gridspec(1 + (not plot_only), n_plot_axs, **gridspec_kwargs)

        self.plot_ax = []
        for i in range(n_plot_axs):
            self.plot_ax.append(
                self.add_subplot(gridspec[0, i], projection=projection[i])
            )
        self.plot_ax_list = self.plot_ax
        if n_plot_axs == 2:
            # Setting labels on the right side of the plot instead
            self.plot_ax_list[1].yaxis.tick_right()
            self.plot_ax_list[1].yaxis.set_label_position("right")

        if n_plot_axs == 1:
            self.plot_ax = self.plot_ax[0]
            # self.plot_ax_list = [self.plot_ax]

        if not hasattr(self, "valmin"):
            self.valmin = 0
        if not hasattr(self, "valmax"):
            self.valmax = self.valmin + 1
        start_frame = self.valmin
        # Frame slider
        if plot_only:
            # no visual slider but a background one instead
            self.frame_slider = BaseSliderWidget(
                self.valmin,
                self.valmax,
                start_frame,
            )
            self.frame_slider.on_changed(self.set_frame)
        else:
            if self.valmin == self.valmax:
                # to avoid zero length slider
                self.valmax += 1
            axcolor = "lightgoldenrodyellow"
            self.frame_slider_ax = self.add_subplot(
                gridspec[1, :], facecolor=axcolor, label="slider"
            )
            self.frame_slider = SliderLegacy(
                ax=self.frame_slider_ax,
                label="Frame",
                valmin=self.valmin,
                valmax=self.valmax,
                valinit=start_frame,
                closedmin=True,
                closedmax=True,
                valfmt="%d",
                valstep=1,
                track_color=axcolor,
                facecolor="lightgray",
            )
            self.frame_slider.on_changed(self.set_frame)

        self.canvas.mpl_connect("key_press_event", self.on_key_press)
        self.canvas.mpl_connect("scroll_event", self.on_scroll)

        self.canvas.mpl_connect("pick_event", self.pick_data)

        self.lru_cache = None
        if max_cached_frames > 0:
            self.lru_cache = helpers.LRU(max_cached_frames)

        [plot_ax.set_autoscale_on(False) for plot_ax in self.plot_ax_list]

        self.post_plotting_func = post_plotting

    def update_intensities(self, frames, intensity):
        plot_only = not hasattr(self, "frame_slider_ax")
        if intensity is not None and not plot_only:
            if len(intensity.shape) == 1:
                intensity = np.expand_dims(intensity, 1)
            if len(self.frame_slider_ax.lines) == 0:
                self.frame_slider_ax.plot(frames, intensity)
            else:
                for i in range(intensity.shape[1]):
                    self.frame_slider_ax.lines[i].set_ydata(intensity[:, i])

            self.canvas.draw_idle()

    @abstractmethod
    def make_frame_artists(self, frame):
        """
        Abstract function that is called for each frame. It should return a list of artists to add to the axes
        """
        ...

    @abstractmethod
    def on_pick_data(self, event):
        """
        Abstract function that is called when the used clicks on a plot. Then extra information can be added such as information text or circles in the tracking that show which particles can be tracked.
        """
        ...

    def post_plotting(self):
        if self.post_plotting_func is not None:
            self.post_plotting_func(self)

    def on_key_press(self, event):
        """Use shift and left/right arrows to go to previous and forward frames respectively"""
        key = event.key
        new_frame = None
        if key == "shift+left":
            new_frame = self.get_frame() - 1
        elif key == "shift+right":
            new_frame = self.get_frame() + 1
        if new_frame is not None:
            self.set_frame(max(0, new_frame))

    def on_scroll(self, event):
        """Scroll to change frame"""
        new_frame = self.get_frame() - event.step
        # max is required unless scrolling backward should start over from zero
        self.set_frame(max(0, new_frame))

    def get_plot_artists(self, frame):
        if self.lru_cache is not None and frame in self.lru_cache:
            artists = self.lru_cache[frame]
        else:
            artists = self.make_frame_artists(frame)
            if not isinstance(self.plot_ax, list):
                artists = [artists]
            if self.lru_cache is not None:
                self.lru_cache[frame] = artists
        return artists

    def get_framelims(self):
        return (self.valmin, self.valmax)

    def set_framelims(self, lims):
        lims = np.asarray(lims)
        lims[0] = max(0, lims[0])
        lims[1] = max(lims[1], lims[0] + 1)
        self.valmin = lims[0]
        self.valmax = lims[1]
        if hasattr(self, "frame_slider"):
            self.frame_slider.valmin = lims[0]
            self.frame_slider.valmax = lims[1]
            if hasattr(self, "frame_slider_ax"):
                self.frame_slider_ax.set_xlim(lims)

    def get_frame(self):
        return int(self.frame_slider.val)

    def set_frame(self, frame):
        slider = self.frame_slider
        if frame < 0:
            # negative frames count from the end
            frame = self.valmax + frame + 1
        if not slider.val == frame:
            # if this function is run directly the value of the slider is also updated
            slider.set_val(frame)
            return
        if frame < self.valmin:
            slider.set_val(self.valmin)
            return
        elif frame > self.valmax:
            slider.set_val(self.valmax)
            return

        frame = int(frame)
        self.clear_axes()
        all_artists = self.get_plot_artists(frame)
        for plot_ax, artists in zip(self.plot_ax_list, all_artists):
            for artist in artists:
                if isinstance(artist, martist.Artist):
                    plot_ax.add_artist(artist)
                else:
                    # Not an artist but should be a function that takes the axes as first argument
                    artist(plot_ax)
        self.post_plotting()
        self.canvas.draw_idle()

    def information_text(self, text):
        if self.text_artist is not None:
            self.text_artist.remove()
        self.text_artist = self.text(
            0.02, 0.98, text, va="top", bbox={"facecolor": "white", "alpha": 0.4}
        )

    def pick_data(self, event=None, track_id=None, ind=-1):
        if event is None:
            assert track_id is not None, "either event or track_id must be given"
            event = Custom_pick_event(track_id, ind, self.pick_prefix)
        self.on_pick_data(event)
        self.canvas.draw_idle()

    def clear_lines(self):
        self.clear_axes(is_matplotlib_line)

    def clear_axes(self, filter_func=lambda a: True):
        artists = []
        for ax in self.plot_ax_list:
            artists.extend(ax.lines)
            artists.extend(ax.patches)
            artists.extend(ax.artists)
            artists.extend(ax.texts)
            artists.extend(ax.images)
            artists.extend(ax.collections)

        artists = filter(filter_func, artists)
        for artist in artists:
            artist.remove()

        if self.text_artist is not None:
            self.text_artist.remove()
            self.text_artist = None

    def colorbar(self, mappable=None, ax=None, **kwargs):
        if hasattr(self, "mappable") and mappable is None:
            if self.mappable is None:
                raise ValueError(
                    "If no mappable in the plotslider then it must be provided as parameter"
                )
            mappable = self.mappable
        if ax is None:
            ax = self.plot_ax_list[-1]
        colorbar = super().colorbar(mappable, ax=ax, **kwargs)
        return colorbar

    def clear_cache(self):
        if hasattr(self, "lru_cache") and self.lru_cache is not None:
            self.lru_cache.clear()


class Custom_pick_event:
    """For creating custom events to the PlotSlider picking"""

    def __init__(self, track_id, picked_ind=-1, prefix=""):
        self.artist = mlines.Line2D([], [], label="{} {}".format(prefix, int(track_id)))
        self.ind = [picked_ind]


def is_matplotlib_line(artist):
    return isinstance(artist, (mlines.Line2D, mplot3d.art3d.Line3D))


class BaseSliderWidget:
    """
    To be used when the slider should not be visible in the figure
    """

    def __init__(self, valmin, valmax, valinit, *args, **kwargs):
        self.val = valinit
        self.valmin = valmin
        self.valmax = valmax
        self.on_changed_function = None

    def set_val(self, val):
        self.val = val
        if self.on_changed_function is not None:
            self.on_changed_function(val)

    def on_changed(self, function):
        self.on_changed_function = function


class SliderLegacy(mwidgets.Slider):
    """
    Recent update of matplotlib makes the slider only take up 50 percent of the ax :/
    fixes below
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._handle.remove()

        self.track.set_xy((0, 0))
        self.track.set_width(1)
        self.track.set_height(1)
        self.track.set(edgecolor="0.75")

        xy = self.poly.xy
        val = self.val
        if self.orientation == "vertical":
            xy[0] = 0, 0
            xy[1] = 0, val
            xy[2] = 1, val
            xy[3] = 1, 0
            xy[4] = 0, 0
        else:
            xy[0] = 0, 0
            xy[1] = 0, 1
            xy[2] = val, 1
            xy[3] = val, 0
            xy[4] = 0, 0
        self.poly.xy = xy

        self.vline.remove()

    def set_val(self, val):
        """
        Override the hardcoded 0.5 fraction height/width of slider
        Set slider value to *val*.
        Parameters
        ----------
        val : float
        """
        xy = self.poly.xy
        if self.orientation == "vertical":
            xy[1] = 0, val
            xy[2] = 1, val
        else:
            xy[2] = val, 1
            xy[3] = val, 0
        self.poly.xy = xy
        self.valtext.set_text(self._format(val))
        if self.drawon:
            self.ax.figure.canvas.draw_idle()
        self.val = val
        if self.eventson:
            self._observers.process("changed", val)


def pad_multiply_plot_lims(plot_lims, padding_multiplier):
    dim_lengths = np.abs(np.diff(plot_lims, axis=1)).ravel()

    dim_half_lengths = dim_lengths * padding_multiplier / 2
    middle_coordinates = np.mean(plot_lims, axis=1)
    plot_lims = np.column_stack(
        (middle_coordinates - dim_half_lengths, middle_coordinates + dim_half_lengths)
    )
    return plot_lims


def get_automated_plot_lims(coordinates, padding_multiplier=1.1, padding=0):
    padding_multiplier = np.asarray(padding_multiplier)
    min_coordinates = np.min(coordinates, axis=0) - padding
    max_coordinates = np.max(coordinates, axis=0) + padding
    plot_lims = np.column_stack((min_coordinates, max_coordinates))
    plot_lims = pad_multiply_plot_lims(plot_lims, padding_multiplier)
    return plot_lims


def plot_lims_equal(plot_lims, aspect_multipliers=None):
    """
    Take plot_lims in 3D and new lims will have the same size as the largest input lims
    Aspect multiplier can correct for a plot window that is not square
    """
    if aspect_multipliers is None:
        ndims = len(plot_lims)
        if ndims == 2:
            aspect_multipliers = np.array([5 / 4, 1])
        else:
            aspect_multipliers = np.ones(ndims)
    else:
        aspect_multipliers = np.asarray(aspect_multipliers)

    axes_diffs = np.diff(plot_lims, axis=1).ravel()
    axes_signs = np.sign(axes_diffs)
    axes_sizes = np.abs(axes_diffs)
    new_half_size = (np.max(axes_sizes) * aspect_multipliers) / 2
    mean_plot_lims = np.mean(plot_lims, axis=1)
    plot_lims = np.array(
        [
            mean_plot_lims - new_half_size * axes_signs,
            mean_plot_lims + new_half_size * axes_signs,
        ]
    ).T
    return plot_lims


def plot_lims_symmetrical(plot_lims, around=0):
    around = (np.ones((1, len(plot_lims))) * np.asarray(around)).T
    max_abs_diffs = np.max(np.abs(plot_lims - around), axis=1)
    around = around.ravel()
    plot_lims = np.array([around - max_abs_diffs, around + max_abs_diffs]).T
    return plot_lims


def new_coordinates_zaxis(coordinates, zaxis):
    assert coordinates.shape[1] == 3, "must have 3 coordinates"
    assert zaxis == 0 or zaxis == 1 or zaxis == 2, "only 0, 1 or 2 valid for zaxis"
    coordinates_copy = coordinates.copy()
    coordinates[:, 2] = coordinates_copy[:, zaxis]
    coordinates[:, 1] = coordinates_copy[:, zaxis - 1]
    coordinates[:, 0] = coordinates_copy[:, zaxis - 2]
    return coordinates
