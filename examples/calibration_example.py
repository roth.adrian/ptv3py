import cv2
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path

import ptv3py

calibration_path = Path(__file__).parent / "calibration_images"


def read_calibration_images():
    Z_image_paths = sorted(list(calibration_path.glob("*.jpg")))

    calibration_images = []
    Z_positions = []
    for image_path in Z_image_paths:
        Z_position = "".join(
            [s for s in image_path.name if s.isdigit() or s == "-"]
        )  # in mm
        Z_position = int(Z_position)
        Z_positions.append(Z_position)

        current_calibration_image = cv2.imread(str(image_path), -1)
        # images of different cameras are stacked vertically
        current_calibration_image.shape = (
            2,
            current_calibration_image.shape[0] // 2,
            current_calibration_image.shape[1],
        )
        calibration_images.append(current_calibration_image)
    calibration_images = np.array(calibration_images)
    Z_positions = np.array(Z_positions)

    return calibration_images, Z_positions


chessboard_shape = (19, 10)
chessboard_square_width = 6  # mm

if __name__ == "__main__":
    calibration_images, Z_positions = read_calibration_images()
    camera_calibration = ptv3py.camera_calibration(
        calibration_images,
        Z_positions,
        chessboard_shape,
        chessboard_square_width,
        method="advanced",
        verbose=True,
    )
    ptv3py.visualise_camera_calibration(camera_calibration)
    plt.show()
