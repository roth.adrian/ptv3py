import numpy as np
import matplotlib.pyplot as plt

import ptv3py
from ptv3py import particle_finding, simulation

image_shape = (512, 512)

ndims = 3
simulation_kwargs = {
    "tracking": {
        "n_particles": 10,
        "n_frames": 15,  # at least three frames since the tracking requires it to function
        "ndims": ndims,
        "speed_mu": 3,
        "speed_std": 2,
        "acceleration_mu": 0.1,
        "acceleration_std": 0,
        "noise_std": 0.1,
        "volume": np.ones(ndims) * image_shape[0],
        "remove_particles_outside_volume": True,
    },
    "images": {
        "point_spread_sizes": 5,
        "point_spread_intensity": 255,
        "saturation": 4095,
    },
}

ptv_kwargs = {
    "finding": {
        "threshold": 100,
        "gradient_threshold": 0.1,
        "relative_gradient_threshold": True,
        "saturation": 4095,
        "extra_features_func": particle_finding.max_sum_area_features_func,
    },
    "matching": {
        "max_error": 0.1,
        "coordinate_limits": [[0, image_shape[0]]] * 3,
    },
    "tracking": {
        "max_speed": 20,
        "max_acceleration": 2,
        "max_extrapolation_error": 2,
        "max_jumped_frames": 0,
        "min_track_length": 4,
    },
}


def get_full_simulation(seed=1):
    print("Creating simulation")
    np.random.seed(seed)
    true_tracked_particles = simulation.simulate_particle_tracks(
        **simulation_kwargs["tracking"]
    )

    camera_calibration = simulation.get_simple_camera_calibration(image_shape)
    particles_2d = simulation.simulate_projection(
        true_tracked_particles,
        camera_calibration,
    )

    images = simulation.simulate_particle_images(
        image_shape, particles_2d, **simulation_kwargs["images"]
    )

    return true_tracked_particles, camera_calibration, particles_2d, images


def example_interactive_parameter_tuning(full_simulation):
    (
        true_tracked_particles,
        camera_calibration,
        true_particles_2d,
        images,
    ) = full_simulation

    # creates two windows for each step, one for visualising data and one for tuning parameters

    print("Create finding tuning figures")
    all_figures = []
    fptf, ps = ptv3py.particle_finding_parameters_helper(
        images=images,
    )
    fptf.suptitle("Finding")
    ps.suptitle("Finding")
    all_figures.extend([fptf, ps])
    # set initial params
    fptf.set_parameters(**ptv_kwargs["finding"])
    fptf.set_clims([0, 400])
    ps.set_frame(5)
    fptf.find_particles()

    print("Create matching tuning figures")
    mptf, ps = ptv3py.particle_matching_parameters_helper(
        true_particles_2d,
        camera_calibration=camera_calibration,
        images=images,
    )
    mptf.suptitle("Matching")
    ps.suptitle("Matching")
    all_figures.extend([mptf, ps])
    mptf.set_parameters(**ptv_kwargs["matching"])
    mptf.match_particles()

    print("Create tracking tuning figures")
    true_tracked_particles["id_track_hidden"] = true_tracked_particles["id_track"]
    true_tracked_particles.drop("id_track", axis=1, inplace=True)
    tptf, ps = ptv3py.particle_tracking_parameters_helper(
        particles=true_tracked_particles,
        particles_2d=true_particles_2d,
        images=images,
        threed2twod=True,
    )  # threed2twod simplifies understanding of the tracking limits with circles when clicking on tracks
    tptf.suptitle("Tracking")
    ps.suptitle("Tracking")
    all_figures.extend([tptf, ps])
    tptf.set_parameters(**ptv_kwargs["tracking"])
    # limit tracking to only part of the dataset
    tptf.set_framelims([0, 12])
    tptf.track_particles()

    plt.show()
    for fig in all_figures:
        plt.close(fig)


def example_tracking(full_simulation):
    (
        true_tracked_particles,
        camera_calibration,
        true_particles_2d,
        images,
    ) = full_simulation

    plotsliders = []

    ### Visualise the simulated tracks and images ###
    def post_plotting(ps):
        frame = ps.get_frame()
        ps.plot_ax_list[-1].imshow(
            images[frame][0],
            cmap="gray",
            vmin=0,
            vmax=400,
            zorder=-3,
        )

    ps = ptv3py.visualise_tracked_particles(
        true_tracked_particles,
        axis_equal=True,
        verbose=2,
        post_plotting=post_plotting,
        particles_2d=true_particles_2d,
    )
    ps.suptitle("Correct tracks")
    plotsliders.append(ps)
    ###

    ### Finding particles from images ###
    print("Finding particles")
    particles = ptv3py.find_particles(images, verbose=True, **ptv_kwargs["finding"])

    # use scroll wheel or shift arrow keys to go through all frames
    ps = ptv3py.visualise_found_particles(
        particles,
        images=images,
        extent=[0, image_shape[1], image_shape[0], 0],
        cmap="gray",
        verbose=True,  # makes interactive plot,
    )
    ps.suptitle("Finding")
    plotsliders.append(ps)
    ###

    ### Matching and triangulating 3D coordinates ###
    print("Matching and triangulating particles")
    matched_particles = ptv3py.threedmatch_particles(
        particles, camera_calibration, verbose=True, **ptv_kwargs["matching"]
    )
    ps = ptv3py.visualise_matched_particles(
        matched_particles, particles, images=images, axis_equal=True
    )  # again try clicking on the found particle points
    ps.suptitle("Matching")
    plotsliders.append(ps)
    ###

    ### Tracking 3D positions ###
    print("Tracking particles")
    tracked_particles = ptv3py.track_particles(
        matched_particles, verbose=True, **ptv_kwargs["tracking"]
    )
    ps = ptv3py.visualise_tracked_particles(
        tracked_particles,
        axis_equal=True,
        verbose=2,
        post_plotting=post_plotting,
        particles_2d=particles,
        cmap="viridis",  # show the speed of the tracks
    )
    ps.suptitle("Tracking")
    plotsliders.append(ps)
    ps.colorbar(label="speed")
    ###

    plt.show()
    for fig in plotsliders:
        plt.close(fig)


if __name__ == "__main__":
    full_simulation = get_full_simulation()

    example_interactive_parameter_tuning(full_simulation)
    example_tracking(full_simulation)
