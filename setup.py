import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ptv3py",
    version="0.0.1",
    author="Adrian Roth",
    author_email="roth.adrian@protonmail.com",
    description="3D Particle Tracking Velocimetry package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/roth.adrian/ptv3py",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU GPLv3",
        "Operating System :: OS Independent",
    ],
    scripts=[],
    python_requires=">=3.6",
    install_requires=[
        "numpy",
        "scipy",
        "scikit-image",
        "pandas",
        "matplotlib",
        "opencv-python",
        "uncertainties",
        "tqdm",
    ],
)
