# Particle Tracking Velocimetry in 3D
A python package to make the whole pipeline from images to estimated 3D velocities.

![Found particle tracks, velocity, and sizes, of coughed droplets](./readme_assets/coughing_tracks.mp4)

This package was used to estimate particle tracks found in the video above

## Installation
```
pip install git+https://gitlab.com/roth.adrian/ptv3py.git@master
```
Or clone the repository and

```
pip install <path-to-ptv3py>
```

## Usage
The ptv algorithm is divided into three parts:

1. Finding particles in images (`ptv3py.find_particles`)
   - Uses threshold and weighted mean to get the particle coordinates for each image
2. Matching particles between different cameras by using a camera calibration to finally get 3D points (`ptv3py.threedmatch_particles`)
  - There is a camera calibration module for assistance (`ptv3py.camera_calibration`)
  - Currently only matching for two cameras is implemented
3. Track particles over time (`ptv3py.track_particles`)
  - Uses an approach similar to so-called 4 frame best estimate tracking
  - Tracking can be performed in either 2D or 3D

See docstrings in source code for more detailed explanations and `ptv3py/__init__.py` for more useful functions.
The particle locations and tracks are returned as `pandas.DataFrame` with column names that should be self explanatory.

In the examples folder there are scripts that shows how the package can be used.

- `examples/ptv_example.py` shows the full process of tracking in 3D with the visualizations of each step on the way. There is also examples of the interactive tools designed to help with finding optimal parameters for the tracking.
- `examples/calibration_example.py` shows how the calibration is performed with the example calibration images found in a sub-folder.

## Publications

The following conference article describes the tracking algorithm in detail.

[https://portal.research.lu.se/en/publications/stereoscopic-high-speed-imaging-for-3d-tracking-of-coughed-saliva](https://portal.research.lu.se/en/publications/stereoscopic-high-speed-imaging-for-3d-tracking-of-coughed-saliva)

These publications have used this tracking algorithm:

[Roth A, Frantz D, Stiti M, Berrocal E. High-speed scattered-light imaging for sizing respiratory droplets. Journal of Aerosol Science. 2023 1;174:106257.](https://doi.org/10.1016/j.jaerosci.2023.106257)

[Roth A, Stiti M, Frantz D, Corber A, Berrocal E. Exhaled aerosols and saliva droplets measured in time and 3D space: Quantification of pathogens flow rate applied to SARS-CoV-2. Natural Sciences. 2023:e20230007.](https://doi.org/10.1002/ntls.20230007)
